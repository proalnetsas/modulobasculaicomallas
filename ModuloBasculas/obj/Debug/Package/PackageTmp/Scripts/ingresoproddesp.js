﻿var baseUrl = "../odata/";
var timeCapture = 3000;
var today = new Date();
var produccion = new Array();
produccion.push({ value: "SI", text: "PRODUCCIÓN CONFORME" });
produccion.push({ value: "NO", text: "PRODUCTO NO CONFORME" });
produccion.push({ value: "REPROCESO", text: "REPROCESO" });

var CantidadAPesarOrdenProduccion;
var CantidadPesadaOrdenProduccion;

var OrdenTerminada = false;

var maquinas;
var codigoTurno;
var codigoBatch;
var productos;

var TipoImpresion;
var ServerTipoImpresion;
var opimpresion;
var pesobrutoimpresion;
var pesonetoimpresion;
var rolloimpresion;
var metrosimpresion;
var item;
var operario;
var maquina;
var referenciaproducto;
var function_Login;
var cantidaddebobinaproduccion;
var IDBascula = null;
var consecutivo;
var anchoTramos;
var largoTramos;

var nombremaquina;
var codigooperario;

var tempTara;

var aplicalimite = false;
var codigoProceso;

var nombreimpresora;
var esquemadeimpresion;
var codigodespercio;
var nombrecliente;
var codigocliente;
var nitcliente;

var warehousecode;
var mesesdepuesvencimiento;

var tipodeimpresion;
var basculamanual = false

var tipoimpresionedicio;
var nuevoregistroetiquetaedicio = false;

var idcarrete = 0;
var idestibaabierta = 0;

var valorrepeticiones = 0;
var valorrepeticionesimpresion = "";

var camposocultos = "false";

var pesoTramoTeorico;
var pesoTramoReal;
var tramosAPesar;
var totalTramos;
var pesoTotalTramos;
var produccion;
var desperdicio;


$(document).ready(function () {

    obtenerPeso();

    //Cultura español de kendo
    kendo.culture("es-CO");
    activeClass("CapturaProdDesp", "ParentIngreso", "liIngreso");

    $("#datepicker").kendoDateTimePicker({
        value: today
    }).data("kendoDateTimePicker");
   
    $("#produccion").kendoDropDownList({
        optionLabel: "Seleccione ...",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: {
            data: produccion
        },
        select: selecProduccion,
    });

    $("#recurso").kendoDropDownList({
        optionLabel: "Seleccione recurso ...",
        dataTextField: "CODIGOPUESTO",
        dataValueField: "NOMBREPUESTO",
        change: selecRecurso
    });

    $("#referencia").kendoDropDownList({
        optionLabel: "Seleccione producto ...",
        dataTextField: "NombreProducto",
        dataValueField: "CodigoProducto",
        change: selecReferencia
    });

    $("#carrete").kendoDropDownList({
        optionLabel: "Seleccione carrete ...",
        dataTextField: "NOMBRE",
        dataValueField: "VALOR"
    });

    $("#celulacarrete").kendoDropDownList({
        optionLabel: "Seleccione proceso ...",
        dataTextField: "CODIGOCELULA",
        dataValueField: "CODIGOCELULA"
    });
    $("#estadocarrete").kendoDropDownList({});
    
    $("#CheckTramos").iCheck({
        checkboxClass: 'icheckbox_minimal',
        increaseArea: '20%'
    });

    $("#CheckTramos").on('ifChecked', function (event) {
        ValidarTramos();
    });

    $("#CheckTramos").on('ifUnchecked', function (event) {
        $("#variosTramos").addClass("displaynone");
        $("#tramosAPesar").addClass("displaynone");
    });

    $("#motdesperdicio").kendoDropDownList({
        optionLabel: "Seleccione el motivo ...",
        dataTextField: "NOMBRE",
        dataValueField: "CODIGO",
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: baseUrl + "CAUSALESDESPERDICIOs",
                }
            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
            },
        }
    });

    //Armar GridBanderas
    ObternerTipoImpresion();
    ArmarGridBanderas();

    $("#OpcionDeImpresion").kendoDropDownList();


    //Validación para ocultar configuración de manera local
    if (this.location.hostname == "localhost") {
        //document.getElementById("BtEliminarBobinas").style.visibility = "hidden";
        //document.getElementById("BtnConfiguracion").style.visibility = "hidden";
        //document.getElementById("BtnConfiguracionCarrete").style.visibility = "hidden";
    }
});

function selecRecurso(e) {
    
    if ($("#recurso").val()) {
        var recurso = $("#recurso").data("kendoDropDownList");
        getDetalle(recurso.text());
    }
}

function selecReferencia(e) {

    $("#produccion").data("kendoDropDownList").select(0);
    $("#ancho").val("");
    $("#largo").val("");
    TipoDeProduccion();
}

function getDetalle(codigoRecurso) {
    
    $("#txtError").html("");
    var op = $("#ordenproduccion").val();
    if (!validaTextoOP(op))
        return;
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    var filter = {
        OrdenProduccion: op,
        Fecha: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
        CodigoPuesto: codigoRecurso
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetDetalleOP", function (data) {

        if (data.RequiereCambioOperario == false) {

            $("#operario").val(data.Operario);
            $("#turno").val(data.Turno);
            codigoTurno = data.CodigoTurno;
            codigoBatch = data.CodigoEntradaEjecucion;

            item = data.CodigoProducto;
            operario = data.Operario;
            maquina = data.CodigoPuesto;

            nombremaquina = data.NombreMaquina;
            codigooperario = data.CodigoOperario;

            codigoProceso = data.CodigoProceso;



            /*ACTUALIZAR FECHA*/
            //if (basculamanual == false) {
            //    var todayDate = kendo.toString(kendo.parseDate(new Date()));
            //    $("#datepicker").data("kendoDateTimePicker").value(todayDate);
            //}

            //ConsultarTaraPorOrden(op, codigoRecurso, document.getElementById("produccion").value);
            //ConsultarSiAplicaLimites(codigoProceso);
            //ConsultarEsquemaImpresion(codigoRecurso);

            //ObtenerInformacionCliente(op, item);

            //ConsultarListaDeCarretes();

            //Validar si es devolución
            if (document.getElementById("produccion").value == "devolucion")
                ConsultarInformacionDevoluciones(op, codigoProceso);

            getProductosOP();
        }
        else {
            limpiar();
            toastr.error("Debe corregir la información del el ID Empleado desde el Módulo Dispositivo antes de poder ingresar bobinas para este BATCH", "Error");
        }
            

        

    }, errorHandler, JSON.stringify(filter))
}
function ObtenerInformacionCliente($op, $codigoproducto) {

    var filter = {
        OrdenProduccion: $op,
        CodigoProucto: $codigoproducto
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

        if (data != null) {
            nombrecliente = data.NombreCliente;
            codigocliente = data.CodigoCliente;
            nitcliente = data.NitCliente;
        }

    }, errorHandler, JSON.stringify(filter))
}
function ConsultarEsquemaImpresion($codigopuesto) {
    debugger;
    var filter = {
        CodigoPuesto: $codigopuesto
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetEsquemaDeImpresion", function (data) {
        if (data != null) {
            
            var jsontext = data.value;
            esquemadeimpresion = JSON.parse(jsontext);

            //Validacion para peso repeticiones y proceso siguiente
            if (esquemadeimpresion.TipoImpresion == "CORTE") {

                var filter = {
                    TipoImpresion: esquemadeimpresion.TipoImpresion,
                    OrdenDeProduccion: $("#ordenproduccion").val(),
                    CodigoProducto: item,
                    CodigoPuesto: maquina,
                    CodigoProceso: codigoProceso
                }
                request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionExtraImpresion", function (data) {
                    if (data != null) {
                        var value = JSON.parse(data.value);
                        if (value.Repeticiones != null && value.Repeticiones != "" && value.Repeticiones != 0)
                            valorrepeticiones = parseFloat(value.Repeticiones);
                        else
                            valorrepeticiones = 0;

                        if (document.getElementById("produccion").value == "SI") {
                            document.getElementById("contenedorrepeticiones").style.display = "";
                            document.getElementById("contenedorprocesosiguiente").style.display = "";
                        }
                    }
                }, errorHandler, JSON.stringify(filter))
            }
            else {
                document.getElementById("contenedorrepeticiones").style.display = "none";
                document.getElementById("contenedorprocesosiguiente").style.display = "none";
            }
        }
    }, errorHandler, JSON.stringify(filter))
}
function getConsecutivoBobina(codigoRecurso, Op, produccion)
{
    debugger
    var filter = {
        OrdenProduccion: Op,
        CodigoPuesto: codigoRecurso,
        TipoProduccion: produccion
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetConsecutivoBobina", function (data) {

        cantidaddebobinaproduccion = (data.value + 1);
        document.getElementById("consecutivo").value = cantidaddebobinaproduccion;

    }, errorHandler, JSON.stringify(filter))
}

function selecProduccion(e) {
    var dataItem = this.dataItem(e.item.index());
    var btn = $("#btnGuardar");
    var desp = $("#desperdicio");
    btn.addClass("displaynone");
    desp.addClass("displaynone");
    if (dataItem.value) {
        btn.removeClass("displaynone");
        if (dataItem.value == "NO") {
            desp.removeClass("displaynone");
        }
    }
}
function buscarOP() {
    
    //limpiarcarretes();
    $("#txtError").html("");
    var op = $("#ordenproduccion").val();
    if (!validaTextoOP(op))
        return;
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    var filter = {
        OrdenProduccion: op,
        Fecha: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
    }


    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetMaquinasOP", function (data) {
        if (data.value.length == 0) {
            $("#txtError").html("No existe información de esta orden de producción en el turno actual");
            return;
        }

        maquinas = data.value;
        var recurso = $("#recurso").data("kendoDropDownList");
        recurso.dataSource.data(maquinas);
        //Si es solo un recurso lo debemos seleccionar para que traiga los datos de la op
        if (maquinas.length == 1) {
            recurso.select(function (dataItem) {
                return dataItem.CODIGOPUESTO === maquinas[0].CODIGOPUESTO;
            });
            getDetalle(maquinas[0].CODIGOPUESTO);
        }

        getResumenOrden(op);

    }, errorHandler, JSON.stringify(filter))

    getCantidadOP(op);
}

function getCantidadOP(OrdenProduccion) {
    debugger;
    var filter = {
            CODIGOORDENPRODUCCION: OrdenProduccion
        }


    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetCantidadAPesarOrdenProduccion", function (data) {

        debugger;
        CantidadAPesarOrdenProduccion = data.value;

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetCantidadPesadaOrdenProduccion", function (data) {

            debugger;
            CantidadPesadaOrdenProduccion = data.value;

            if ((CantidadPesadaOrdenProduccion <= (CantidadAPesarOrdenProduccion + (CantidadAPesarOrdenProduccion * 0.03))) && (CantidadPesadaOrdenProduccion >= (CantidadAPesarOrdenProduccion - (CantidadAPesarOrdenProduccion * 0.03))) || ((CantidadAPesarOrdenProduccion - CantidadPesadaOrdenProduccion) == 0))
            {
                debugger;
                toastr.warning("La orden de Produccion ya se ha terminado de pesar", "Información");
                OrdenTerminada = true;
            }
            else
            {
                OrdenTerminada = false;
            }

        }, errorHandler, JSON.stringify(filter))
        
    }, errorHandler, JSON.stringify(filter))
}

function getResumenOrden(OrdenProduccion)
{

    var filter = {
        OrdenProduccion: OrdenProduccion
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetResumenOp", function (data) {
        debugger;
        if (data.value != null && data.value != undefined) {

            var data = data.value;

            $("#GridResumenOrden").kendoGrid({
                dataSource: {
                    data: data
                },
                groupable: false,
                sortable: false,
                resizable: false,
                reorderable: false,
                pageable: false,
                dataBound: onDataBound,
                heigth: 100,
                columns: [{
                    field: "TIPO",
                    title: "OP: " + OrdenProduccion
                },
                {
                    field: "TRAMOSPRODUCCION",
                    title: "TRAMOS"
                }, 
                {
                    field: "M2PRODUCCION",
                    title: "M2"
                },
                {
                        field: "PESOPRODUCCION",
                        title: "PESO"
                },
                {
                   
                        field: "TRAMOSRETAL",
                        title: "TRAMOS"
                    }, {
                        field: "M2RETAL",
                        title: "M2"
                    }, {
                        field: "PESORETAL",
                        title: "PESO"
                    
                },
                {
                        field: "TRAMOSIMPERFECTO",
                        title: "TRAMOS"
                    }, {
                        field: "M2IMPERFECTO",
                        title: "M2"
                    }, {
                        field: "PESOIMPERFECTO",
                        title: "PESO"
                    
                },
                {
                        field: "TRAMOSSALDO",
                        title: "TRAMOS"
                    }, {
                        field: "M2SALDO",
                        title: "M2"
                    }, {
                        field: "PESOSALDO",
                        title: "PESO"
                    
                },
                {
                        field: "TRAMOSREPROCESO",
                        title: "TRAMOS"
                    }, {
                        field: "M2REPROCESO",
                        title: "M2"
                    }, {
                        field: "PESOREPROCESO",
                        title: "PESO"
                }]
            });
        }

    }, errorHandler, JSON.stringify(filter))

}

function onDataBound(arg) {
    var myElem = document.getElementById('trParentHeader');
    if (myElem == null) {
        $("#GridResumenOrden").find("th.k-header").parent().before("<tr id='trParentHeader'> <th colspan='1' class='k-header'> </th> <th colspan='3' class='k-header'><strong>Producción</strong></th> <th colspan='3' class='k-header'><strong>Retal</strong></th> <th colspan='3' class='k-header'><strong>Imperfecto</strong></th> <th colspan='3' class='k-header'><strong>Saldo</strong></th> <th colspan='3' class='k-header'><strong>Reproceso</strong></th></tr>");
    }
}

function getProductosOP() {
    debugger;
    $("#txtError").html("");
    var op = $("#ordenproduccion").val();


    var filtro = {
        OrdenProduccion: op
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetProductosOP", function (data) {

        debugger;
        if (data.value.length == 0) {
            $("#txtError").html("No hay información de productos en esta orden de producción.");
            return;
        }

        productos = data.value;
        var ProdOP = $("#referencia").data("kendoDropDownList");
        ProdOP.dataSource.data(productos);

        if (productos.length == 1) {
            ProdOP.select(function (dataItem) {
                return dataItem.NombreProducto === productos[0].NombreProducto;
            });
          
        }


    }, errorHandler, JSON.stringify(filtro))

}

//Función para hacer ajaxrequest
function request(contenttype, tokentype, token, type, url, sucess, error, data) {
    $.ajax({
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader("Content-Type", contenttype);
            if (tokentype)
                xhrObj.setRequestHeader("Authorization", tokentype + " " + token);
        },
        type: type,
        url: url,
        data: data,
        dataType: "json",
        success: sucess,
        error: error
    });
}
//Función para controlar error de llamados ajax
function errorHandler(jqXHR, exception) {
    debugger;
    var txt = '';
    if (jqXHR.status === 0) {
        txt = 'No se puede encontrar la ruta especificada.';
    } else if (jqXHR.status == 404) {
        txt = 'La página solicitada no se encuentra. [404]';
    } else if (jqXHR.status == 500) {
        txt = 'Error interno del servidor. [500]';
    } else if (exception === 'parsererror') {
        txt = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        txt = 'Error de tiempo de espera agotado';
    } else if (exception === 'abort') {
        txt = 'Ajax request aborted.';
    }
    else {
        if (jqXHR.responseJSON) {
            if (jqXHR.responseJSON["odata.error"]) {
                var error = '';
                if (jqXHR.responseJSON["odata.error"].message) {
                    error = jqXHR.responseJSON["odata.error"].message.value;
                }
                if (jqXHR.responseJSON["odata.error"].innererror) {
                    error = error + " " + jqXHR.responseJSON["odata.error"].innererror.message;
                }
                if (error != "")
                    txt = error;
                else
                    txt = 'Error no controlado.\n' + jqXHR.responseText;
            }
            else
                txt = 'Error no controlado.\n' + jqXHR.responseText;
        }
        else
            txt = 'Error no controlado.\n' + jqXHR.responseText;
    }
    $("#txtError").html(txt);
}


function ValidarCantidadTotalTramos()
{
    debugger;

    var tramosAPesar = parseFloat($("#InputTramosAPesar").val());
    totalTramos = parseInt($("#InputTotalTramos").val());

    if(tramosAPesar > totalTramos)
    {
        toastr.error("La cantidad de tramos a pesar no puede superar la cantidad total de tramos", "Error");
        $("#InputTramosAPesar").val("");
    }
}

function guardar(produccion) {

    debugger;
    var bascula;


    /*Valores para la impresion*/
    var checked = $("#CheckTramos").iCheck('update')[0].checked;

    pesobrutoimpresion = parseFloat($("#pesoneto").val()).toFixed(2);
    pesonetoimpresion = "";
    rolloimpresion = "";
    metrosimpresion = "";
    opimpresion = $("#ordenproduccion").val();

    pesonetoimpresion = $("#peso").val();
    pesonetoimpresion = parseFloat(pesonetoimpresion).toFixed(2);

    metrosimpresion = parseFloat($("#ancho").val()) * parseFloat($("#largo").val());
    metrosimpresion = metrosimpresion.toFixed(2);

    $("#txtError").html("");
    var metros = parseFloat($("#ancho").val()) * parseFloat($("#largo").val());
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    let CmbDesperdicio = $("#motdesperdicio").data("kendoDropDownList");
    let MotivoDesperdicio = CmbDesperdicio.text();

    if (produccion == "PRODUCCION" || produccion == "REPROCESO")
    {
        MotivoDesperdicio = "";
    }
    

    
    var puestotrabajo = $("#recurso").data("kendoDropDownList");

    var PesoTotalBas = parseFloat(document.getElementById("pesoneto").value);
    PesoTotalBas = PesoTotalBas.toFixed(2);

    if (!checked)
    {
        bascula = {
            FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
            CODIGOORDENPRODUCCION: $("#ordenproduccion").val(),
            TURNO: $("#turno").val(),
            NOMBREPUESTO: $("#recurso").val(),
            REFERENCIA: $("#referencia").val(),
            PRODUCCION: produccion,
            TRAMOS: 1,
            ANCHO: parseFloat($("#ancho").val()).toFixed(2),
            LARGO: parseFloat($("#largo").val()).toFixed(2),
            PESO: PesoTotalBas,
            METROS: metros,
            CAUSALDESPERDICIO: MotivoDesperdicio,
            CODIGOPUESTO: puestotrabajo.text(),
            CODIGOTURNO: codigoTurno,
            CODIGOENTRADAEJECUCION: codigoBatch,
            ELIMINADO: false,
            CODIGODESPERDICIO: codigodespercio,
            OBSERVACIONES: document.getElementById("ObservacionesProduccion").value,
            NROETIQUETA: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3)
        };
    }
    else if (checked)
    {
        debugger;
        anchoTramos = totalTramos * parseFloat($("#ancho").val());
        largoTramos = totalTramos * parseFloat($("#largo").val());

        bascula = {
            FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
            CODIGOORDENPRODUCCION: $("#ordenproduccion").val(),
            TURNO: $("#turno").val(),
            NOMBREPUESTO: $("#recurso").val(),
            REFERENCIA: $("#referencia").val(),
            PRODUCCION: produccion,
            TRAMOS: totalTramos,
            ANCHO: anchoTramos.toString(),
            LARGO: largoTramos.toString(),
            PESO: pesoTotalTramos,
            METROS: metros,
            CAUSALDESPERDICIO: MotivoDesperdicio,
            CODIGOPUESTO: puestotrabajo.text(),
            CODIGOTURNO: codigoTurno,
            CODIGOENTRADAEJECUCION: codigoBatch,
            ELIMINADO: false,
            CODIGODESPERDICIO: codigodespercio,
            OBSERVACIONES: document.getElementById("ObservacionesProduccion").value,
            NROETIQUETA: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3)
        };
    }

    request("application/json", null, null, "POST", baseUrl + "BASCULAs", function (data) {
        debugger;
        document.getElementById("btnGuardar").disabled = false;
        IDBascula = data.ID;

        //limpiar();

        if (produccion == "PRODUCCION")
        {
            ImprimirProduccionGenerico();
        }
        else if (produccion  == "DESPERDICIO")
        {
            ImprimirDesperdicio();
        }
        else if (produccion == "REPROCESO")
        {
            ImprimirReproceso();
        }



    }, errorHandler, JSON.stringify(bascula));
}

function InformarOrdenTerminada()
{
    debugger;
    if (OrdenTerminada && $("#produccion").val() == "SI") {

        $('#ModalOrdenTerminada').modal('show');
    }
    else {
        ImprimirEtiqueta();
    }

}


function ImprimirEtiqueta() {

    debugger;
    $('#ModalOrdenTerminada').modal('hide');
    
    var op = $("#ordenproduccion").val();

    if (!validaTextoOP(op) || !validaValores())
        return;
    if (!validarInicioTurno())
        return;

    

    desperdicio = $("#motdesperdicio").data("kendoDropDownList");

    produccion = "PRODUCCION";

    if ($("#produccion").val() == "NO") {
        produccion = "DESPERDICIO";
        if (desperdicio.value() == "") {
            $("#txtError").html("Se debe seleccionar el motivo de desperdicio");
            document.getElementById("btnGuardar").disabled = false;
            return;
        }
        motivo = desperdicio.text();



    }
    else if ($("#produccion").val() == "REPROCESO") {
        motivo = "REPROCESO";
        produccion = "REPROCESO";
    }

    getConsecutivoBobina(maquina, op, produccion);

    var checked = $("#CheckTramos").iCheck('update')[0].checked;

    if (checked) {
        if (($("#InputTotalTramos").val() == "") || ($("#InputTramosAPesar").val() == "")) {
            toastr.warning("Por favor ingrese el total de tramos y los tramos a pesar antes de guardar");
            return;
        }
        if ($("#pesoneto").val() == "") {
            toastr.warning("No se ha registrado peso en la bascula, por favor pese el tramo antes de guardar");
            return;
        }

    }

    var ancho = document.getElementById("ancho").value;
    var largo = document.getElementById("largo").value;
    var pesoneto = document.getElementById("pesoneto").value;

    if (ancho == "No hay un Ancho definido para este producto") {
        toastr.warning("Este producto no tiene un Ancho definido, por favor contacte al administrador", "Información");
        document.getElementById("btnGuardar").disabled = false;
        return;
    }

    if (largo == "No hay un Largo definido para este producto") {
        toastr.warning("Este producto no tiene un Largo definido, por favor contacte al administrador", "Información");
        document.getElementById("btnGuardar").disabled = false;
        return;
    }

    if (!ancho)
        ancho = 0;
    if (!pesoneto)
        pesoneto = 0;
    if (!largo)
        largo = 0;

    document.getElementById("btnGuardar").disabled = true;


    if (produccion == "PRODUCCION")
    {
        $("#tituloModalImprimirProduccion").text("IMPRIMIR PRODUCCIÓN");
    }
    else if (produccion == "DESPERDICIO")
    {
        $("#tituloModalImprimirProduccion").text("IMPRIMIR DESPERDICIO");
    }
    else
    {
        $("#tituloModalImprimirProduccion").text("IMPRIMIR REPROCESO");
    }

    $('#ModalImprimirProduccion').modal('show');
}

function ImprimirProduccionGenerico() {
    debugger;
    var filter;
    var checked = $("#CheckTramos").iCheck('update')[0].checked;

    if (!checked)
    {
        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: "1",
            Ancho: parseFloat($("#ancho").val()).toFixed(2),
            Largo: parseFloat($("#largo").val()).toFixed(2),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesobrutoimpresion,
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
        }

    }
    else if (checked)
    {
        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: totalTramos.toString(),
            Ancho: anchoTramos.toFixed(2),
            Largo: largoTramos.toFixed(2),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesoTotalTramos.toString(),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
        }
    }

        $('#ModalMensaje').modal('show');
        $('#ModalImprimirProduccion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintProduccionGenerico",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
                ActualizarOP();
            },
            error: function (ajaxContext) {
                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON["odata.error"].innererror.message);
            }
        });
}

function ImprimirDesperdicio() {
    debugger;
    var filter;
    var checked = $("#CheckTramos").iCheck('update')[0].checked;

    if (!checked) {
        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: "1",
            Ancho: parseFloat($("#ancho").val()).toFixed(2),
            Largo: parseFloat($("#largo").val()).toFixed(2),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesobrutoimpresion,
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
            MotivoDesperdicio: motivo
        }

    }
    else if (checked) {

        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: totalTramos,
            Ancho: anchoTramos.toFixed(2),
            Largo: largoTramos.toFixed(2),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesoTotalTramos,
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
            MotivoDesperdicio: motivo
        }
    }

    $('#ModalMensaje').modal('show');
    $('#ModalImprimirProduccion').modal('hide');

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/PrintDesperdicio",
        data: JSON.stringify(filter),
        success: function (data) {
            $('#ModalMensaje').modal('hide');
            ActualizarOP();
        },
        error: function (ajaxContext) {
            $('#ModalMensaje').modal('hide');
            alert(ajaxContext.responseJSON["odata.error"].innererror.message);
        }
    });
};

function ImprimirReproceso() {

    debugger;
    var filter;
    var checked = $("#CheckTramos").iCheck('update')[0].checked;

    if (!checked) {
        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: "1",
            Ancho: $("#ancho").val(),
            Largo: $("#largo").val(),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesobrutoimpresion,
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
            MotivoDesperdicio: motivo
        }

    }
    else if (checked) {

        filter = {
            OrdenDeProduccion: opimpresion,
            CodigoProducto: item,
            Producto: $("#referencia").data("kendoDropDownList").text(),
            CodigoMaquina: maquina,
            Consecutivo: $("#ordenproduccion").val() + pad($("#consecutivo").val(), 3),
            Tramos: totalTramos,
            Ancho: anchoTramos.toFixed(2),
            Largo: largoTramos.toFixed(2),
            Metros: metrosimpresion,
            PesoNeto: pesonetoimpresion,
            PesoTotal: pesoTotalTramos,
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            Observaciones: document.getElementById("ObservacionesProduccion").value,
            MotivoDesperdicio: motivo
        }
    }

    $('#ModalMensaje').modal('show');
    $('#ModalImprimirProduccion').modal('hide');

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/PrintReproceso",
        data: JSON.stringify(filter),
        success: function (data) {
            $('#ModalMensaje').modal('hide');
            ActualizarOP();
        },
        error: function (ajaxContext) {
            $('#ModalMensaje').modal('hide');
            alert(ajaxContext.responseJSON["odata.error"].innererror.message);
        }
    });
};

function ImprimirCoExtrusionGeneral() {

    if (document.getElementById("BanderasExtrusion").value == "")
        toastr.warning("Debe ingresar la información de número de banderas", "Información");

    else if (document.getElementById("MaterialExtrusion").value == "")
        toastr.warning("Debe seleccionar un valor de material de extrusión", "Información");

    else if (document.getElementById("TratamientoExtrusion").value == "")
        toastr.warning("Debe seleccionar un valor de tratamiento de corona", "Información");

    else if (document.getElementById("FormulaExtrusion").value == "")
        toastr.warning("Debe ingresar la información de fórmula de extrusión", "Información");

    else if (document.getElementById("CalibreExtrusion").value == "")
        toastr.warning("Debe ingresar la información de calibre de extrusión", "Información");

    else {

        //Almacenar registro de banderas
        AlmacenarBanderas("Extrusion", document.getElementById("BanderasExtrusion").value);
        //Almacenar observaciones
        AlmacenarObservaciones(document.getElementById("NotaExtrusion").value);

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Banderas: document.getElementById("BanderasExtrusion").value,
            TipoMaterial: document.getElementById("MaterialExtrusion").value,
            TratamientoCorona: document.getElementById("TratamientoExtrusion").value,
            Fecha: "",
            Formula: document.getElementById("FormulaExtrusion").value,
            Calibre: document.getElementById("CalibreExtrusion").value,
            AnchoExtrusion: document.getElementById("AnchoExtrusion").value,
            Metros: parseInt(metrosimpresion).toString(),
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            Nota: document.getElementById("NotaExtrusion").value,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString()
        }
        $('#ModalMensaje').modal('show');
        $('#ModalExtrusionImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCoExtrusoraGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {
                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON["odata.error"].innererror.message);
            }
        });
    }
}
function ImprimirFlexoGeneral() {

    if (document.getElementById("BanderasFlexo").value == "")
        toastr.warning("Debe ingresar la información de número de banderas", "Información");

    else if (document.getElementById("MaterialFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de material de extrusión", "Información");

    else if (document.getElementById("CuradoFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de curado", "Información");

    else if (document.getElementById("OpSiguienteFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de Op. Siguiente", "Información");

    else {

        //Almacenar registro de banderas
        AlmacenarBanderas("Flexografia", document.getElementById("BanderasFlexo").value);
        //Almacenar observaciones
        AlmacenarObservaciones(document.getElementById("NotaFlexo").value);

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Banderas: document.getElementById("BanderasFlexo").value,
            TipoMaterial: document.getElementById("MaterialFlexo").value,
            Curado: document.getElementById("CuradoFlexo").value,
            OpSiguiente: document.getElementById("OpSiguienteFlexo").value,
            Fecha: "",
            Metros: parseInt(metrosimpresion).toString(),
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            Nota: document.getElementById("NotaFlexo").value,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString()
        }
        $('#ModalMensaje').modal('show');
        $('#ModalFlexoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintFlexoGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {
                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON["odata.error"].innererror.message);
            }
        });
    }
}

function ImprimirCorte() {
    
    if (tipodeimpresion == "PrintCorteGeneral") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString()
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteQuala") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            CodigoQrRutaCliente: "",
            Codigo128: "",
            Codigo128Texto: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteQuala",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteNutresa") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Codigo128: "",
            Codigo128Texto: "",
            CodigoEAN14: "",
            SKU: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteNutresa",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteIngles") {
        
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Brand: "",
            Ponum: "",
            FechaBatch: "",
            CodigoEntrada: codigoBatch.toString(),
            CamposOcultos: camposocultos
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteIngles",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
        
}
function AlmacenarRegistroEPT() {

    var _cantidad = 0;
    if (esquemadeimpresion.TipoImpresion == "CORTE")
        _cantidad = (valorrepeticionesimpresion == "" || valorrepeticionesimpresion == null) ? 0 : parseFloat(valorrepeticionesimpresion);
    else
        _cantidad = parseFloat(metrosimpresion);

    //Insertar estiba
    var filter1 = {
        consecutivo: parseInt(rolloimpresion.toString()),
        idbascula: parseInt(IDBascula),
        idestibaencabezado: parseInt(idestibaabierta),
        pesoneto: parseFloat(parseFloat(pesonetoimpresion).toFixed(2)),
        tara: parseFloat(parseFloat((parseFloat(pesobrutoimpresion) - parseFloat(pesonetoimpresion))).toFixed(2)),
        cantidad: _cantidad
    }
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/InsertarDetalleEstiba",
        data: JSON.stringify(filter1),
        success: function () {}
    });
}

function ImprimirSelado() {
    debugger;
    if (tipodeimpresion == "PrintSelladoGeneral") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString()
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintSelladoQuala") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            CodigoQrRutaCliente: "",
            Codigo128: "",
            Codigo128Texto: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoQuala",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintSelladoNutresa") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Codigo128: "",
            Codigo128Texto: "",
            CodigoEAN14: "",
            SKU: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoNutresa",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }
    
    else if (tipodeimpresion == "PrintSelladoIngles") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Brand: "",
            Ponum: "",
            FechaBatch: "",
            CodigoEntrada: codigoBatch.toString(),
            CamposOcultos: camposocultos
        }
        
        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoIngles",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
}

function ConsultarInformacionImpresion($TipoImpresion){

    var filter = {
        TipoImpresion: $TipoImpresion,
        OrdenDeProduccion: opimpresion,
        CodigoProducto: item,
        CodigoPuesto: maquina,
        CodigoProceso: codigoProceso
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionExtraImpresion", function (data) {

        if (data != null) {
            var value = JSON.parse(data.value);
            if ($TipoImpresion == "COEXTRUSION") {
                document.getElementById("AnchoExtrusion").value = value.AnchoExtrusion;
            }
            if ($TipoImpresion == "CORTE") {
                
                //if (value.Repeticiones != null && value.Repeticiones != "" && value.Repeticiones != 0)
                //    document.getElementById("RepeticionesCorte").value = parseFloat((pesonetoimpresion / parseFloat(value.Repeticiones)) * 1000).toFixed(0);
                //else
                //    document.getElementById("RepeticionesCorte").value = "";
                document.getElementById("RepeticionesCorte").value = valorrepeticionesimpresion;
                document.getElementById("BobinasCorte").value = value.Bobinas;
            }
        }
    }, errorHandler, JSON.stringify(filter))
}
function LimpiarPreImpresion($TipoImpresion) {

    warehousecode = "";
    mesesdepuesvencimiento = "";
    tipodeimpresion = "";
    if ($TipoImpresion == "COEXTRUSION") {
        document.getElementById("BanderasExtrusion").value = "";
        document.getElementById("MaterialExtrusion").value = "";
        document.getElementById("MaterialExtrusion").text = "";
        document.getElementById("TratamientoExtrusion").value = "";
        document.getElementById("TratamientoExtrusion").text = "";
        document.getElementById("FormulaExtrusion").value = "";
        document.getElementById("CalibreExtrusion").value = "";
        document.getElementById("NotaExtrusion").value = "";
        document.getElementById("AnchoExtrusion").value = "";
    }
    else if ($TipoImpresion == "FLEXO") {
        document.getElementById("BanderasFlexo").value = "";
        document.getElementById("MaterialFlexo").value = "";
        document.getElementById("CuradoFlexo").text = "";
        document.getElementById("OpSiguienteFlexo").value = "";
        document.getElementById("NotaFlexo").value = "";
    }

    else if ($TipoImpresion == "CORTE") {
        document.getElementById("RepeticionesCorte").value = "";
        document.getElementById("BobinasCorte").value = "";
    }
}
function AlmacenarBanderas($TipoImpresion, $valor) {

    if (IDBascula != null && IDBascula != "") {
        var BasculaBanderas = {
            COLORBANDERA: "Unico",
            CANTIDADBANDERAS: $valor,
            NOBREPROCESO: $TipoImpresion,
            BASCULAID: IDBascula
        }

        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))
    }
}
function AlmacenarObservaciones($valor) {

    if (IDBascula != null && IDBascula != "" && $valor != "") {
        var BasculaExtras =
            {
                NOMBRECAMPO: "Observaciones",
                VALORCAMPO: $valor,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULAEXTRASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaExtras))
    }
}

function limpiar() {
    debugger;
    $("#txtError").html("");
    //$("#produccion").val("");
    $("#turno").val("");
    $("#recurso").val("");
    $("#referencia").val("");
    $("#bobina").val("");
    $("#tara").val("");
    $("#peso").val("");
    $("#metros").val("");
    $("#operario").val("");
    //$("#recurso").data("kendoDropDownList").value(0);
    $("#datepicker").data("kendoDateTimePicker").value(new Date());
    //$("#produccion").data("kendoDropDownList").value(0);
    //codigoTurno = "";
    $("#produccion").data("kendoDropDownList").select(0);
    document.getElementById("procesosiguiente").selectedIndex = 0;
    document.getElementById("repetiones").value = "";
    document.getElementById("contenedorrepeticiones").style.display = "none";
    document.getElementById("contenedorprocesosiguiente").style.display = "none";
    document.getElementById("contenedordevoluciones").style.display = "none";
    $("#desperdicio").addClass("displaynone");

    var recurso = $("#recurso").data("kendoDropDownList");
    var _data = recurso.dataSource.data();

    for (var i = 0; i <= _data.length; i++) {
        recurso.dataSource.remove(_data[0]);
    }
    document.getElementById("assembly").value = "";
    document.getElementById("secuenciamaterial").value = "";
    document.getElementById("materialemitido").value = "";
    document.getElementById("materialdescripcion").value = "";
    document.getElementById("lote").value = "";
    document.getElementById("notadevolucion").value = "";
    //codigoProceso = "";
    document.getElementById("diveventocolor").style.backgroundColor = "";
    document.getElementById("lblevento").innerText = "";
}

function obtenerPeso() {
    debugger;
    connection = $.hubConnection("http://localhost:9992");
    proxyBascula = connection.createHubProxy("clientHub");
    connection.start({ logging: true })
    proxyBascula.on('data', function (dato) {
        if (basculamanual == true) {
            connection.stop({ logging: true });
            proxyBascula.off('data', function () { });
        }
        else {
            if (dato != null && dato.Valor != undefined) {
                $("#pesoneto").val(dato.Valor);
                nombreimpresora = dato.NombreImpresora;
                //nombreimpresora = "Microsoft Print to PDF";
                $("#pesoneto").change();
            }
        }
    });
    connection.error(function (error) {
        toastr.error('Se ha presentado una falla con la comunicación con la báscula, contacte al administrador: ' + error.message, 'Error');
    });
}
function validaTextoOP(op) {
    if (op == "" || op == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
        document.getElementById("btnGuardar").disabled = false;
        return false;
    }
    return true;
}
function validaValores() {

    if (document.getElementById("produccion").value == "NO" || document.getElementById("produccion").value == "devolucion")//Desperdicio
    {
        var resultado = true;
        if ($("#peso").val() == 0 || $("#peso").val() == "" || $("#peso").val() < 0 || $("#peso").val() == null)
            resultado = false;
        //if ($("#metros").val() == 0 || $("#metros").val() == "" || $("#metros").val() == null)
        //    resultado = false;
        if ($("#pesoneto").val() == 0 || $("#pesoneto").val() == "" || $("#pesoneto").val() < 0 || $("#pesoneto").val() == null)
            resultado = false;
        if (!resultado) {
            $("#txtError").html("Se debe ingresar el peso (no negativo / ceros) y los metros para poder guardar");
            document.getElementById("btnGuardar").disabled = false;
        }
        return resultado;
    }
    else {

        var resultado = true;
        if ($("#peso").val() == 0 || $("#peso").val() == "" || $("#peso").val() < 0 || $("#peso").val() == null)
            resultado = false;
        //if ($("#metros").val() == 0 || $("#metros").val() == "" || $("#metros").val() == null)
        //    resultado = false;
        if ($("#pesoneto").val() == 0 || $("#pesoneto").val() == "" || $("#pesoneto").val() < 0 || $("#pesoneto").val() == null)
            resultado = false;
        if (!resultado) {
            $("#txtError").html("Se debe ingresar el peso (no negativo / ceros) y los metros para poder guardar");
            document.getElementById("btnGuardar").disabled = false;
        }
        return resultado;
    }
    
}
function validarInicioTurno() {

    var resultado = true;
    if ($("#datepicker").data("kendoDateTimePicker").value() == 0 || $("#datepicker").data("kendoDateTimePicker").value() == "" || $("#datepicker").data("kendoDateTimePicker").value() == null)
        resultado = false;
    if ($("#ordenproduccion").val().replace(" ", "") == null || $("#ordenproduccion").val().replace(" ", "") == "")
        resultado = false;
    if ($("#recurso").val() == null || $("#recurso").val() == "" || $("#recurso").val() == "Seleccione recurso ...")
        resultado = false;
    if ($("#operario").val() == null || $("#operario").val() == "")
        resultado = false;
    if ($("#turno").val() == null || $("#turno").val() == "")
        resultado = false;
    if ($("#referencia").val() == null || $("#referencia").val() == "")
        resultado = false;
    if (!resultado) {
        $("#txtError").html("Se debe ingresar los valores de inicio de turno");
        document.getElementById("btnGuardar").disabled = false;
    }
    return resultado;
}

function calculoPeso() {
    debugger;

    var peso = parseFloat($("#peso").val());
    var pesobascula = parseFloat($("#pesoneto").val());

    if (!peso)
        peso = 0;
    if (!pesobascula)
        pesobascula = 0;

    if ($("#produccion").val() == "SI")
    {
        var checked = $("#CheckTramos").iCheck('update')[0].checked;

        if(!checked)
        {
            if (pesobascula > (pesoTramoTeorico + (pesoTramoTeorico * 0.03)) || pesobascula < (pesoTramoTeorico - (pesoTramoTeorico * 0.03)))
            {
                toastr.warning('Producto fuera de especificación, por favor seleccione una opción de no conformidad', 'Error');
                $("#btnGuardar").attr("disabled", "true");
            }
            else
            {
                
                $("#btnGuardar").removeAttr("disabled", "false");
            }
        }
        else
        {
            debugger;
            tramosAPesar = $("#InputTramosAPesar").val();
            totalTramos = $("#InputTotalTramos").val();

            pesoTramoReal = (pesobascula / tramosAPesar);

            if (pesoTramoReal > (pesoTramoTeorico + (pesoTramoTeorico * 0.03)) || pesoTramoReal < (pesoTramoTeorico - (pesoTramoTeorico * 0.03))) {
                toastr.warning('Producto fuera de especificación, por favor seleccione una opción de no conformidad', 'Error');
                $("#btnGuardar").attr("disabled", "true");
            }
            else {
                
                pesoTotalTramos = pesoTramoReal * totalTramos;
                $("#btnGuardar").removeAttr("disabled", "false");
            }
        }

    }
    else
    {
        tramosAPesar = $("#InputTramosAPesar").val();
        totalTramos = $("#InputTotalTramos").val();

        pesoTramoReal = (pesobascula / tramosAPesar);

        pesoTotalTramos = pesoTramoReal * totalTramos;
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function isNumberWithoutDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function ValidarTramos() {
    debugger;
    $("#variosTramos").removeClass("displaynone");
    $("#tramosAPesar").removeClass("displaynone");
    $("#btnGuardar").removeAttr("disabled", "false");

}

function buscarconfiguracionetiquetas() {

    if (document.getElementById("procesosconfiguracion").value != "") {
        var filter = {
            Proceso: document.getElementById("procesosconfiguracion").value
        }

        //limpiar 
        tipoimpresionedicio = "";

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/GetEsquemaDeImpresionPorProceso",
            data: JSON.stringify(filter),
            success: function (data) {
                
                var _data = JSON.parse(data.value);

                var grid = $("#gridconfigetiquetas").data("kendoGrid");
                if (grid != undefined) {

                    $("#gridconfigetiquetas").empty();
                }
                tipoimpresionedicio = _data.TipoImpresion;
                $("#gridconfigetiquetas").kendoGrid({
                    dataSource: {
                        data: _data.Reportes,
                        schema: {
                            model: {
                                fields: {
                                    DESTINOIMPRESION: { type: "string" },
                                    MesesVencimiento: { type: "string" },
                                    NitCliente: { type: "string" },
                                    WareHouseCode: { type: "string" },
                                    CamposOcultos: { type: "string" }
                                }
                            }
                        },
                    },
                    height: 250,
                    scrollable: true,
                    sortable: true,
                    filterable: true,
                    selectable: "true",
                    change: viewseleccionarconfigetiquetas,
                    columns: [
                        { field: "DESTINOIMPRESION", title: "Impresión", width: "50%", filterable: false },
                        { field: "MesesVencimiento", title: "Meses vencimiento", width: "25%", filterable: false },
                        { field: "NitCliente", title: "Nit cliente", width: "25%", filterable: { multi: true } }
                    ]
                });
            },
            error: function (ajaxContext) {
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
    else {
        tipoimpresionedicio = "";
        alert("Debe seleccionar un proceso para la configuración del sistema");
        var grid = $("#gridconfigetiquetas").data("kendoGrid");
        if (grid != undefined) {

            $("#gridconfigetiquetas").empty();
        }
    }
}
function viewseleccionarconfigetiquetas() {
    
    var grid = $("#gridconfigetiquetas").data("kendoGrid");
    var itemgrid = grid.dataItem(grid.select());

    if (itemgrid.NitCliente == "*") {
        alert("No se puede editar el registro genérico de etiqueta");
    }
    else {
        
        $('#ModalEdicionEtiqueta').modal('show');
        document.getElementById("areaedicionetiqueta").value = tipoimpresionedicio;
        nuevoregistroetiquetaedicio = false;
        if (itemgrid.DESTINOIMPRESION == "ImprimirCoExtrusionGeneral" || itemgrid.DESTINOIMPRESION == "PrintFlexoGeneral"
            || itemgrid.DESTINOIMPRESION == "PrintSelladoGeneral" || itemgrid.DESTINOIMPRESION == "PrintCorteGeneral")
            $("#tipoetiquetaedicion").val("GENERAL");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoNutresa" || itemgrid.DESTINOIMPRESION == "PrintCorteNutresa")
            $("#tipoetiquetaedicion").val("NUTRESA");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoQuala" || itemgrid.DESTINOIMPRESION == "PrintCorteQuala")
            $("#tipoetiquetaedicion").val("QUALA");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoIngles" || itemgrid.DESTINOIMPRESION == "PrintCorteIngles")
            $("#tipoetiquetaedicion").val("INGLES");
        
        $("#mesesvencimientoetiqueta").val(itemgrid.MesesVencimiento);
        document.getElementById("nitclienteedicionetiquetas").value = itemgrid.NitCliente;
        document.getElementById("wareHouseCodeetiqueta").value = itemgrid.WareHouseCode;
        $("#camposvisibleetiqueta").val(itemgrid.CamposOcultos);
    }
}

function almacenarconfiguracionetiqueta() {
    
    if (document.getElementById("tipoetiquetaedicion").value == "")
        alert("Debe seleccionar un valor para el tipo de etiqueta");
    else if (document.getElementById("mesesvencimientoetiqueta").value == "")
        alert("Debe seleccionar un valor para los meses de vencimiento");
    else if (document.getElementById("nitclienteedicionetiquetas").value == "")
        alert("Debe ingresar un valor para el nit del cliente");
    else if (document.getElementById("nitclienteedicionetiquetas").value == "*")
        alert("Debe ingresar un valor diferente al genérico de etiqueta usando el nit cliente *");
    else if (document.getElementById("wareHouseCodeetiqueta").value == "")
        alert("Debe ingresar un valor para WareHouse");
    else {

        if (nuevoregistroetiquetaedicio == false) {

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var itemgrid = grid.dataItem(grid.select());

            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION") {
                itemgrid.DESTINOIMPRESION = "ImprimirCoExtrusionGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO") {
                itemgrid.DESTINOIMPRESION = "PrintFlexoGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "CORTE") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    itemgrid.DESTINOIMPRESION = "PrintCorteGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    itemgrid.DESTINOIMPRESION = "PrintCorteNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    itemgrid.DESTINOIMPRESION = "PrintCorteQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    itemgrid.DESTINOIMPRESION = "PrintCorteIngles";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoIngles";
            }

            itemgrid.MesesVencimiento = document.getElementById("mesesvencimientoetiqueta").value;
            itemgrid.NitCliente = document.getElementById("nitclienteedicionetiquetas").value;
            itemgrid.WareHouseCode = document.getElementById("wareHouseCodeetiqueta").value;
            itemgrid.CamposOcultos = document.getElementById("camposvisibleetiqueta").value;

            var tipoWareHouseCode = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "CORTE")
                tipoWareHouseCode = "BPTF";
            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO")
                tipoWareHouseCode = "BPTF";
            else
                tipoWareHouseCode = "BPTF";

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var cadenaedicion = '{"TipoImpresion":"' + document.getElementById("areaedicionetiqueta").value + '","WareHouseCode":"' + tipoWareHouseCode + '","Reportes":[';

            for (var i = 0; i < grid._data.length; i++) {
                cadenaedicion = cadenaedicion + '{"DESTINOIMPRESION":"' + grid._data[i].DESTINOIMPRESION + '","NitCliente":"'
                    + grid._data[i].NitCliente + '","MesesVencimiento":"' + grid._data[i].MesesVencimiento
                    + '","WareHouseCode":"' + grid._data[i].WareHouseCode + '","CamposOcultos":"' + grid._data[i].CamposOcultos + '"},'
            }
            cadenaedicion = cadenaedicion.substr(0, cadenaedicion.length - 1) + "]}";

            var filter = {
                Proceso: document.getElementById("areaedicionetiqueta").value,
                ValueRDLC: cadenaedicion
            }

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/GetEditarEstandares",
                data: JSON.stringify(filter),
                success: function (data) {

                    buscarconfiguracionetiquetas();
                },
                error: function (ajaxContext) {
                    alert("Error al intentar almacenar el registro");
                }
            });
        }

        else {

            var _destinoimpresion = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION") {
                _destinoimpresion = "ImprimirCoExtrusionGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO") {
                _destinoimpresion = "PrintFlexoGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "CORTE") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    _destinoimpresion = "PrintCorteGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    _destinoimpresion = "PrintCorteNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    _destinoimpresion = "PrintCorteQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    _destinoimpresion = "PrintCorteIngles";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    _destinoimpresion = "PrintSelladoGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    _destinoimpresion = "PrintSelladoNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    _destinoimpresion = "PrintSelladoQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    _destinoimpresion = "PrintSelladoIngles";
            }

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            grid.dataSource.add({
                DESTINOIMPRESION: _destinoimpresion, MesesVencimiento: document.getElementById("mesesvencimientoetiqueta").value,
                NitCliente: document.getElementById("nitclienteedicionetiquetas").value,
                WareHouseCode: document.getElementById("wareHouseCodeetiqueta").value,
                CamposOcultos: document.getElementById("camposvisibleetiqueta").value,
            });

            var tipoWareHouseCode = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "CORTE")
                tipoWareHouseCode = "BPTF";
            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO")
                tipoWareHouseCode = "BPTF";
            else
                tipoWareHouseCode = "BPTF";

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var cadenaedicion = '{"TipoImpresion":"' + document.getElementById("areaedicionetiqueta").value + '","WareHouseCode":"' + tipoWareHouseCode + '","Reportes":[';
            for (var i = 0; i < grid._data.length; i++) {
                cadenaedicion = cadenaedicion + '{"DESTINOIMPRESION":"' + grid._data[i].DESTINOIMPRESION + '","NitCliente":"'
                    + grid._data[i].NitCliente + '","MesesVencimiento":"' + grid._data[i].MesesVencimiento
                    + '","WareHouseCode":"' + grid._data[i].WareHouseCode + '","CamposOcultos":"' + grid._data[i].CamposOcultos + '"},'
            }
            cadenaedicion = cadenaedicion.substr(0, cadenaedicion.length - 1) + "]}";

            var filter = {
                Proceso: document.getElementById("areaedicionetiqueta").value,
                ValueRDLC: cadenaedicion
            }

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/GetEditarEstandares",
                data: JSON.stringify(filter),
                success: function (data) {

                    buscarconfiguracionetiquetas();
                },
                error: function (ajaxContext) {
                    alert("Error al intentar almacenar el registro");
                }
            });
        }

        $('#gridconfigetiquetas').data('kendoGrid').refresh();
        $('#ModalEdicionEtiqueta').modal('hide');

    }
}

function agregarnuevaetiquetaedicion() {
    
    if (tipoimpresionedicio == "" || tipoimpresionedicio == undefined)
        alert("Debe seleccionar y buscar un tipo de proceso para agregar una nueva regla")
    else {

        $('#ModalEdicionEtiqueta').modal('show');
        document.getElementById("areaedicionetiqueta").value = tipoimpresionedicio;
        document.getElementById("tipoetiquetaedicion").selectedIndex = 0;
        document.getElementById("mesesvencimientoetiqueta").selectedIndex = 0;
        document.getElementById("nitclienteedicionetiquetas").value = "";
        document.getElementById("wareHouseCodeetiqueta").value = "";
        document.getElementById("camposvisibleetiqueta").selectedIndex = 0;
        nuevoregistroetiquetaedicio = true;
    }
}

/*Funciones imprimir*/
function ObternerTipoImpresion() {

    /*Buscar tipo reporte*/
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetTipoImpresion", function (data) {

        ServerTipoImpresion = data.value;
    }, errorHandler)

}
function ConfirmarImprimir($Valor) {

    if ($Valor == "Si") {

        $('#ModalConfirmacion').modal('hide');
        AbrirImprmir();
    }

    else {

        IDBascula = null;
        $('#ModalConfirmacion').modal('hide');
    }
}
function AbrirImprmir() {

    TipoImpresion = document.getElementById("OpcionDeImpresion").value;

    /*Limpiar impresión*/
    LimpiarImpresion();

    ArmarGridBanderas();

    if (TipoImpresion == "Extrusion") {

        $('#ModalExtrusionImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionExtrusion").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Flexografia") {
        $('#ModalFlexografiaImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionFlexografia").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Laminado") {
        $('#ModalLaminadoImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionLaminado").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {
        $('#ModalCorteImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionCorte").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "SelladoPT") {
        $('#ModalSelladoPTImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSelladoPt").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "SelladoPQ") {
        $('#ModalSelladoPQImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSelladoPq").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {
        $('#ModalSelladoImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSellado").value = opimpresion;
            BuscarOPImpresion();
        }
    }
}
function LimpiarImpresion() {

    if (TipoImpresion == "Extrusion") {

        document.getElementById("OrdenProduccionExtrusion").value = "";
        document.getElementById("DateExtrusion").value = "";
        document.getElementById("ItemExtrusion").value = "";
        document.getElementById("FormulaExtrusion").value = "";
        document.getElementById("OperarioExtrusion").value = "";
        document.getElementById("PesoBrutoExtrusion").value = "";
        document.getElementById("PesoNetoExtrusion").value = "";
        document.getElementById("RolloExtrusion").value = "";
        document.getElementById("MaquinaExtrusion").value = "";
        document.getElementById("MetrosExtrusion").value = "";
        document.getElementById("AnchoExtrusion").value = "";
        document.getElementById("CalibreExtrusion").value = "";
        document.getElementById("ProcesoExtrusion").selectedIndex = 0
        document.getElementById("MaterialExtrusion").selectedIndex = 0
        document.getElementById("ObservacionesExtrusion").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasExtrusion').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Flexografia") {

        document.getElementById("OrdenProduccionFlexografia").value = "";
        document.getElementById("DateFlexografia").value = "";
        document.getElementById("TurnoFlexografia").value = "";
        document.getElementById("ItemFlexografia").value = "";
        document.getElementById("ReferenciaFlexografia").value = "";
        document.getElementById("OperarioFlexografia").value = "";
        document.getElementById("PesoBrutoFlexografia").value = "";
        document.getElementById("RolloFlexografia").value = "";
        document.getElementById("PesoNetoFlexografia").value = "";
        document.getElementById("MetroFlexografia").value = "";
        document.getElementById("ObservacionesFlexografia").value = "";
        document.getElementById("ClienteFlexografia").value = "";
        document.getElementById("ProcesoFlexografia").selectedIndex = 0;
        document.getElementById("MaterialFlexografia").selectedIndex = 0;
        document.getElementById("MaquinaFlexografia").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasFlexografia').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Laminado") {

        document.getElementById("OrdenProduccionLaminado").value = "";
        document.getElementById("DateLaminado").value = "";
        document.getElementById("TurnoLaminado").value = "";
        document.getElementById("ItemLaminado").value = "";
        document.getElementById("ReferenciaLaminado").value = "";
        document.getElementById("OperarioLaminado").value = "";
        document.getElementById("PesoBrutoLaminado").value = "";
        document.getElementById("RolloLaminado").value = "";
        document.getElementById("PesoNetoLaminado").value = "";
        document.getElementById("MetroLaminado").value = "";
        document.getElementById("ObservacionesLaminado").value = "";
        document.getElementById("ClienteLaminado").value = "";
        document.getElementById("ProcesoLaminado").selectedIndex = 0;
        document.getElementById("MaterialLaminado").selectedIndex = 0;
        document.getElementById("MaquinaLaminado").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasLaminado').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {

        document.getElementById("OrdenProduccionCorte").value = "";
        document.getElementById("DateCorte").value = "";
        document.getElementById("TurnoCorte").value = "";
        document.getElementById("ItemCorte").value = "";
        document.getElementById("ReferenciaCorte").value = "";
        document.getElementById("ClienteCorte").value = "";
        document.getElementById("OperarioCorte").value = "";
        document.getElementById("MaquinaCorte").value = "";
        document.getElementById("PesoBrutoCorte").value = "";
        document.getElementById("ConsecutivoCorte").value = "";
        document.getElementById("PesoNetoCorte").value = "";
        document.getElementById("RepeticionesCorte").value = "";
        document.getElementById("QBobinasCorte").value = "";
        document.getElementById("CodigoClienteCorte").value = "";
        document.getElementById("NitClienteCorte").value = "";
        document.getElementById("BrandCorte").value = "";
        document.getElementById("PonumCorte").value = "";
        document.getElementById("SlitPackDateCorte").value = "";

    }

    else if (TipoImpresion == "SelladoPT") {

        document.getElementById("OrdenProduccionSelladoPt").value = "";
        document.getElementById("DateSelladoPt").value = "";
        document.getElementById("TurnoSelladoPt").value = "";
        document.getElementById("ItemSelladoPt").value = "";
        document.getElementById("ReferenciaSelladoPt").value = "";
        document.getElementById("ClienteSelladoPt").value = "";
        document.getElementById("OperarioSelladoPt").value = "";
        document.getElementById("MaterialSelladoPt").value = "";
        document.getElementById("PesoNetoSelladoPt").value = "";
        document.getElementById("CantidadSelladoPt").value = "";
        document.getElementById("CodigoClienteSelladoPt").value = "";
        document.getElementById("OCSelladoPt").value = "";

    }

    else if (TipoImpresion == "SelladoPQ") {

        document.getElementById("OrdenProduccionSelladoPq").value = "";
        document.getElementById("DateSelladoPq").value = "";
        document.getElementById("TurnoSelladoPq").value = "";
        document.getElementById("ItemSelladoPq").value = "";
        document.getElementById("ReferenciaSelladoPq").value = "";
        document.getElementById("ClienteSelladoPq").value = "";
        document.getElementById("OperarioSelladoPq").value = "";
        document.getElementById("MaterialSelladoPq").value = "";
        document.getElementById("PaquetePq").value = "";
        document.getElementById("UnidadPorPaquetePq").value = "";
        document.getElementById("CodigoClienteSelladoPq").value = "";
        document.getElementById("OCSelladoPq").value = "";

    }

    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        document.getElementById("OrdenProduccionSellado").value = "";
        document.getElementById("DateSellado").value = "";
        document.getElementById("TurnoSellado").value = "";
        document.getElementById("ItemSellado").value = "";
        document.getElementById("ReferenciaSellado").value = "";
        document.getElementById("ClienteSellado").value = "";
        document.getElementById("OperarioSellado").value = "";
        document.getElementById("MaquinaSellado").value = "";
        document.getElementById("PesoBrutoSellado").value = "";
        document.getElementById("ConsecutivoSellado").value = "";
        document.getElementById("PesoNetoSellado").value = "";
        document.getElementById("CodigoClienteSellado").value = "";
        document.getElementById("NitClienteSellado").value = "";
        document.getElementById("BrandSellado").value = "";
        document.getElementById("PonumSellado").value = "";
        document.getElementById("SlitPackDateSellado").value = "";
        document.getElementById("CantidadSellado").value = "";
    }
}
function ArmarGridBanderas() {

    if (TipoImpresion == "Extrusion") {
        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasExtrusion").empty();
        }

        $("#GridBanderasExtrusion").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasExtrusion .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasExtrusion .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {
            
            var CountRows = 1;
            //var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                //NumerBandera = parseInt(NumerBandera) - 1;
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasExtrusion").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {
                
                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }


    else if (TipoImpresion == "Flexografia") {

        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasFlexografia").empty();
        }

        $("#GridBanderasFlexografia").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasFlexografia .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasFlexografia .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            var CountRows = 1;
            //var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasFlexografia").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {

                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }


    else if (TipoImpresion == "Laminado") {

        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasLaminado").empty();
        }

        $("#GridBanderasLaminado").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasLaminado .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasLaminado .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            var CountRows = 1;
            var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasLaminado").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {

                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }
}


function BuscarOPImpresion() {

    if (TipoImpresion == "Extrusion") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionExtrusion").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateExtrusion").value = today;

        document.getElementById("PesoBrutoExtrusion").value = pesobrutoimpresion;
        document.getElementById("PesoNetoExtrusion").value = pesonetoimpresion;
        document.getElementById("RolloExtrusion").value = rolloimpresion;
        document.getElementById("MetrosExtrusion").value = metrosimpresion;

        document.getElementById("ItemExtrusion").value = item;
        document.getElementById("OperarioExtrusion").value = operario;
        document.getElementById("MaquinaExtrusion").value = maquina;

    }

    else if (TipoImpresion == "Flexografia") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionFlexografia").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateFlexografia").value = today;
        
        document.getElementById("PesoBrutoFlexografia").value = pesobrutoimpresion;
        document.getElementById("PesoNetoFlexografia").value = pesonetoimpresion;
        document.getElementById("RolloFlexografia").value = rolloimpresion;
        document.getElementById("MetroFlexografia").value = metrosimpresion;

        document.getElementById("ItemFlexografia").value = item;
        document.getElementById("OperarioFlexografia").value = operario;
        document.getElementById("ReferenciaFlexografia").value = referenciaproducto;
        document.getElementById("TurnoFlexografia").value = codigoTurno;
        document.getElementById("MaquinaFlexografia").value = maquina;
        

        BuscarCliente();
    }

    else if (TipoImpresion == "Laminado") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionLaminado").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateLaminado").value = today;

        document.getElementById("PesoBrutoLaminado").value = pesobrutoimpresion;
        document.getElementById("PesoNetoLaminado").value = pesonetoimpresion;
        document.getElementById("RolloLaminado").value = rolloimpresion;
        document.getElementById("MetroLaminado").value = metrosimpresion;

        document.getElementById("ItemLaminado").value = item;
        document.getElementById("OperarioLaminado").value = operario;
        document.getElementById("ReferenciaLaminado").value = referenciaproducto;
        document.getElementById("TurnoLaminado").value = codigoTurno;
        document.getElementById("MaquinaLaminado").value = maquina;

        BuscarCliente();
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionCorte").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateCorte").value = today;
        
        document.getElementById("PesoBrutoCorte").value = parseFloat(pesobrutoimpresion).toFixed(2);
        document.getElementById("PesoNetoCorte").value = parseFloat(pesonetoimpresion).toFixed(2);

        document.getElementById("ItemCorte").value = item;
        document.getElementById("OperarioCorte").value = codigooperario;
        document.getElementById("MaquinaCorte").value = nombremaquina;
        document.getElementById("ReferenciaCorte").value = referenciaproducto;
        document.getElementById("TurnoCorte").value = codigoTurno;
        document.getElementById("ConsecutivoCorte").value = rolloimpresion;

        BuscarCliente();

        var filter = {
            OrdenProduccion: op,
            CodigoProducto: item
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBobinasPorCaja", function (data) {

            if (data != null) {
                document.getElementById("QBobinasCorte").value = data.value;
            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPesoRepeticion", function (data) {

            if (data != null) {

                if (data.value == "")
                {
                    document.getElementById("RepeticionesCorte").value = "";
                }
                else
                {
                    document.getElementById("RepeticionesCorte").value = parseFloat((document.getElementById("PesoNetoCorte").value / parseFloat(data.value)) * 1000).toFixed(0);
                }
                
            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPonum", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("PonumCorte").value = "";
                }
                else {
                    document.getElementById("PonumCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBrand", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("BrandCorte").value = "";
                }
                else {
                    document.getElementById("BrandCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        var filter = {
            Bacth: codigoBatch
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetFechaBatch", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("SlitPackDateCorte").value = "";
                }
                else {
                    document.getElementById("SlitPackDateCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

    }

    else if (TipoImpresion == "SelladoPT") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPt").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSelladoPt").value = today;
        
        document.getElementById("PesoNetoSelladoPt").value = pesonetoimpresion;

        document.getElementById("ItemSelladoPt").value = item;
        document.getElementById("OperarioSelladoPt").value = operario;
        document.getElementById("ReferenciaSelladoPt").value = referenciaproducto;
        document.getElementById("TurnoSelladoPt").value = codigoTurno;

        BuscarCliente();
    }

    else if (TipoImpresion == "SelladoPQ") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPq").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSelladoPq").value = today;
        
        document.getElementById("ItemSelladoPq").value = item;
        document.getElementById("OperarioSelladoPq").value = operario;
        document.getElementById("ReferenciaSelladoPq").value = referenciaproducto;
        document.getElementById("TurnoSelladoPq").value = codigoTurno;

        BuscarCliente();
    }


    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSellado").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSellado").value = today;

        document.getElementById("PesoBrutoSellado").value = parseFloat(pesobrutoimpresion).toFixed(2);
        document.getElementById("PesoNetoSellado").value = parseFloat(pesonetoimpresion).toFixed(2);
        document.getElementById("CantidadSellado").value = parseFloat(metrosimpresion).toFixed(0);

        document.getElementById("ItemSellado").value = item;
        document.getElementById("OperarioSellado").value = codigooperario;
        document.getElementById("MaquinaSellado").value = nombremaquina;
        document.getElementById("ReferenciaSellado").value = referenciaproducto;
        document.getElementById("TurnoSellado").value = codigoTurno;
        document.getElementById("ConsecutivoSellado").value = rolloimpresion;

        BuscarCliente();

        var filter = {
            OrdenProduccion: op,
            CodigoProducto: item
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPonum", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("PonumSellado").value = "";
                }
                else {
                    document.getElementById("PonumSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBrand", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("BrandSellado").value = "";
                }
                else {
                    document.getElementById("BrandSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        var filter = {
            Bacth: codigoBatch
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetFechaBatch", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("SlitPackDateSellado").value = "";
                }
                else {
                    document.getElementById("SlitPackDateSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

    }

    
}
function BuscarCliente() {

    if (TipoImpresion == "Flexografia") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionFlexografia").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemFlexografia").value   
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null)
            {
                document.getElementById("ClienteFlexografia").value = data.NombreCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Laminado") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionLaminado").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemLaminado").value   
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteLaminado").value = data.NombreCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionCorte").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemCorte").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteCorte").value = data.NombreCliente;
                document.getElementById("CodigoClienteCorte").value = data.CodigoCliente;
                document.getElementById("NitClienteCorte").value = data.NitCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    else if (TipoImpresion == "SelladoPT") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPt").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSelladoPt").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSelladoPt").value = data.NombreCliente;
                document.getElementById("CodigoClienteSelladoPt").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))

    }

    else if (TipoImpresion == "SelladoPQ") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPq").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSelladoPq").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSelladoPq").value = data.NombreCliente;
                document.getElementById("CodigoClienteSelladoPq").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSellado").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSellado").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSellado").value = data.NombreCliente;
                document.getElementById("CodigoClienteSellado").value = data.CodigoCliente;
                document.getElementById("NitClienteSellado").value = data.NitCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }
}



function AbrirImpresionGenerico() {
    
    if (document.getElementById("ordenproduccion").value == "" || document.getElementById("ordenproduccion").value == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
    }
    else if (document.getElementById("ordenproduccion").value == "" || document.getElementById("ordenproduccion").value == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
    }
    else if (document.getElementById("recurso").value == "" || document.getElementById("recurso").value == null) {
        $("#txtError").html("No hay información del recurso");
    }
    else if (document.getElementById("operario").value == "" || document.getElementById("operario").value == null) {
        $("#txtError").html("No hay información del operario");
    }
    else if (document.getElementById("turno").value == "" || document.getElementById("turno").value == null) {
        $("#txtError").html("No hay información del turno");
    }
    else if (document.getElementById("referencia").value == "" || document.getElementById("referencia").value == null) {
        $("#txtError").html("No hay información del producto");
    }
    else
    {
        /*Abrir Modal Genérico*/
        $('#ModalGenerico').modal('show');
        var op = document.getElementById("ordenproduccion").value;

        /*Buscar cliente*/
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: item

        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteGenerico").value = data.NombreCliente;
                document.getElementById("CodigoClienteGenerico").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))

        /*Setear Objetos de impresión genérico*/
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateGenerico").value = today;
        document.getElementById("OrdenProduccionGenerico").value = op;
        document.getElementById("ItemGenerico").value = item;
        document.getElementById("ReferenciaGenerico").value = referenciaproducto;
        document.getElementById("MaquinaGenerico").value = maquina;
        document.getElementById("OperarioGenerico").value = operario;
        document.getElementById("TurnoGenerico").value = codigoTurno;
        document.getElementById("CopiasGenerico").value = "1";
    }
}
function Confirmar() {

    $('#ModalConfirmacion').modal('show');
}
function TipoDeProduccion() {

    debugger;

    var ancho;
    var largo;
    var pesom2;

    $("#largo").replaceWith($('<input />').attr({
        type: 'text',
        id: $("#largo").attr('id'),
        name: $("#largo").attr('name'),
        class: $("#largo").attr('class'),
        value: $("#largo").val()
    }));


    document.getElementById("contenedorrepeticiones").style.display = "None";
    document.getElementById("contenedorprocesosiguiente").style.display = "None";
    document.getElementById("contenedordevoluciones").style.display = "None";
    document.getElementById("diveventocolor").style.backgroundColor = "";
    document.getElementById("lblevento").innerText = "";

    var opcion = document.getElementById("produccion").value;
    var producto = $("#referencia").data("kendoDropDownList").value();

    var filter = {
        PRODUCTO: producto
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetAnchoLargo", function (data) {

        if (data.value[0].AnchoProducto != null) {
            document.getElementById("ancho").value = data.value[0].AnchoProducto;
            ancho = data.value[0].AnchoProducto;
        }
        else {
            document.getElementById("ancho").value = "No hay un Ancho definido para este producto"
            ancho = 0;
        }

        if (data.value[0].LargoProducto != null) {
            document.getElementById("largo").value = data.value[0].LargoProducto;
            largo = data.value[0].LargoProducto;
        }
        else {
            document.getElementById("largo").value = "No hay un Largo definido para este producto"
            ancho = 0;
        }

        if (data.value[0].PesoProducto != null) {

            document.getElementById("peso").value = data.value[0].PesoProducto;
            pesom2 = data.value[0].PesoProducto;
        }
        else {
            document.getElementById("peso").value = "No hay un Peso definido para este producto"
            pesom2 = 0;
        }

        pesoTramoTeorico = ((ancho * largo) * pesom2);

    }, errorHandler, JSON.stringify(filter))

    $("#checkVariosTramos").removeClass("displaynone");

    

    if (opcion == "NO")
    {
        document.getElementById("diveventocolor").style.backgroundColor = "red";
        document.getElementById("lblevento").innerText = "PRODUCTO NO CONFORME";

        document.getElementById("ancho").removeAttribute("disabled");
        document.getElementById("largo").removeAttribute("disabled");
        $("#btnGuardar").removeAttr("disabled", "false");
    }
    else if (opcion == "SI") {

        document.getElementById("diveventocolor").style.backgroundColor = "green";
        document.getElementById("lblevento").innerText = "PRODUCCIÓN CONFORME";

        document.getElementById("ancho").setAttribute("disabled", "true");
        document.getElementById("largo").setAttribute("disabled", "true");
    }
    
    else if (opcion == "REPROCESO") {

        document.getElementById("diveventocolor").style.backgroundColor = "blue";
        document.getElementById("lblevento").innerText = "REPROCESO";

        document.getElementById("ancho").removeAttribute("disabled");
        document.getElementById("largo").removeAttribute("disabled");
        $("#btnGuardar").removeAttr("disabled", "false");
    }
}

function ActualizarOP()
{
    location.reload();
}

function checkRetal() {
    debugger;
    let CmbDesperdicio = $("#motdesperdicio").data("kendoDropDownList");
    let MotivoDesperdicio = CmbDesperdicio.text();

   

    if(MotivoDesperdicio[0] === 'R')
    {
        $("#largo").replaceWith('<select id="largo" name="largo" class="form-control">' +
          '<option value="1">1 m</option>' +
          '<option value="1.2">1,2 m</option>' +
          '<option value="1.5">1,5 m</option>' +
          '<option value="1.8">1,8 m</option>' +
        '</select>');
    }
    else
    {
        $("#largo").replaceWith($('<input />').attr({
            type: 'text',
            id: $("#largo").attr('id'),
            name: $("#largo").attr('name'),
            class: $("#largo").attr('class'),
            value: $("#largo").val()
        }));
    }
}

function ModalLogin($function) {

    function_Login = $function;
    $('#ModalLogin').modal('show');
}

function LoginEditBobinas()
{
    var Username = document.getElementById("Username").value;
    var Password = document.getElementById("Password").value;
    
    if (function_Login != null | function_Login != "")
    {
        if (Username != "" & Password != "") {
            var filter = {
                Username: Username,
                Password: Password
            }

            request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetLoginEdicionBobinas", function (data) {

                if (data.value == true) {
                    
                    document.getElementById("Username").value = "";
                    document.getElementById("Password").value = "";

                    $('#ModalLogin').modal('hide');

                    if (function_Login == "Bobinas") 
                        $('#ModalEditBobinas').modal('show');

                    else if (function_Login == "BobinasManual") {

                        if (basculamanual == false) {
                            document.getElementById("pesoneto").value = "";
                            document.getElementById("peso").value = "";
                            document.getElementById("pesoneto").disabled = false;
                            basculamanual = true;
                        }
                        else
                            location.reload();
                    }

                    else if (function_Login == "Configuracion") {
                        $('#ModalConfiguracion').modal('show');
                    }

                    else if (function_Login == "ConfiguracionCarrete") {
                        listadecarretesedicion();
                        $('#ModalConfiguracionCarrete').modal('show');
                    }
                        
                }

                else
                    alert("Username y/o Password incorrecto");

            }, errorHandler, JSON.stringify(filter))
        }

        else
            alert("Debe ingresar Username/Password");
    }
}
function ConsultarOrdenProduccionBobinas() {
    
    var OrdenProduccion = document.getElementById("OrdenProduccionBobina").value;
    if (OrdenProduccion != "") {

        var filter = {
            OrdenProduccion: OrdenProduccion
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EdicionOrdenProduccionBobinas", function (data) {

            if (data.value != null) {
               
                var data = data.value;

                $("#GridBobinas").kendoGrid({
                    dataSource: {
                        data: data,
                        schema: {
                            model: {
                                id: "ID",
                                fields: {
                                    NROETIQUETA: { editable: false },
                                    CODIGOORDENPRODUCCION: { editable: false },
                                    NOMBREPUESTO: { editable: false },
                                    REFERENCIA: { editable: false },
                                    PRODUCCION: { editable: false },
                                    TRAMOS: { editable: false },
                                    ANCHO: { editable: false },
                                    LARGO: {editable: false},
                                    PESO: { editable: false },
                                    METROS: { editable: false },
                                    ELIMINADO: { editable: true, type: "boolean" },
                                }
                            }
                        }
                    },
                    filterable: true,
                    sortable: true,
                    resizable: true,
                    pageable: false,
                    editable: false,
                    columns: [
                        { title: "ETIQUETA", field: "NROETIQUETA", filterable: false, editable: false, width:"85px"},
                        { title: "ORDEN", field: "CODIGOORDENPRODUCCION", filterable: false, editable: false, width: "70px" },
                        { title: "MAQUINA", field: "NOMBREPUESTO", filterable: false, editable: false, width: "80px" },
                        { title: "PRODUCTO", field: "REFERENCIA", filterable: false, editable: false, width: "90px" },
                        { title: "TIPO", field: "PRODUCCION", filterable: false, editable: false, width: "100px"  },
                        { title: "TRAMOS", field: "TRAMOS", filterable: false, editable: false },
                        { title: "ANCHO", field: "ANCHO", filterable: false, editable: false },
                        { title: "LARGO", field: "LARGO", filterable: false, editable: false },
                        { title: "PESO", field: "PESO", filterable: false, editable: false },
                        { title: "METROS", field: "METROS", filterable: false, editable: false },
                        { title: "ELIMINADO", field: "ELIMINADO", filterable: false, editable: true, type: "boolean", template: "#= ELIMINADO ? 'Si' : 'No' #" },
                        { command: ["edit" ], title: "Editar", width: "80px" },
                    ],
                    editable: "popup",
                    edit: function(e)
                    {
                        debugger;
                        var grid = $("#GridBobinas").data("kendoGrid");
                        var dataItem = e.model;

                        $(".k-grid-update").on("click", function (e) {
                            debugger;

                            var filter = {
                                ID: dataItem.ID,
                                OrdenProduccion: dataItem.CODIGOORDENPRODUCCION,
                                Valor: dataItem.ELIMINADO
                            }

                            request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarBascula", function (data) {
                            }, errorHandler, JSON.stringify(filter))
                        });
                    }
                });
            }

            else
                alert("No hay información con el numero Op ingresado");

        }, errorHandler, JSON.stringify(filter))
    }

    else
        alert("Debe ingresar una orden de producción");
}
function GuardarBobinas() {
    
    var _idbasculaeliminar;
    var _idbasculaeliminarestado;
    var grid = $("#GridBobinas").data("kendoGrid");
    if (grid != null)
    {
        var cantidad = grid._data.length;
        if (cantidad > 1)
        {
            for (var i = 1; i <= cantidad; i++) {
                
                var Text = "tr:eq(" + i + ")"
                var data = grid.dataItem(Text);
                
                if (data != null)
                {
                    _idbasculaeliminar = data.ID;
                    _idbasculaeliminarestado = data.ELIMINADO;
                    var filter = {
                        ID: data.ID,
                        OrdenProduccion: data.CODIGOORDENPRODUCCION,
                        Valor: data.ELIMINADO
                    }
                    
                    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarBascula", function (data1) {
                    }, errorHandler, JSON.stringify(filter))

                    //Eliminar registro de EPT si estaiba este asociada
                    if (_idbasculaeliminarestado == true) {

                        var filter2 = {
                            ID: parseInt(_idbasculaeliminar)
                        }

                        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarBobinaEstiba", function (data2) {
                            
                            if (data2.value == true)
                                toastr.info("Se ha eliminado la bobinas de la estiba de EPT", "Error");

                        }, errorHandler, JSON.stringify(filter2))
                    }
                }
            }

            alert("Edición terminada correctamente");
            setTimeout(function () { location.reload(); }, 3000);
        }
    }
    
}
function GuardarConfiguracion() {

    var Impresora = document.getElementById("Impresora").value;
    var CopiasProduccion = document.getElementById("CopiasProduccion").value;
    var CopiasDesperdicio = document.getElementById("CopiasDesperdicio").value;
    var TipoImpresion = document.getElementById("TipoImpresion").value;
    var AnchoImpresion = document.getElementById("AnchoImpresion").value;
    var AltoImpresion = document.getElementById("AltoImpresion").value;
    
    if (Impresora != null & Impresora != "" & CopiasProduccion != null & CopiasProduccion != "" & CopiasDesperdicio != null & CopiasDesperdicio != "" &
        TipoImpresion != null & TipoImpresion != "" & AnchoImpresion != null & AnchoImpresion != "" & AltoImpresion != null & AltoImpresion != "") {

        CopiasProduccion = parseInt(CopiasProduccion);
        CopiasDesperdicio = parseInt(CopiasDesperdicio);

        var filter = {
            Impresora: Impresora,
            CopiasProduccion: CopiasProduccion,
            CopiasDesperdicio: CopiasDesperdicio,
            TipoImpresion: TipoImpresion,
            AnchoImpresion: AnchoImpresion,
            AltoImpresion: AltoImpresion
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarConfiguracion", function (data) {

            $('#ModalConfiguracion').modal('hide');
            alert("Cambios almacenados correctamente");
        }, errorHandler, JSON.stringify(filter))
    }

    else
        alert("Todos los campos deben contener información");

    
}
function SaveBanderas(Banderas, Proceso){
    
    if (IDBascula != null && IDBascula != "")
    {
        //Bandera Negro
        var BasculaBanderas =
            {
                COLORBANDERA: "Negro",
                CANTIDADBANDERAS: Banderas.Negro,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Rojo
        var BasculaBanderas =
            {
                COLORBANDERA: "Rojo",
                CANTIDADBANDERAS: Banderas.Rojo,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Amariillo
        var BasculaBanderas =
            {
                COLORBANDERA: "Amarillo",
                CANTIDADBANDERAS: Banderas.Amarillo,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Azul
        var BasculaBanderas =
            {
                COLORBANDERA: "Azul",
                CANTIDADBANDERAS: Banderas.Azul,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Verde
        var BasculaBanderas =
            {
                COLORBANDERA: "Verde",
                CANTIDADBANDERAS: Banderas.Verde,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))
    }   
}
function SaveObservacion(Observacion, Proceso) {

    if (IDBascula != null && IDBascula != "")
    {
        //Bandera Amariillo
        var BasculaExtras =
            {
                NOMBRECAMPO: "Observaciones",
                VALORCAMPO: Observacion,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULAEXTRASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaExtras))
    }
}


function ConsultarTaraPorOrden($OrdenProduccion, $CodigoMaquina, $OpcionProducion) {
    
    var filter = {
        OrdenDeProduccion: $OrdenProduccion,
        CodigoPuesto: $CodigoMaquina
    }
        
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetUltimaTaraPorOrdenProduccion", function (data) {
        
        if (data.value != null) {
            var str = data.value;
            tempTara = str.replace(",", ".");
            document.getElementById("tara").value = "";

            if ($OpcionProducion == "SI")
                document.getElementById("tara").value = tempTara;
            else if ($OpcionProducion == "NO")
                document.getElementById("tara").value = 0;
            else
                document.getElementById("tara").value = "";
        }
    }, errorHandler, JSON.stringify(filter))
}

function ConsultarSiAplicaLimites($CodigoProceso) {
    
    var codigos = ['PSELLA1', 'PSELLA2', 'PSELLA4', 'PWICKET', 'PVALVUL'];//Area de sellado
    aplicalimite = codigos.includes($CodigoProceso)
}

function CerrarModalMensaje() {

    $('#ModalMensajes').modal('hide');
    $("#metros").focus();
}

function ConsultarListaDeCarretes() {

    limpiarcarretes();
    if (maquina != null && maquina != "") {
        
        //var filter = {
        //    CodigoPuesto: maquina
        //}

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/GetListaDeCarretesPorPuestoDeTrabajo",
            //data: JSON.stringify(filter),
            success: function (data) {
                
                var carrete = $("#carrete").data("kendoDropDownList");
                carrete.dataSource.data(data.value);
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
}
function limpiarcarretes() {
    var carrete = $("#carrete").data("kendoDropDownList");
    carrete.dataSource.data([]); 
    carrete.text("");
    carrete.value(""); 
}
function listadecarretesedicion(){
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/GetListaDeCarretes",
        success: function (data) {

            var grid = $("#gridlistacarrete").data("kendoGrid");
            if (grid != undefined) {
                $("#gridlistacarrete").empty();
            }
            
            $("#gridlistacarrete").kendoGrid({
                dataSource: {
                    data: data.value,
                    schema: {
                        model: {
                            fields: {
                                ID: { type: "int" },
                                NOMBRE: { type: "string" },
                                VALOR: { type: "number" },
                                DESCRIPCION: { type: "string" },
                                ESTADO: { type: "bool" },
                                TIMESPAN: { type: "datetime" },
                                CODIGOCELULA: { type: "string" }
                            }
                        }
                    },
                },
                height: 300,
                scrollable: true,
                sortable: true,
                filterable: true,
                selectable: "true",
                change: vieweditarcarrete,
                columns: [
                    { field: "NOMBRE", title: "Código", width: "20%", filterable: { multi: true } },
                    { field: "VALOR", title: "Valor (kg)", width: "20%", filterable: false },
                    { field: "DESCRIPCION", title: "Descripción", width: "30%", filterable: false },
                    { field: "ESTADO", title: "Estado", width: "10%", filterable: false },
                    { field: "CODIGOCELULA", title: "Operación", width: "20%", filterable: { multi: true } }
                ]
            });
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}
function vieweditarcarrete() {

    listarcelulascarrete();
    var grid = $("#gridlistacarrete").data("kendoGrid");
    var itemgrid = grid.dataItem(grid.select());
    $('#ModalEdicionCarrete').modal('show');
    idcarrete = itemgrid.ID;
    document.getElementById("nombrecarrete").value = itemgrid.NOMBRE;
    document.getElementById("valorcarrete").value = itemgrid.VALOR;
    document.getElementById("descripcioncarrte").value = itemgrid.DESCRIPCION;
    $("#estadocarrete").data('kendoDropDownList').value(itemgrid.ESTADO);
    $("#celulacarrete").data('kendoDropDownList').value(itemgrid.CODIGOCELULA);
    document.getElementById("BtnguardarCarrete").textContent = "Editar";
    document.getElementById("Btneliminarcarrete").disabled = false;
}

function listarcelulascarrete() {

    var celulas = $("#celulacarrete").data("kendoDropDownList");
    celulas.dataSource.data([]);
    celulas.text("");
    celulas.value("");
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/GetListaCelulas",
        success: function (data) {

                var celulas = $("#celulacarrete").data("kendoDropDownList");
                celulas.dataSource.data(data.value);
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}

function editarcarrete() {

    if (document.getElementById("nombrecarrete").value == null || document.getElementById("nombrecarrete").value == "")
        toastr.warning("Debe ingresar un código para la carreta", "Información");
    else if (document.getElementById("valorcarrete").value == null || document.getElementById("nombrecarrete").value == "")
        toastr.warning("Debe ingresar un valor (kg) para la carreta", "Información");
    else if ($("#celulacarrete").data('kendoDropDownList').value() == 0 || $("#celulacarrete").data('kendoDropDownList').value() == null)
        toastr.warning("Debe seleccionar un código de proceso para la carreta", "Información");

    else if (document.getElementById("BtnguardarCarrete").textContent == "Guardar") {

        var filter = {
            Id: 0,
            Nombre: document.getElementById("nombrecarrete").value,
            Valor: document.getElementById("valorcarrete").value,
            Estado: $("#estadocarrete").data('kendoDropDownList').value() == "true" ? true : false,
            Descripcion: document.getElementById("descripcioncarrte").value,
            CodigoCelula: $("#celulacarrete").data('kendoDropDownList').value()
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EditarCarrata",
            data: JSON.stringify(filter),
            success: function (data) {

                toastr.success("La carreta ha sido creada correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');

                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }

    else { 

        var filter = {
            Id: idcarrete,
            Nombre: document.getElementById("nombrecarrete").value,
            Valor: document.getElementById("valorcarrete").value,
            Estado: $("#estadocarrete").data('kendoDropDownList').value() == "true" ? true : false,
            Descripcion: document.getElementById("descripcioncarrte").value,
            CodigoCelula: $("#celulacarrete").data('kendoDropDownList').value()
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EditarCarrata",
            data: JSON.stringify(filter),
            success: function (data) {

                idcarrete = 0;
                toastr.success("La carreta ha sido editado correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');

                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
}

function crearnuevocarrete() {
    
    $('#ModalEdicionCarrete').modal('show');
    listarcelulascarrete();
    document.getElementById("nombrecarrete").value = "";
    document.getElementById("valorcarrete").value = 0;
    document.getElementById("descripcioncarrte").value = "";
    $("#estadocarrete").data('kendoDropDownList').value(true);
    document.getElementById("BtnguardarCarrete").textContent = "Guardar";
    document.getElementById("Btneliminarcarrete").disabled = true;
}

function eliminarcarrete() {

    if (idcarrete != 0) {

        var filter = {
            Id: idcarrete
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EliminarCarreta",
            data: JSON.stringify(filter),
            success: function (data) {

                idcarrete = 0;
                toastr.success("La carreta ha sido eliminada correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');
                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
    else
        toastr.warning("No se puede eliminar la carreta", "Información");
}

function ConsultarInformacionDevoluciones($codigoorden, $codigoproceso) {
    var filter = {
        CodigoOrdenProduccion: $codigoorden,
        CodigoProceso: $codigoproceso
    }

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/ConsultarMaterialesPorJob",
        data: JSON.stringify(filter),
        success: function (data) {
            if (data != null) {
                document.getElementById("assembly").value = data.Assembly;
                document.getElementById("secuenciamaterial").value = data.Secuencia;
                document.getElementById("materialemitido").value = data.Material;
                document.getElementById("materialdescripcion").value = data.DescripcionMaterial;
                document.getElementById("lote").value = data.Lote;
                document.getElementById("notadevolucion").value = "";
            }
            else
                toastr.warning("No existe información de materiales por job el registro, contactar con el administrador");
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}

function pad(number, length) {

    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;

}
