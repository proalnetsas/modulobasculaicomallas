﻿var baseUrl = "../odata/";
var timeCapture = 3000;
var today = new Date();
var produccion = new Array();
produccion.push({ value: "SI", text: "PRODUCCION" });
produccion.push({ value: "NO", text: "DESPERDICIO" });
produccion.push({ value: "devolucion", text: "DEVOLUCION" });
var maquinas;
var codigoTurno;
var codigoBatch;

var TipoImpresion;
var ServerTipoImpresion;
var opimpresion;
var pesobrutoimpresion;
var pesonetoimpresion;
var rolloimpresion;
var metrosimpresion;
var item;
var operario;
var maquina;
var referenciaproducto;
var function_Login;
var cantidaddebobinaproduccion;
var IDBascula = null;
var consecutivo;

var nombremaquina;
var codigooperario;

var tempTara;

var aplicalimite = false;
var codigoProceso;

var nombreimpresora;
var esquemadeimpresion;
var codigodespercio;
var nombrecliente;
var codigocliente;
var nitcliente;

var warehousecode;
var mesesdepuesvencimiento;

var tipodeimpresion;
var basculamanual = false

var tipoimpresionedicio;
var nuevoregistroetiquetaedicio = false;

var idcarrete = 0;
var idestibaabierta = 0;

var valorrepeticiones = 0;
var valorrepeticionesimpresion = "";

var camposocultos = "false";

$(document).ready(function () {

    obtenerPeso();

    //Cultura español de kendo
    kendo.culture("es-CO");
    activeClass("CapturaProdDesp", "ParentIngreso", "liIngreso");

    $("#datepicker").kendoDateTimePicker({
        value: today
    }).data("kendoDateTimePicker");
   
    $("#produccion").kendoDropDownList({
        optionLabel: "Seleccione ...",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: {
            data: produccion
        },
        select: selecProduccion,
    });
    $("#recurso").kendoDropDownList({
        optionLabel: "Seleccione recurso ...",
        dataTextField: "CODIGOPUESTO",
        dataValueField: "NOMBREPUESTO",
        change: selecRecurso
    });

    $("#carrete").kendoDropDownList({
        optionLabel: "Seleccione carrete ...",
        dataTextField: "NOMBRE",
        dataValueField: "VALOR"
    });

    $("#celulacarrete").kendoDropDownList({
        optionLabel: "Seleccione proceso ...",
        dataTextField: "CODIGOCELULA",
        dataValueField: "CODIGOCELULA"
    });
    $("#estadocarrete").kendoDropDownList({});
    
    
    $("#motdesperdicio").kendoDropDownList({
        optionLabel: "Seleccione el motivo ...",
        dataTextField: "NOMBRE",
        dataValueField: "CODIGO",
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: baseUrl + "CAUSALESDESPERDICIOs",
                }
            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
            },
        }
    });

    //Armar GridBanderas
    ObternerTipoImpresion();
    ArmarGridBanderas();

    $("#OpcionDeImpresion").kendoDropDownList();


    //Validación para ocultar configuración de manera local
    if (this.location.hostname == "localhost") {
        //document.getElementById("BtEliminarBobinas").style.visibility = "hidden";
        //document.getElementById("BtnConfiguracion").style.visibility = "hidden";
        document.getElementById("BtnConfiguracionCarrete").style.visibility = "hidden";
    }
});

function selecRecurso(e) {
    
    if ($("#recurso").val()) {
        var recurso = $("#recurso").data("kendoDropDownList");
        getDetalle(recurso.text());
    }
}
function getDetalle(codigoRecurso) {
    
    $("#txtError").html("");
    var op = $("#ordenproduccion").val();
    if (!validaTextoOP(op))
        return;
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    var filter = {
        OrdenProduccion: op,
        Fecha: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
        CodigoPuesto: codigoRecurso
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetDetalleOP", function (data) {

        if (data.RequiereCambioOperario == false) {

            $("#operario").val(data.Operario);
            $("#turno").val(data.Turno);
            codigoTurno = data.CodigoTurno;
            $("#referencia").val(data.Referencia);
            codigoBatch = data.CodigoEntradaEjecucion;

            item = data.CodigoProducto;
            operario = data.Operario;
            maquina = data.CodigoPuesto;
            referenciaproducto = data.Referencia;

            nombremaquina = data.NombreMaquina;
            codigooperario = data.CodigoOperario;

            codigoProceso = data.CodigoProceso;

            getConsecutivoBobina(maquina, op);

            /*ACTUALIZAR FECHA*/
            //if (basculamanual == false) {
            //    var todayDate = kendo.toString(kendo.parseDate(new Date()));
            //    $("#datepicker").data("kendoDateTimePicker").value(todayDate);
            //}

            ConsultarTaraPorOrden(op, codigoRecurso, document.getElementById("produccion").value);
            ConsultarSiAplicaLimites(codigoProceso);
            ConsultarEsquemaImpresion(codigoRecurso);

            ObtenerInformacionCliente(op, item);

            ConsultarListaDeCarretes();

            //Validar si es devolución
            if (document.getElementById("produccion").value == "devolucion")
                ConsultarInformacionDevoluciones(op, codigoProceso);
        }
        else {
            limpiar();
            toastr.error("Debe corregir la información del el ID Empleado desde el Módulo Dispositivo antes de poder ingresar bobinas para este BATCH", "Error");
        }
            

        

    }, errorHandler, JSON.stringify(filter))
}
function ObtenerInformacionCliente($op, $codigoproducto) {

    var filter = {
        OrdenProduccion: $op,
        CodigoProucto: $codigoproducto
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

        if (data != null) {
            nombrecliente = data.NombreCliente;
            codigocliente = data.CodigoCliente;
            nitcliente = data.NitCliente;
        }

    }, errorHandler, JSON.stringify(filter))
}
function ConsultarEsquemaImpresion($codigopuesto) {

    var filter = {
        CodigoPuesto: $codigopuesto
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetEsquemaDeImpresion", function (data) {
        if (data != null) {
            
            var jsontext = data.value;
            esquemadeimpresion = JSON.parse(jsontext);

            //Validacion para peso repeticiones y proceso siguiente
            if (esquemadeimpresion.TipoImpresion == "CORTE") {

                var filter = {
                    TipoImpresion: esquemadeimpresion.TipoImpresion,
                    OrdenDeProduccion: $("#ordenproduccion").val(),
                    CodigoProducto: item,
                    CodigoPuesto: maquina,
                    CodigoProceso: codigoProceso
                }
                request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionExtraImpresion", function (data) {
                    if (data != null) {
                        var value = JSON.parse(data.value);
                        if (value.Repeticiones != null && value.Repeticiones != "" && value.Repeticiones != 0)
                            valorrepeticiones = parseFloat(value.Repeticiones);
                        else
                            valorrepeticiones = 0;

                        if (document.getElementById("produccion").value == "SI") {
                            document.getElementById("contenedorrepeticiones").style.display = "";
                            document.getElementById("contenedorprocesosiguiente").style.display = "";
                        }
                    }
                }, errorHandler, JSON.stringify(filter))
            }
            else {
                document.getElementById("contenedorrepeticiones").style.display = "none";
                document.getElementById("contenedorprocesosiguiente").style.display = "none";
            }
        }
    }, errorHandler, JSON.stringify(filter))
}
function getConsecutivoBobina(codigoRecurso, Op)
{
    var filter = {
        OrdenProduccion: Op,
        CodigoPuesto: codigoRecurso
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetConsecutivoBobina", function (data) {

        cantidaddebobinaproduccion = (data.value + 1);
        if (document.getElementById("produccion").value == "NO")
        {
            document.getElementById("bobina").value = 0;
        }

        else
            document.getElementById("bobina").value = cantidaddebobinaproduccion;

    }, errorHandler, JSON.stringify(filter))
}
function selecProduccion(e) {
    var dataItem = this.dataItem(e.item.index());
    var btn = $("#btnGuardar");
    var desp = $("#desperdicio");
    btn.addClass("displaynone");
    desp.addClass("displaynone");
    if (dataItem.value) {
        btn.removeClass("displaynone");
        if (dataItem.value == "NO") {
            desp.removeClass("displaynone");
        }
    }
}
function buscarOP() {
    
    limpiarcarretes();
    $("#txtError").html("");
    var op = $("#ordenproduccion").val();
    if (!validaTextoOP(op))
        return;
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    var filter = {
        OrdenProduccion: op,
        Fecha: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
    }

    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetMaquinasOP", function (data) {
        if (data.value.length == 0) {
            $("#txtError").html("No existe información de esta orden de producción en el turno actual");
            return;
        }

        maquinas = data.value;
        var recurso = $("#recurso").data("kendoDropDownList");
        recurso.dataSource.data(maquinas);
        //Si es solo un recurso lo debemos seleccionar para que traiga los datos de la op
        if (maquinas.length == 1) {
            recurso.select(function (dataItem) {
                return dataItem.CODIGOPUESTO === maquinas[0].CODIGOPUESTO;
            });
            getDetalle(maquinas[0].CODIGOPUESTO);
        }
    }, errorHandler, JSON.stringify(filter))
}
//Función para hacer ajaxrequest
function request(contenttype, tokentype, token, type, url, sucess, error, data) {
    $.ajax({
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader("Content-Type", contenttype);
            if (tokentype)
                xhrObj.setRequestHeader("Authorization", tokentype + " " + token);
        },
        type: type,
        url: url,
        data: data,
        dataType: "json",
        success: sucess,
        error: error
    });
}
//Función para controlar error de llamados ajax
function errorHandler(jqXHR, exception) {
    
    var txt = '';
    if (jqXHR.status === 0) {
        txt = 'No se puede encontrar la ruta especificada.';
    } else if (jqXHR.status == 404) {
        txt = 'La página solicitada no se encuentra. [404]';
    } else if (jqXHR.status == 500) {
        txt = 'Error interno del servidor. [500]';
    } else if (exception === 'parsererror') {
        txt = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        txt = 'Error de tiempo de espera agotado';
    } else if (exception === 'abort') {
        txt = 'Ajax request aborted.';
    }
    else {
        if (jqXHR.responseJSON) {
            if (jqXHR.responseJSON["odata.error"]) {
                var error = '';
                if (jqXHR.responseJSON["odata.error"].message) {
                    error = jqXHR.responseJSON["odata.error"].message.value;
                }
                if (jqXHR.responseJSON["odata.error"].innererror) {
                    error = error + " " + jqXHR.responseJSON["odata.error"].innererror.message;
                }
                if (error != "")
                    txt = error;
                else
                    txt = 'Error no controlado.\n' + jqXHR.responseText;
            }
            else
                txt = 'Error no controlado.\n' + jqXHR.responseText;
        }
        else
            txt = 'Error no controlado.\n' + jqXHR.responseText;
    }
    $("#txtError").html(txt);
}
function guardar() {

    var pesocarrete = $("#carrete").val();
    if (pesocarrete == "") {
        toastr.warning("Debe seleccionar una opción de carrete", "Información");
        document.getElementById("btnGuardar").disabled = false;
        return;
    }

    if (!tara)
        tara = 0;
    if (!pesoneto)
        pesoneto = 0;
    if (!pesocarrete)
        pesocarrete = 0;
    var peso = pesoneto - (parseFloat(tara) + parseFloat(pesocarrete));

    calculoPeso(this);
    document.getElementById("btnGuardar").disabled = true;
    /*Valores para la impresion*/
    opimpresion = "";
    pesobrutoimpresion = "";
    pesonetoimpresion = "";
    rolloimpresion = "";
    metrosimpresion = "";
    opimpresion = $("#ordenproduccion").val();

    var pesocarrete = $("#carrete").val();
    if (!pesocarrete)
        pesocarrete = 0;
    pesobrutoimpresion = String(parseFloat($("#pesoneto").val()) - pesocarrete);

    pesonetoimpresion = $("#peso").val();
    rolloimpresion = $("#bobina").val();
    metrosimpresion = $("#metros").val();

    $("#txtError").html("");
    var op = $("#ordenproduccion").val();

    if (!validaTextoOP(op) || !validaValores())
        return;
    if (!validarInicioTurno())
        return;
    var fecha = $("#datepicker").data("kendoDateTimePicker").value();
    var motivo = "";
    var desperdicio = $("#motdesperdicio").data("kendoDropDownList");
    var puestotrabajo = $("#recurso").data("kendoDropDownList");
    var produccion = true;
    if ($("#produccion").val() == "NO") {
        produccion = false;
        if (desperdicio.value() == "") {
            $("#txtError").html("Se debe seleccionar el motivo de desperdicio");
            document.getElementById("btnGuardar").disabled = false;
            return;
        }
        motivo = desperdicio.text();
    }

    var PesoTotalBas = parseFloat(document.getElementById("peso").value);
    PesoTotalBas = PesoTotalBas.toFixed(2);

    codigodespercio = null;
    if ($("#produccion").val() == "NO") {
        if (desperdicio.value() == "") {
            $("#txtError").html("Se debe seleccionar el motivo de desperdicio");
            document.getElementById("btnGuardar").disabled = false;
            return;
        }
        codigodespercio = desperdicio.value();
    }

    var procesosiguiente_ = $("#procesosiguiente").val();
    if (procesosiguiente_ == "" && document.getElementById("contenedorprocesosiguiente").style.display == "" && $("#produccion").val() == "SI") {
        toastr.warning("Debe seleccionar una opción de proceso siguiente", "Información");
        document.getElementById("btnGuardar").disabled = false;
        return;
    }

    if (aplicalimite == true && produccion == true) {//Validación para limite

        var filter = {
            CodigoPuesto: puestotrabajo.text(),
            CodigoProceso: codigoProceso,
            CodigoProducto: item,
            PesoNeto: PesoTotalBas
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetConsultarLimitesParaSellado", function (data) {
            if (data != null) {
                debugger;
                //Validar si el valor de metros es valido por sus limites
                if (parseFloat($("#metros").val()) < data.value[0] || parseFloat($("#metros").val()) > data.value[1]) {

                    document.getElementById("btnGuardar").disabled = false;

                    var texto = "Peso unitario producto final: " + data.value[2] + " Kg/u";
                    document.getElementById("TxtModalMensaje").innerText = "Error al intentar almacenar el registro. " + "\r\n " + "Verifique, ya sea la cantidad digitada en relación con el peso capturado; o en su caso, el peso unitario almacenado en el sistema. " + "\r\n " + texto;
                    $('#ModalMensajes').modal('show');
                }
                else {

                    //PROCESO PARA REGISTRAR EPT
                    if (esquemadeimpresion.TipoImpresion == "CORTE" && document.getElementById("procesosiguiente").value == "EPT") {
                        //Valida si puede pesar por la estiba

                        var filter = {
                            ordendeproduccion: op,
                            codigoproducto: item,
                            codigoproceso: codigoProceso,
                            codigomaquina: puestotrabajo.text()
                        }

                        $.ajax({
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            url: baseUrl + "TUPLAEJECUCIONs/ConsultarEstibaAbierta",
                            data: JSON.stringify(filter),
                            success: function (data) {

                                if (data.value == 0) {
                                    toastr.warning("Es necesario que ingrese al módulo de EPT y abra una estiba para la JOB antes de poder crear la etiqueta.", "Información", { timeOut: 8000 });
                                    document.getElementById("btnGuardar").disabled = false;
                                }
                                else {

                                    idestibaabierta = data.value;

                                    var filter = {
                                        idestibaencabezado: parseInt(idestibaabierta),
                                        codigoproducto: item
                                    }

                                    $.ajax({
                                        dataType: "json",
                                        contentType: "application/json; charset=utf-8",
                                        type: "POST",
                                        url: baseUrl + "TUPLAEJECUCIONs/ConsultarEstibaAtope",
                                        data: JSON.stringify(filter),
                                        success: function (data) {

                                            if (data.value == false){
                                                toastr.warning("Es necesario que se dirija al módulo de EPT, cerrar y abrir una nueva estiba para poder continuar.", "Información", { timeOut: 5000 });
                                                document.getElementById("btnGuardar").disabled = false;
                                            }
                                            else {

                                                var bascula = {
                                                    FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
                                                    CODIGOORDENPRODUCCION: op,
                                                    TURNO: $("#turno").val(),
                                                    NOMBREPUESTO: $("#recurso").val(),
                                                    REFERENCIA: $("#referencia").val(),
                                                    PRODUCCION: produccion,
                                                    BOBINA: $("#bobina").val(),
                                                    TARA: $("#tara").val(),
                                                    PESO: PesoTotalBas,
                                                    METROS: $("#metros").val(),
                                                    CAUSALDESPERDICIO: motivo,
                                                    CODIGOPUESTO: puestotrabajo.text(),
                                                    CODIGOTURNO: codigoTurno,
                                                    CODIGOENTRADAEJECUCION: codigoBatch,
                                                    ELIMINADO: false,
                                                    CODIGODESPERDICIO: codigodespercio
                                                };

                                                request("application/json", null, null, "POST", baseUrl + "BASCULAs", function (data) {

                                                    document.getElementById("btnGuardar").disabled = false;
                                                    IDBascula = data.ID;
                                                    //IDBascula = 0;

                                                    if (esquemadeimpresion.TipoImpresion == "CORTE")
                                                        valorrepeticionesimpresion = document.getElementById("repetiones").value;

                                                    limpiar();

                                                    //Determinación de tipo de impresión
                                                    if (produccion == false)
                                                        ImprimirDesperdicio();
                                                    else {
                                                        AlmacenarRegistroEPT();
                                                        ImprimirProduccion();
                                                    }

                                                }, errorHandler, JSON.stringify(bascula))
                                            }
                                        }
                                    });
                                }
                            },
                            error: function (ajaxContext) {
                                $('#ModalMensaje').modal('hide');
                                ajaxContext.responseJSON.ExceptionMessage;
                            }
                        });
                    }

                    //REGISTRO DE DEVOLUCIÓN
                    else if (document.getElementById("produccion").value == "devolucion") {
                        
                        if (document.getElementById("secuenciamaterial").value == "" ||
                            document.getElementById("assembly").value == "" || document.getElementById("materialemitido").value == "") {
                            toastr.warning("No hay información de materiales para continuar", "Información");
                            document.getElementById("btnGuardar").disabled = false;
                            return;
                        }
                        
                        var filter = {
                            OrdenDeProduccion: op,
                            CodigoProducto: $("#materialemitido").val(),
                            CodigoQrTexto: "",
                            CodigoMaquina: puestotrabajo.text(),
                            Fecha: "",
                            NombreOperario: codigooperario,
                            CodigoQrRuta: "",
                            Assembly: $("#assembly").val(),
                            Producto: $("#materialdescripcion").val(),
                            Lote: $("#lote").val(),
                            Cliente: "DEVOLUCION MATERIA PRIMA A BODEGA",
                            Secuencia: $("#secuenciamaterial").val(),
                            CodigoProceso: codigoProceso,
                            Nota: document.getElementById("notadevolucion").value,
                            BodegaTo: "BMP",
                            BinTo: "SALDOS",
                            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
                            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
                            Impresora: nombreimpresora,
                            Tara: parseFloat(parseFloat((parseFloat(pesobrutoimpresion) - parseFloat(pesonetoimpresion))).toFixed(2)).toFixed(2),
                            Cantidad: parseInt(metrosimpresion).toFixed(0)
                        };
                        
                        $.ajax({
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            url: baseUrl + "TUPLAEJECUCIONs/AlmacenarRegistroDevolucion",
                            data: JSON.stringify(filter),
                            success: function (data) {
                                
                                limpiar();
                                document.getElementById("btnGuardar").disabled = false;
                                toastr.success("Se ha agregado el registro de devolución correctamente", "Información");
                            },
                            error: function (ajaxContext) {
                                document.getElementById("btnGuardar").disabled = false;
                                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
                            }
                        });
                    }

                    else {

                        var bascula = {
                            FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
                            CODIGOORDENPRODUCCION: op,
                            TURNO: $("#turno").val(),
                            NOMBREPUESTO: $("#recurso").val(),
                            REFERENCIA: $("#referencia").val(),
                            PRODUCCION: produccion,
                            BOBINA: $("#bobina").val(),
                            TARA: $("#tara").val(),
                            PESO: PesoTotalBas,
                            METROS: $("#metros").val(),
                            CAUSALDESPERDICIO: motivo,
                            CODIGOPUESTO: puestotrabajo.text(),
                            CODIGOTURNO: codigoTurno,
                            CODIGOENTRADAEJECUCION: codigoBatch,
                            ELIMINADO: false,
                            CODIGODESPERDICIO: codigodespercio
                        };

                        request("application/json", null, null, "POST", baseUrl + "BASCULAs", function (data) {

                            document.getElementById("btnGuardar").disabled = false;
                            IDBascula = data.ID;
                            //IDBascula = 0;

                            if (esquemadeimpresion.TipoImpresion == "CORTE")
                                valorrepeticionesimpresion = document.getElementById("repetiones").value;

                            limpiar();

                            //Determinación de tipo de impresión
                            if (produccion == false)
                                ImprimirDesperdicio();
                            else
                                ImprimirProduccion();
                        }, errorHandler, JSON.stringify(bascula))
                    }
                }
            }
            else {

                document.getElementById("btnGuardar").disabled = false;
                document.getElementById("TxtModalMensaje").innerText = "Error al calcular la lista de limites para el proceso de de sellado Factor Variable 1, validar con el administrador";
                $('#ModalMensajes').modal('show');
            }
        }, errorHandler, JSON.stringify(filter))
    }

    else {
        
        if (esquemadeimpresion.TipoImpresion == "CORTE" && document.getElementById("procesosiguiente").value == "EPT") {

            //Valida si puede pesar por la estiba
            var filter = {
                ordendeproduccion: op,
                codigoproducto: item,
                codigoproceso: codigoProceso,
                codigomaquina: puestotrabajo.text()
            }

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/ConsultarEstibaAbierta",
                data: JSON.stringify(filter),
                success: function (data) {

                    if (data.value == 0) {
                        toastr.warning("Es necesario que ingrese al módulo de EPT y abra una estiba para la JOB antes de poder crear la etiqueta.", "Información", { timeOut: 5000 });
                        document.getElementById("btnGuardar").disabled = false;
                    }
                        
                    else {

                        idestibaabierta = data.value;

                        var filter = {
                            idestibaencabezado: parseInt(idestibaabierta),
                            codigoproducto: item
                        }

                        $.ajax({
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            url: baseUrl + "TUPLAEJECUCIONs/ConsultarEstibaAtope",
                            data: JSON.stringify(filter),
                            success: function (data) {

                                if (data.value == false){
                                    toastr.warning("Es necesario que se dirija al módulo de EPT, cerrar y abrir una nueva estiba para poder continuar.", "Información", { timeOut: 5000 });
                                    document.getElementById("btnGuardar").disabled = false;
                                }
                                else {

                                    var bascula = {
                                        FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
                                        CODIGOORDENPRODUCCION: op,
                                        TURNO: $("#turno").val(),
                                        NOMBREPUESTO: $("#recurso").val(),
                                        REFERENCIA: $("#referencia").val(),
                                        PRODUCCION: produccion,
                                        BOBINA: $("#bobina").val(),
                                        TARA: $("#tara").val(),
                                        PESO: PesoTotalBas,
                                        METROS: $("#metros").val(),
                                        CAUSALDESPERDICIO: motivo,
                                        CODIGOPUESTO: puestotrabajo.text(),
                                        CODIGOTURNO: codigoTurno,
                                        CODIGOENTRADAEJECUCION: codigoBatch,
                                        ELIMINADO: false,
                                        CODIGODESPERDICIO: codigodespercio
                                    };

                                    request("application/json", null, null, "POST", baseUrl + "BASCULAs", function (data) {

                                        document.getElementById("btnGuardar").disabled = false;
                                        IDBascula = data.ID;
                                        //IDBascula = 0;

                                        if (esquemadeimpresion.TipoImpresion == "CORTE")
                                            valorrepeticionesimpresion = document.getElementById("repetiones").value;

                                        limpiar();

                                        //Determinación de tipo de impresión
                                        if (produccion == false)
                                            ImprimirDesperdicio();
                                        else {
                                            AlmacenarRegistroEPT();
                                            ImprimirProduccion();
                                        }


                                    }, errorHandler, JSON.stringify(bascula))
                                }
                            }
                        });
                    }
                },
                error: function (ajaxContext) {
                    $('#ModalMensaje').modal('hide');
                    ajaxContext.responseJSON.ExceptionMessage;
                }
            });
        }

            ///REGISTRO DE DEVOLUCIÓN
        else if (document.getElementById("produccion").value == "devolucion") {
            if (document.getElementById("secuenciamaterial").value == "" ||
                document.getElementById("assembly").value == "" || document.getElementById("materialemitido").value == "") {
                toastr.warning("No hay información de materiales para continuar", "Información");
                document.getElementById("btnGuardar").disabled = false;
                return;
            }
            
            var filter = {
                OrdenDeProduccion: op,
                CodigoProducto: $("#materialemitido").val(),
                CodigoQrTexto: "",
                CodigoMaquina: puestotrabajo.text(),
                Fecha: "",
                NombreOperario: codigooperario,
                CodigoQrRuta: "",
                Assembly: $("#assembly").val(),
                Producto: $("#materialdescripcion").val(),
                Lote: $("#lote").val(),
                Cliente: "DEVOLUCION MATERIA PRIMA A BODEGA",
                Secuencia: $("#secuenciamaterial").val(),
                CodigoProceso: codigoProceso,
                Nota: document.getElementById("notadevolucion").value,
                BodegaTo: "BMP",
                BinTo: "SALDOS",
                PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
                PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
                Impresora: nombreimpresora,
                Tara: parseFloat(parseFloat((parseFloat(pesobrutoimpresion) - parseFloat(pesonetoimpresion))).toFixed(2)).toFixed(2),
                Cantidad: parseInt(metrosimpresion).toFixed(0)
            };

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/AlmacenarRegistroDevolucion",
                data: JSON.stringify(filter),
                success: function (data) {

                    limpiar();
                    document.getElementById("btnGuardar").disabled = false;
                    toastr.success("Se ha agregado el registro de devolución correctamente", "Información");
                },
                error: function (ajaxContext) {
                    document.getElementById("btnGuardar").disabled = false;
                    toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
                }
            });
        }

        else {

            var bascula = {
                FECHA: kendo.toString(fecha, "yyyy-MM-ddTHH:mm:ss"),
                CODIGOORDENPRODUCCION: op,
                TURNO: $("#turno").val(),
                NOMBREPUESTO: $("#recurso").val(),
                REFERENCIA: $("#referencia").val(),
                PRODUCCION: produccion,
                BOBINA: $("#bobina").val(),
                TARA: $("#tara").val(),
                PESO: PesoTotalBas,
                METROS: $("#metros").val(),
                CAUSALDESPERDICIO: motivo,
                CODIGOPUESTO: puestotrabajo.text(),
                CODIGOTURNO: codigoTurno,
                CODIGOENTRADAEJECUCION: codigoBatch,
                ELIMINADO: false,
                CODIGODESPERDICIO: codigodespercio
            };

            request("application/json", null, null, "POST", baseUrl + "BASCULAs", function (data) {

                document.getElementById("btnGuardar").disabled = false;
                IDBascula = data.ID;
                //IDBascula = 0;

                if (esquemadeimpresion.TipoImpresion == "CORTE")
                    valorrepeticionesimpresion = document.getElementById("repetiones").value;

                limpiar();

                //Determinación de tipo de impresión
                if (produccion == false)
                    ImprimirDesperdicio();
                else
                    ImprimirProduccion();
            }, errorHandler, JSON.stringify(bascula))
        }
    }
}
function ImprimirDesperdicio() {

    var filter = {
        OrdenDeProduccion: opimpresion,
        Cliente: nombrecliente,
        ParteyDescripcionProducto: item + " " + referenciaproducto,
        ParteyDescripcionDesperdicio: "",
        CausalDesperdicio: $("#motdesperdicio").data("kendoDropDownList").text(),
        CodigoQrTexto: "",
        CodigoQrRuta: "",
        NombreRecurso: nombremaquina,
        CodigoProceso: codigoProceso,
        NombreEmpleado: operario,
        Fecha: "",
        PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
        Metros: parseInt(metrosimpresion).toString(),
        Unidad: "",
        CodigoPuesto: maquina,
        CodigoProducto: item,
        Impresora: nombreimpresora,
        NumeroCopias: "1"
    }
    $('#ModalMensaje').modal('show');

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/PrintDesperdicio",
        data: JSON.stringify(filter),
        success: function (data) {
            $('#ModalMensaje').modal('hide');
        },
        error: function (ajaxContext) {
            $('#ModalMensaje').modal('hide');
            alert(ajaxContext.responseJSON["odata.error"].innererror.message);
        }
    });
};
function ImprimirProduccion() {
    debugger;
    LimpiarPreImpresion(esquemadeimpresion.TipoImpresion);
    if (esquemadeimpresion.TipoImpresion == "COEXTRUSION") {
        ConsultarInformacionImpresion(esquemadeimpresion.TipoImpresion);
        //warehousecode = esquemadeimpresion.WareHouseCode;
        if (esquemadeimpresion.Reportes.length == 1) {
            mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
            warehousecode = esquemadeimpresion.Reportes[0].WareHouseCode;
            $('#ModalExtrusionImpresion').modal('show');
        }
    }
    else if (esquemadeimpresion.TipoImpresion == "FLEXO") {
        ConsultarInformacionImpresion(esquemadeimpresion.TipoImpresion);
        //warehousecode = esquemadeimpresion.WareHouseCode;
        if (esquemadeimpresion.Reportes.length == 1) {
            mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
            warehousecode = esquemadeimpresion.Reportes[0].WareHouseCode;
            $('#ModalFlexoImpresion').modal('show');
        }
    }
    else if (esquemadeimpresion.TipoImpresion == "CORTE") {
        ConsultarInformacionImpresion(esquemadeimpresion.TipoImpresion);
        //warehousecode = esquemadeimpresion.WareHouseCode;
        if (esquemadeimpresion.Reportes.length == 1) {
            mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
            $('#ModalCorteImpresion').modal('show');
        }
        else {
            for (var i = 0; i < esquemadeimpresion.Reportes.length; i++) {
                if (esquemadeimpresion.Reportes[i].NitCliente == nitcliente) {
                    mesesdepuesvencimiento = esquemadeimpresion.Reportes[i].MesesVencimiento;
                    tipodeimpresion = esquemadeimpresion.Reportes[i].DESTINOIMPRESION;
                    warehousecode = esquemadeimpresion.Reportes[i].WareHouseCode;
                    camposocultos = esquemadeimpresion.Reportes[i].CamposOcultos;
                    break;
                }
                if (i == esquemadeimpresion.Reportes.length - 1) {
                    mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
                    tipodeimpresion = esquemadeimpresion.Reportes[0].DESTINOIMPRESION;
                    warehousecode = esquemadeimpresion.Reportes[0].WareHouseCode;
                    camposocultos = esquemadeimpresion.Reportes[0].CamposOcultos;
                }
            }
            $('#ModalCorteImpresion').modal('show');
        }
    }
    else if (esquemadeimpresion.TipoImpresion == "SELLADO") {
        
        ConsultarInformacionImpresion(esquemadeimpresion.TipoImpresion);
        //warehousecode = esquemadeimpresion.WareHouseCode;
        if (esquemadeimpresion.Reportes.length == 1) {
            mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
            $('#ModalSelladoImpresion').modal('show');
        }
        else {
            for (var i = 0; i < esquemadeimpresion.Reportes.length; i++) {
                if (esquemadeimpresion.Reportes[i].NitCliente == nitcliente) {
                    mesesdepuesvencimiento = esquemadeimpresion.Reportes[i].MesesVencimiento;
                    tipodeimpresion = esquemadeimpresion.Reportes[i].DESTINOIMPRESION;
                    warehousecode = esquemadeimpresion.Reportes[i].WareHouseCode;
                    camposocultos = esquemadeimpresion.Reportes[i].CamposOcultos;
                    break;
                }
                if (i == esquemadeimpresion.Reportes.length - 1) {
                    mesesdepuesvencimiento = esquemadeimpresion.Reportes[0].MesesVencimiento;
                    tipodeimpresion = esquemadeimpresion.Reportes[0].DESTINOIMPRESION;
                    warehousecode = esquemadeimpresion.Reportes[0].WareHouseCode;
                    camposocultos = esquemadeimpresion.Reportes[0].CamposOcultos;
                }
            }
            $('#ModalSelladoImpresion').modal('show');
        }
    }
}
function ImprimirCoExtrusionGeneral() {

    if (document.getElementById("BanderasExtrusion").value == "")
        toastr.warning("Debe ingresar la información de número de banderas", "Información");

    else if (document.getElementById("MaterialExtrusion").value == "")
        toastr.warning("Debe seleccionar un valor de material de extrusión", "Información");

    else if (document.getElementById("TratamientoExtrusion").value == "")
        toastr.warning("Debe seleccionar un valor de tratamiento de corona", "Información");

    else if (document.getElementById("FormulaExtrusion").value == "")
        toastr.warning("Debe ingresar la información de fórmula de extrusión", "Información");

    else if (document.getElementById("CalibreExtrusion").value == "")
        toastr.warning("Debe ingresar la información de calibre de extrusión", "Información");

    else {

        //Almacenar registro de banderas
        AlmacenarBanderas("Extrusion", document.getElementById("BanderasExtrusion").value);
        //Almacenar observaciones
        AlmacenarObservaciones(document.getElementById("NotaExtrusion").value);

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Banderas: document.getElementById("BanderasExtrusion").value,
            TipoMaterial: document.getElementById("MaterialExtrusion").value,
            TratamientoCorona: document.getElementById("TratamientoExtrusion").value,
            Fecha: "",
            Formula: document.getElementById("FormulaExtrusion").value,
            Calibre: document.getElementById("CalibreExtrusion").value,
            AnchoExtrusion: document.getElementById("AnchoExtrusion").value,
            Metros: parseInt(metrosimpresion).toString(),
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            Nota: document.getElementById("NotaExtrusion").value,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString()
        }
        $('#ModalMensaje').modal('show');
        $('#ModalExtrusionImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCoExtrusoraGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {
                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON["odata.error"].innererror.message);
            }
        });
    }
}
function ImprimirFlexoGeneral() {

    if (document.getElementById("BanderasFlexo").value == "")
        toastr.warning("Debe ingresar la información de número de banderas", "Información");

    else if (document.getElementById("MaterialFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de material de extrusión", "Información");

    else if (document.getElementById("CuradoFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de curado", "Información");

    else if (document.getElementById("OpSiguienteFlexo").value == "")
        toastr.warning("Debe seleccionar un valor de Op. Siguiente", "Información");

    else {

        //Almacenar registro de banderas
        AlmacenarBanderas("Flexografia", document.getElementById("BanderasFlexo").value);
        //Almacenar observaciones
        AlmacenarObservaciones(document.getElementById("NotaFlexo").value);

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Banderas: document.getElementById("BanderasFlexo").value,
            TipoMaterial: document.getElementById("MaterialFlexo").value,
            Curado: document.getElementById("CuradoFlexo").value,
            OpSiguiente: document.getElementById("OpSiguienteFlexo").value,
            Fecha: "",
            Metros: parseInt(metrosimpresion).toString(),
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            Nota: document.getElementById("NotaFlexo").value,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString()
        }
        $('#ModalMensaje').modal('show');
        $('#ModalFlexoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintFlexoGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {
                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON["odata.error"].innererror.message);
            }
        });
    }
}

function ImprimirCorte() {
    
    if (tipodeimpresion == "PrintCorteGeneral") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString()
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteQuala") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            CodigoQrRutaCliente: "",
            Codigo128: "",
            Codigo128Texto: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteQuala",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteNutresa") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Codigo128: "",
            Codigo128Texto: "",
            CodigoEAN14: "",
            SKU: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteNutresa",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }

    else if (tipodeimpresion == "PrintCorteIngles") {
        
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            Bobinas: document.getElementById("BobinasCorte").value,
            Repeticiones: document.getElementById("RepeticionesCorte").value,
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "2",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Brand: "",
            Ponum: "",
            FechaBatch: "",
            CodigoEntrada: codigoBatch.toString(),
            CamposOcultos: camposocultos
        }

        $('#ModalMensaje').modal('show');
        $('#ModalCorteImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintCorteIngles",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
        
}
function AlmacenarRegistroEPT() {

    var _cantidad = 0;
    if (esquemadeimpresion.TipoImpresion == "CORTE")
        _cantidad = (valorrepeticionesimpresion == "" || valorrepeticionesimpresion == null) ? 0 : parseFloat(valorrepeticionesimpresion);
    else
        _cantidad = parseFloat(metrosimpresion);

    //Insertar estiba
    var filter1 = {
        consecutivo: parseInt(rolloimpresion.toString()),
        idbascula: parseInt(IDBascula),
        idestibaencabezado: parseInt(idestibaabierta),
        pesoneto: parseFloat(parseFloat(pesonetoimpresion).toFixed(2)),
        tara: parseFloat(parseFloat((parseFloat(pesobrutoimpresion) - parseFloat(pesonetoimpresion))).toFixed(2)),
        cantidad: _cantidad
    }
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/InsertarDetalleEstiba",
        data: JSON.stringify(filter1),
        success: function () {}
    });
}

function ImprimirSelado() {
    debugger;
    if (tipodeimpresion == "PrintSelladoGeneral") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString()
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoGeneral",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintSelladoQuala") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            CodigoQrRutaCliente: "",
            Codigo128: "",
            Codigo128Texto: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoQuala",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }

    else if (tipodeimpresion == "PrintSelladoNutresa") {

        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            Producto: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Codigo128: "",
            Codigo128Texto: "",
            CodigoEAN14: "",
            SKU: ""
        }

        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoNutresa",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                alert(ajaxContext.responseJSON.ExceptionMessage);
            }
        });
    }
    
    else if (tipodeimpresion == "PrintSelladoIngles") {
        var filter = {
            OrdenDeProduccion: opimpresion,
            Cliente: nombrecliente,
            CodigoProducto: item,
            CodigoQrTexto: "",
            IdClienteYCliente: referenciaproducto,
            CodigoMaquina: maquina,
            CodigoProceso: codigoProceso,
            Consecutivo: rolloimpresion.toString(),
            NombreOperario: "",
            Fecha: "",
            PesoNeto: parseFloat(pesonetoimpresion).toFixed(2),
            PesoBruto: parseFloat(pesobrutoimpresion).toFixed(2),
            CodigoQrRuta: "",
            Impresora: nombreimpresora,
            NumeroCopias: "1",
            WarehouseCode: warehousecode,
            FechaVencimiento: "",
            MesesDespues: mesesdepuesvencimiento.toString(),
            CodigoOperario: codigooperario,
            Metros: parseInt(metrosimpresion).toString(),
            Brand: "",
            Ponum: "",
            FechaBatch: "",
            CodigoEntrada: codigoBatch.toString(),
            CamposOcultos: camposocultos
        }
        
        $('#ModalMensaje').modal('show');
        $('#ModalSelladoImpresion').modal('hide');

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/PrintSelladoIngles",
            data: JSON.stringify(filter),
            success: function (data) {
                $('#ModalMensaje').modal('hide');
            },
            error: function (ajaxContext) {

                $('#ModalMensaje').modal('hide');
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
}

function ConsultarInformacionImpresion($TipoImpresion){

    var filter = {
        TipoImpresion: $TipoImpresion,
        OrdenDeProduccion: opimpresion,
        CodigoProducto: item,
        CodigoPuesto: maquina,
        CodigoProceso: codigoProceso
    }
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionExtraImpresion", function (data) {

        if (data != null) {
            var value = JSON.parse(data.value);
            if ($TipoImpresion == "COEXTRUSION") {
                document.getElementById("AnchoExtrusion").value = value.AnchoExtrusion;
            }
            if ($TipoImpresion == "CORTE") {
                
                //if (value.Repeticiones != null && value.Repeticiones != "" && value.Repeticiones != 0)
                //    document.getElementById("RepeticionesCorte").value = parseFloat((pesonetoimpresion / parseFloat(value.Repeticiones)) * 1000).toFixed(0);
                //else
                //    document.getElementById("RepeticionesCorte").value = "";
                document.getElementById("RepeticionesCorte").value = valorrepeticionesimpresion;
                document.getElementById("BobinasCorte").value = value.Bobinas;
            }
        }
    }, errorHandler, JSON.stringify(filter))
}
function LimpiarPreImpresion($TipoImpresion) {

    warehousecode = "";
    mesesdepuesvencimiento = "";
    tipodeimpresion = "";
    if ($TipoImpresion == "COEXTRUSION") {
        document.getElementById("BanderasExtrusion").value = "";
        document.getElementById("MaterialExtrusion").value = "";
        document.getElementById("MaterialExtrusion").text = "";
        document.getElementById("TratamientoExtrusion").value = "";
        document.getElementById("TratamientoExtrusion").text = "";
        document.getElementById("FormulaExtrusion").value = "";
        document.getElementById("CalibreExtrusion").value = "";
        document.getElementById("NotaExtrusion").value = "";
        document.getElementById("AnchoExtrusion").value = "";
    }
    else if ($TipoImpresion == "FLEXO") {
        document.getElementById("BanderasFlexo").value = "";
        document.getElementById("MaterialFlexo").value = "";
        document.getElementById("CuradoFlexo").text = "";
        document.getElementById("OpSiguienteFlexo").value = "";
        document.getElementById("NotaFlexo").value = "";
    }

    else if ($TipoImpresion == "CORTE") {
        document.getElementById("RepeticionesCorte").value = "";
        document.getElementById("BobinasCorte").value = "";
    }
}
function AlmacenarBanderas($TipoImpresion, $valor) {

    if (IDBascula != null && IDBascula != "") {
        var BasculaBanderas = {
            COLORBANDERA: "Unico",
            CANTIDADBANDERAS: $valor,
            NOBREPROCESO: $TipoImpresion,
            BASCULAID: IDBascula
        }

        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))
    }
}
function AlmacenarObservaciones($valor) {

    if (IDBascula != null && IDBascula != "" && $valor != "") {
        var BasculaExtras =
            {
                NOMBRECAMPO: "Observaciones",
                VALORCAMPO: $valor,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULAEXTRASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaExtras))
    }
}

function limpiar() {
    $("#txtError").html("");
    //$("#produccion").val("");
    $("#turno").val("");
    $("#recurso").val("");
    $("#referencia").val("");
    $("#bobina").val("");
    $("#tara").val("");
    $("#peso").val("");
    $("#metros").val("");
    $("#operario").val("");
    //$("#recurso").data("kendoDropDownList").value(0);
    $("#datepicker").data("kendoDateTimePicker").value(new Date());
    //$("#produccion").data("kendoDropDownList").value(0);
    //codigoTurno = "";
    $("#produccion").data("kendoDropDownList").select(0);
    document.getElementById("procesosiguiente").selectedIndex = 0;
    document.getElementById("repetiones").value = "";
    document.getElementById("contenedorrepeticiones").style.display = "none";
    document.getElementById("contenedorprocesosiguiente").style.display = "none";
    document.getElementById("contenedordevoluciones").style.display = "none";
    $("#desperdicio").addClass("displaynone");

    var recurso = $("#recurso").data("kendoDropDownList");
    var _data = recurso.dataSource.data();

    for (var i = 0; i <= _data.length; i++) {
        recurso.dataSource.remove(_data[0]);
    }
    document.getElementById("assembly").value = "";
    document.getElementById("secuenciamaterial").value = "";
    document.getElementById("materialemitido").value = "";
    document.getElementById("materialdescripcion").value = "";
    document.getElementById("lote").value = "";
    document.getElementById("notadevolucion").value = "";
    //codigoProceso = "";
    document.getElementById("diveventocolor").style.backgroundColor = "";
    document.getElementById("lblevento").innerText = "";
}

function obtenerPeso() {
    connection = $.hubConnection("http://localhost:9992");
    proxyBascula = connection.createHubProxy("clientHub");
    connection.start({ logging: true })
    proxyBascula.on('data', function (dato) {
        if (basculamanual == true) {
            connection.stop({ logging: true });
            proxyBascula.off('data', function () { });
        }
        else {
            if (dato != null && dato.Valor != undefined) {
                $("#pesoneto").val(dato.Valor);
                nombreimpresora = dato.NombreImpresora;
                //nombreimpresora = "Microsoft Print to PDF";
                $("#pesoneto").change();
            }
        }
    });
    connection.error(function (error) {
        toastr.error('Se ha presentado una falla con la comunicación con la báscula, contacte al administrador: ' + error.message, 'Error');
    });
}
function validaTextoOP(op) {
    if (op == "" || op == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
        document.getElementById("btnGuardar").disabled = false;
        return false;
    }
    return true;
}
function validaValores() {

    if (document.getElementById("produccion").value == "NO" || document.getElementById("produccion").value == "devolucion")//Desperdicio
    {
        var resultado = true;
        if ($("#tara").val() == "" || $("#tara").val() == null)
            resultado = false;
        if ($("#peso").val() == 0 || $("#peso").val() == "" || $("#peso").val() < 0 || $("#peso").val() == null)
            resultado = false;
        if ($("#metros").val() == 0 || $("#metros").val() == "" || $("#metros").val() == null)
            resultado = false;
        if (!resultado) {
            $("#txtError").html("Se debe ingresar el peso, tara (no negativo / ceros) y los metros para poder guardar");
            document.getElementById("btnGuardar").disabled = false;
        }
        return resultado;
    }
    else {

        var resultado = true;
        if ($("#tara").val() == 0 || $("#tara").val() == "" || $("#tara").val() == null)
            resultado = false;
        if ($("#peso").val() == 0 || $("#peso").val() == "" || $("#peso").val() < 0 || $("#peso").val() == null)
            resultado = false;
        if ($("#metros").val() == 0 || $("#metros").val() == "" || $("#metros").val() == null)
            resultado = false;
        if (!resultado) {
            $("#txtError").html("Se debe ingresar el peso, tara (no negativo / ceros) y los metros para poder guardar");
            document.getElementById("btnGuardar").disabled = false;
        }
        return resultado;
    }
    
}
function validarInicioTurno() {

    var resultado = true;
    if ($("#datepicker").data("kendoDateTimePicker").value() == 0 || $("#datepicker").data("kendoDateTimePicker").value() == "" || $("#datepicker").data("kendoDateTimePicker").value() == null)
        resultado = false;
    if ($("#ordenproduccion").val().replace(" ", "") == null || $("#ordenproduccion").val().replace(" ", "") == "")
        resultado = false;
    if ($("#recurso").val() == null || $("#recurso").val() == "" || $("#recurso").val() == "Seleccione recurso ...")
        resultado = false;
    if ($("#operario").val() == null || $("#operario").val() == "")
        resultado = false;
    if ($("#turno").val() == null || $("#turno").val() == "")
        resultado = false;
    if ($("#referencia").val() == null || $("#referencia").val() == "")
        resultado = false;
    if (!resultado) {
        $("#txtError").html("Se debe ingresar los valores de inicio de turno");
        document.getElementById("btnGuardar").disabled = false;
    }
    return resultado;
}
function calculoPeso(e) {
    var tara = $("#tara").val();
    var pesoneto = $("#pesoneto").val();
    var pesocarrete = $("#carrete").val();
    
    if (!tara)
        tara = 0;
    if (!pesoneto)
        pesoneto = 0;
    if (!pesocarrete)
        pesocarrete = 0;
    var peso = pesoneto - (parseFloat(tara) + parseFloat(pesocarrete));
    $("#peso").val(peso);

    //Validar repetiones
    if (esquemadeimpresion != undefined) {
        if (esquemadeimpresion.TipoImpresion == "CORTE") {
            if (valorrepeticiones == 0)
                document.getElementById("repetiones").value = "";
            else
                document.getElementById("repetiones").value = parseFloat(($("#peso").val() / parseFloat(valorrepeticiones)) * 1000).toFixed(0);
        }
    }
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function isNumberWithoutDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function buscarconfiguracionetiquetas() {

    if (document.getElementById("procesosconfiguracion").value != "") {
        var filter = {
            Proceso: document.getElementById("procesosconfiguracion").value
        }

        //limpiar 
        tipoimpresionedicio = "";

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/GetEsquemaDeImpresionPorProceso",
            data: JSON.stringify(filter),
            success: function (data) {
                
                var _data = JSON.parse(data.value);

                var grid = $("#gridconfigetiquetas").data("kendoGrid");
                if (grid != undefined) {

                    $("#gridconfigetiquetas").empty();
                }
                tipoimpresionedicio = _data.TipoImpresion;
                $("#gridconfigetiquetas").kendoGrid({
                    dataSource: {
                        data: _data.Reportes,
                        schema: {
                            model: {
                                fields: {
                                    DESTINOIMPRESION: { type: "string" },
                                    MesesVencimiento: { type: "string" },
                                    NitCliente: { type: "string" },
                                    WareHouseCode: { type: "string" },
                                    CamposOcultos: { type: "string" }
                                }
                            }
                        },
                    },
                    height: 250,
                    scrollable: true,
                    sortable: true,
                    filterable: true,
                    selectable: "true",
                    change: viewseleccionarconfigetiquetas,
                    columns: [
                        { field: "DESTINOIMPRESION", title: "Impresión", width: "50%", filterable: false },
                        { field: "MesesVencimiento", title: "Meses vencimiento", width: "25%", filterable: false },
                        { field: "NitCliente", title: "Nit cliente", width: "25%", filterable: { multi: true } }
                    ]
                });
            },
            error: function (ajaxContext) {
                ajaxContext.responseJSON.ExceptionMessage;
            }
        });
    }
    else {
        tipoimpresionedicio = "";
        alert("Debe seleccionar un proceso para la configuración del sistema");
        var grid = $("#gridconfigetiquetas").data("kendoGrid");
        if (grid != undefined) {

            $("#gridconfigetiquetas").empty();
        }
    }
}
function viewseleccionarconfigetiquetas() {
    
    var grid = $("#gridconfigetiquetas").data("kendoGrid");
    var itemgrid = grid.dataItem(grid.select());

    if (itemgrid.NitCliente == "*") {
        alert("No se puede editar el registro genérico de etiqueta");
    }
    else {
        
        $('#ModalEdicionEtiqueta').modal('show');
        document.getElementById("areaedicionetiqueta").value = tipoimpresionedicio;
        nuevoregistroetiquetaedicio = false;
        if (itemgrid.DESTINOIMPRESION == "ImprimirCoExtrusionGeneral" || itemgrid.DESTINOIMPRESION == "PrintFlexoGeneral"
            || itemgrid.DESTINOIMPRESION == "PrintSelladoGeneral" || itemgrid.DESTINOIMPRESION == "PrintCorteGeneral")
            $("#tipoetiquetaedicion").val("GENERAL");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoNutresa" || itemgrid.DESTINOIMPRESION == "PrintCorteNutresa")
            $("#tipoetiquetaedicion").val("NUTRESA");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoQuala" || itemgrid.DESTINOIMPRESION == "PrintCorteQuala")
            $("#tipoetiquetaedicion").val("QUALA");

        else if (itemgrid.DESTINOIMPRESION == "PrintSelladoIngles" || itemgrid.DESTINOIMPRESION == "PrintCorteIngles")
            $("#tipoetiquetaedicion").val("INGLES");
        
        $("#mesesvencimientoetiqueta").val(itemgrid.MesesVencimiento);
        document.getElementById("nitclienteedicionetiquetas").value = itemgrid.NitCliente;
        document.getElementById("wareHouseCodeetiqueta").value = itemgrid.WareHouseCode;
        $("#camposvisibleetiqueta").val(itemgrid.CamposOcultos);
    }
}

function almacenarconfiguracionetiqueta() {
    
    if (document.getElementById("tipoetiquetaedicion").value == "")
        alert("Debe seleccionar un valor para el tipo de etiqueta");
    else if (document.getElementById("mesesvencimientoetiqueta").value == "")
        alert("Debe seleccionar un valor para los meses de vencimiento");
    else if (document.getElementById("nitclienteedicionetiquetas").value == "")
        alert("Debe ingresar un valor para el nit del cliente");
    else if (document.getElementById("nitclienteedicionetiquetas").value == "*")
        alert("Debe ingresar un valor diferente al genérico de etiqueta usando el nit cliente *");
    else if (document.getElementById("wareHouseCodeetiqueta").value == "")
        alert("Debe ingresar un valor para WareHouse");
    else {

        if (nuevoregistroetiquetaedicio == false) {

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var itemgrid = grid.dataItem(grid.select());

            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION") {
                itemgrid.DESTINOIMPRESION = "ImprimirCoExtrusionGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO") {
                itemgrid.DESTINOIMPRESION = "PrintFlexoGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "CORTE") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    itemgrid.DESTINOIMPRESION = "PrintCorteGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    itemgrid.DESTINOIMPRESION = "PrintCorteNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    itemgrid.DESTINOIMPRESION = "PrintCorteQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    itemgrid.DESTINOIMPRESION = "PrintCorteIngles";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    itemgrid.DESTINOIMPRESION = "PrintSelladoIngles";
            }

            itemgrid.MesesVencimiento = document.getElementById("mesesvencimientoetiqueta").value;
            itemgrid.NitCliente = document.getElementById("nitclienteedicionetiquetas").value;
            itemgrid.WareHouseCode = document.getElementById("wareHouseCodeetiqueta").value;
            itemgrid.CamposOcultos = document.getElementById("camposvisibleetiqueta").value;

            var tipoWareHouseCode = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "CORTE")
                tipoWareHouseCode = "BPTF";
            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO")
                tipoWareHouseCode = "BPTF";
            else
                tipoWareHouseCode = "BPTF";

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var cadenaedicion = '{"TipoImpresion":"' + document.getElementById("areaedicionetiqueta").value + '","WareHouseCode":"' + tipoWareHouseCode + '","Reportes":[';

            for (var i = 0; i < grid._data.length; i++) {
                cadenaedicion = cadenaedicion + '{"DESTINOIMPRESION":"' + grid._data[i].DESTINOIMPRESION + '","NitCliente":"'
                    + grid._data[i].NitCliente + '","MesesVencimiento":"' + grid._data[i].MesesVencimiento
                    + '","WareHouseCode":"' + grid._data[i].WareHouseCode + '","CamposOcultos":"' + grid._data[i].CamposOcultos + '"},'
            }
            cadenaedicion = cadenaedicion.substr(0, cadenaedicion.length - 1) + "]}";

            var filter = {
                Proceso: document.getElementById("areaedicionetiqueta").value,
                ValueRDLC: cadenaedicion
            }

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/GetEditarEstandares",
                data: JSON.stringify(filter),
                success: function (data) {

                    buscarconfiguracionetiquetas();
                },
                error: function (ajaxContext) {
                    alert("Error al intentar almacenar el registro");
                }
            });
        }

        else {

            var _destinoimpresion = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION") {
                _destinoimpresion = "ImprimirCoExtrusionGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO") {
                _destinoimpresion = "PrintFlexoGeneral";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "CORTE") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    _destinoimpresion = "PrintCorteGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    _destinoimpresion = "PrintCorteNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    _destinoimpresion = "PrintCorteQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    _destinoimpresion = "PrintCorteIngles";
            }

            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO") {
                if (document.getElementById("tipoetiquetaedicion").value == "GENERAL")
                    _destinoimpresion = "PrintSelladoGeneral";
                else if (document.getElementById("tipoetiquetaedicion").value == "NUTRESA")
                    _destinoimpresion = "PrintSelladoNutresa";
                else if (document.getElementById("tipoetiquetaedicion").value == "QUALA")
                    _destinoimpresion = "PrintSelladoQuala";
                else if (document.getElementById("tipoetiquetaedicion").value == "INGLES")
                    _destinoimpresion = "PrintSelladoIngles";
            }

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            grid.dataSource.add({
                DESTINOIMPRESION: _destinoimpresion, MesesVencimiento: document.getElementById("mesesvencimientoetiqueta").value,
                NitCliente: document.getElementById("nitclienteedicionetiquetas").value,
                WareHouseCode: document.getElementById("wareHouseCodeetiqueta").value,
                CamposOcultos: document.getElementById("camposvisibleetiqueta").value,
            });

            var tipoWareHouseCode = "";
            if (document.getElementById("areaedicionetiqueta").value == "COEXTRUSION")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "CORTE")
                tipoWareHouseCode = "BPTF";
            else if (document.getElementById("areaedicionetiqueta").value == "FLEXO")
                tipoWareHouseCode = "BEXT";
            else if (document.getElementById("areaedicionetiqueta").value == "SELLADO")
                tipoWareHouseCode = "BPTF";
            else
                tipoWareHouseCode = "BPTF";

            var grid = $("#gridconfigetiquetas").data("kendoGrid");
            var cadenaedicion = '{"TipoImpresion":"' + document.getElementById("areaedicionetiqueta").value + '","WareHouseCode":"' + tipoWareHouseCode + '","Reportes":[';
            for (var i = 0; i < grid._data.length; i++) {
                cadenaedicion = cadenaedicion + '{"DESTINOIMPRESION":"' + grid._data[i].DESTINOIMPRESION + '","NitCliente":"'
                    + grid._data[i].NitCliente + '","MesesVencimiento":"' + grid._data[i].MesesVencimiento
                    + '","WareHouseCode":"' + grid._data[i].WareHouseCode + '","CamposOcultos":"' + grid._data[i].CamposOcultos + '"},'
            }
            cadenaedicion = cadenaedicion.substr(0, cadenaedicion.length - 1) + "]}";

            var filter = {
                Proceso: document.getElementById("areaedicionetiqueta").value,
                ValueRDLC: cadenaedicion
            }

            $.ajax({
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                url: baseUrl + "TUPLAEJECUCIONs/GetEditarEstandares",
                data: JSON.stringify(filter),
                success: function (data) {

                    buscarconfiguracionetiquetas();
                },
                error: function (ajaxContext) {
                    alert("Error al intentar almacenar el registro");
                }
            });
        }

        $('#gridconfigetiquetas').data('kendoGrid').refresh();
        $('#ModalEdicionEtiqueta').modal('hide');

    }
}

function agregarnuevaetiquetaedicion() {
    
    if (tipoimpresionedicio == "" || tipoimpresionedicio == undefined)
        alert("Debe seleccionar y buscar un tipo de proceso para agregar una nueva regla")
    else {

        $('#ModalEdicionEtiqueta').modal('show');
        document.getElementById("areaedicionetiqueta").value = tipoimpresionedicio;
        document.getElementById("tipoetiquetaedicion").selectedIndex = 0;
        document.getElementById("mesesvencimientoetiqueta").selectedIndex = 0;
        document.getElementById("nitclienteedicionetiquetas").value = "";
        document.getElementById("wareHouseCodeetiqueta").value = "";
        document.getElementById("camposvisibleetiqueta").selectedIndex = 0;
        nuevoregistroetiquetaedicio = true;
    }
}

/*Funciones imprimir*/
function ObternerTipoImpresion() {

    /*Buscar tipo reporte*/
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetTipoImpresion", function (data) {

        ServerTipoImpresion = data.value;
    }, errorHandler)

}
function ConfirmarImprimir($Valor) {

    if ($Valor == "Si") {

        $('#ModalConfirmacion').modal('hide');
        AbrirImprmir();
    }

    else {

        IDBascula = null;
        $('#ModalConfirmacion').modal('hide');
    }
}
function AbrirImprmir() {

    TipoImpresion = document.getElementById("OpcionDeImpresion").value;

    /*Limpiar impresión*/
    LimpiarImpresion();

    ArmarGridBanderas();

    if (TipoImpresion == "Extrusion") {

        $('#ModalExtrusionImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionExtrusion").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Flexografia") {
        $('#ModalFlexografiaImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionFlexografia").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Laminado") {
        $('#ModalLaminadoImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionLaminado").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {
        $('#ModalCorteImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionCorte").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "SelladoPT") {
        $('#ModalSelladoPTImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSelladoPt").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "SelladoPQ") {
        $('#ModalSelladoPQImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSelladoPq").value = opimpresion;
            BuscarOPImpresion();
        }
    }

    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {
        $('#ModalSelladoImpresion').modal('show');

        if (opimpresion != null || opimpresion != "" || opimpresion != undefined) {

            document.getElementById("OrdenProduccionSellado").value = opimpresion;
            BuscarOPImpresion();
        }
    }
}
function LimpiarImpresion() {

    if (TipoImpresion == "Extrusion") {

        document.getElementById("OrdenProduccionExtrusion").value = "";
        document.getElementById("DateExtrusion").value = "";
        document.getElementById("ItemExtrusion").value = "";
        document.getElementById("FormulaExtrusion").value = "";
        document.getElementById("OperarioExtrusion").value = "";
        document.getElementById("PesoBrutoExtrusion").value = "";
        document.getElementById("PesoNetoExtrusion").value = "";
        document.getElementById("RolloExtrusion").value = "";
        document.getElementById("MaquinaExtrusion").value = "";
        document.getElementById("MetrosExtrusion").value = "";
        document.getElementById("AnchoExtrusion").value = "";
        document.getElementById("CalibreExtrusion").value = "";
        document.getElementById("ProcesoExtrusion").selectedIndex = 0
        document.getElementById("MaterialExtrusion").selectedIndex = 0
        document.getElementById("ObservacionesExtrusion").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasExtrusion').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Flexografia") {

        document.getElementById("OrdenProduccionFlexografia").value = "";
        document.getElementById("DateFlexografia").value = "";
        document.getElementById("TurnoFlexografia").value = "";
        document.getElementById("ItemFlexografia").value = "";
        document.getElementById("ReferenciaFlexografia").value = "";
        document.getElementById("OperarioFlexografia").value = "";
        document.getElementById("PesoBrutoFlexografia").value = "";
        document.getElementById("RolloFlexografia").value = "";
        document.getElementById("PesoNetoFlexografia").value = "";
        document.getElementById("MetroFlexografia").value = "";
        document.getElementById("ObservacionesFlexografia").value = "";
        document.getElementById("ClienteFlexografia").value = "";
        document.getElementById("ProcesoFlexografia").selectedIndex = 0;
        document.getElementById("MaterialFlexografia").selectedIndex = 0;
        document.getElementById("MaquinaFlexografia").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasFlexografia').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Laminado") {

        document.getElementById("OrdenProduccionLaminado").value = "";
        document.getElementById("DateLaminado").value = "";
        document.getElementById("TurnoLaminado").value = "";
        document.getElementById("ItemLaminado").value = "";
        document.getElementById("ReferenciaLaminado").value = "";
        document.getElementById("OperarioLaminado").value = "";
        document.getElementById("PesoBrutoLaminado").value = "";
        document.getElementById("RolloLaminado").value = "";
        document.getElementById("PesoNetoLaminado").value = "";
        document.getElementById("MetroLaminado").value = "";
        document.getElementById("ObservacionesLaminado").value = "";
        document.getElementById("ClienteLaminado").value = "";
        document.getElementById("ProcesoLaminado").selectedIndex = 0;
        document.getElementById("MaterialLaminado").selectedIndex = 0;
        document.getElementById("MaquinaLaminado").value = "";

        /*Limpiar grid*/
        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            var length = grid._data.length;

            for (var i = 1; i <= length; i++) {
                var PositionBandera = $('#GridBanderasLaminado').data().kendoGrid.dataSource.data()[i - 1];
                PositionBandera.negro = 0;
                PositionBandera.rojo = 0;
                PositionBandera.amarillo = 0;
                PositionBandera.azul = 0;
                PositionBandera.verde = 0;
                PositionBandera.set('', '');
            }
        }
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {

        document.getElementById("OrdenProduccionCorte").value = "";
        document.getElementById("DateCorte").value = "";
        document.getElementById("TurnoCorte").value = "";
        document.getElementById("ItemCorte").value = "";
        document.getElementById("ReferenciaCorte").value = "";
        document.getElementById("ClienteCorte").value = "";
        document.getElementById("OperarioCorte").value = "";
        document.getElementById("MaquinaCorte").value = "";
        document.getElementById("PesoBrutoCorte").value = "";
        document.getElementById("ConsecutivoCorte").value = "";
        document.getElementById("PesoNetoCorte").value = "";
        document.getElementById("RepeticionesCorte").value = "";
        document.getElementById("QBobinasCorte").value = "";
        document.getElementById("CodigoClienteCorte").value = "";
        document.getElementById("NitClienteCorte").value = "";
        document.getElementById("BrandCorte").value = "";
        document.getElementById("PonumCorte").value = "";
        document.getElementById("SlitPackDateCorte").value = "";

    }

    else if (TipoImpresion == "SelladoPT") {

        document.getElementById("OrdenProduccionSelladoPt").value = "";
        document.getElementById("DateSelladoPt").value = "";
        document.getElementById("TurnoSelladoPt").value = "";
        document.getElementById("ItemSelladoPt").value = "";
        document.getElementById("ReferenciaSelladoPt").value = "";
        document.getElementById("ClienteSelladoPt").value = "";
        document.getElementById("OperarioSelladoPt").value = "";
        document.getElementById("MaterialSelladoPt").value = "";
        document.getElementById("PesoNetoSelladoPt").value = "";
        document.getElementById("CantidadSelladoPt").value = "";
        document.getElementById("CodigoClienteSelladoPt").value = "";
        document.getElementById("OCSelladoPt").value = "";

    }

    else if (TipoImpresion == "SelladoPQ") {

        document.getElementById("OrdenProduccionSelladoPq").value = "";
        document.getElementById("DateSelladoPq").value = "";
        document.getElementById("TurnoSelladoPq").value = "";
        document.getElementById("ItemSelladoPq").value = "";
        document.getElementById("ReferenciaSelladoPq").value = "";
        document.getElementById("ClienteSelladoPq").value = "";
        document.getElementById("OperarioSelladoPq").value = "";
        document.getElementById("MaterialSelladoPq").value = "";
        document.getElementById("PaquetePq").value = "";
        document.getElementById("UnidadPorPaquetePq").value = "";
        document.getElementById("CodigoClienteSelladoPq").value = "";
        document.getElementById("OCSelladoPq").value = "";

    }

    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        document.getElementById("OrdenProduccionSellado").value = "";
        document.getElementById("DateSellado").value = "";
        document.getElementById("TurnoSellado").value = "";
        document.getElementById("ItemSellado").value = "";
        document.getElementById("ReferenciaSellado").value = "";
        document.getElementById("ClienteSellado").value = "";
        document.getElementById("OperarioSellado").value = "";
        document.getElementById("MaquinaSellado").value = "";
        document.getElementById("PesoBrutoSellado").value = "";
        document.getElementById("ConsecutivoSellado").value = "";
        document.getElementById("PesoNetoSellado").value = "";
        document.getElementById("CodigoClienteSellado").value = "";
        document.getElementById("NitClienteSellado").value = "";
        document.getElementById("BrandSellado").value = "";
        document.getElementById("PonumSellado").value = "";
        document.getElementById("SlitPackDateSellado").value = "";
        document.getElementById("CantidadSellado").value = "";
    }
}
function ArmarGridBanderas() {

    if (TipoImpresion == "Extrusion") {
        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasExtrusion").empty();
        }

        $("#GridBanderasExtrusion").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasExtrusion .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasExtrusion .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasExtrusion").data("kendoGrid");
        if (grid != undefined) {
            
            var CountRows = 1;
            //var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                //NumerBandera = parseInt(NumerBandera) - 1;
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasExtrusion").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {
                
                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }


    else if (TipoImpresion == "Flexografia") {

        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasFlexografia").empty();
        }

        $("#GridBanderasFlexografia").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasFlexografia .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasFlexografia .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasFlexografia").data("kendoGrid");
        if (grid != undefined) {

            var CountRows = 1;
            //var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasFlexografia").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {

                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }


    else if (TipoImpresion == "Laminado") {

        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            $("#GridBanderasLaminado").empty();
        }

        $("#GridBanderasLaminado").kendoGrid({
            editable: true,
            pageable: false,
            selectable: true,
            navigatable: true,
            edit: function (e) {
                $("#GridBanderasLaminado .k-dirty").addClass("k-dirty-clear");
                $("#GridBanderasLaminado .k-dirty").removeClass("k-dirty-clear");
            },
            columns:
            [
                { field: "negro", title: "Negro", editable: true, type: "number", minValue: 2, attributes: { style: "text-align:center;" } },
                { field: "rojo", title: "Rojo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "amarillo", title: "Amarillo", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "azul", title: "Azul", editable: true, type: "number", attributes: { style: "text-align:center;" } },
                { field: "verde", title: "Verde", editable: true, type: "number", attributes: { style: "text-align:center;" } }
            ]
        }).data("kendoGrid");

        var grid = $("#GridBanderasLaminado").data("kendoGrid");
        if (grid != undefined) {

            var CountRows = 1;
            var NumerBandera = parseInt(CountRows) + 1;
            for (i = 1; i <= CountRows; i++) {
                var sel = grid.select();
                var sel_idx = sel.index();
                var item = grid.dataItem(sel);
                var idx = grid.dataSource.indexOf(item);
                grid.dataSource.insert(idx, { negro: 0, rojo: 0, amarillo: 0, azul: 0, verde: 0 });
                grid.editRow($("#grid tr:eq(" + (sel_idx + 1) + ")"));
            }

            //Colores encabezado grid
            var grid = $("#GridBanderasLaminado").data("kendoGrid");
            var Bgcolors = ["#000000", "#FF0000", "#F3FF00", "#002EFF", "#289B00"]
            var Txtcolors = ["#FFFFFF", "#FFFFFF", "#000000", "#FFFFFF", "#000000"]
            if (grid != undefined) {

                for (var i = 0; i < grid.thead[0].firstChild.cells.length; i++) {
                    grid.thead[0].firstChild.cells[i].className = "";
                    grid.thead[0].firstChild.cells[i].bgColor = Bgcolors[i];
                    grid.thead[0].firstChild.cells[i].style.color = Txtcolors[i];
                    grid.thead[0].firstChild.cells[i].style.textAlign = "center";
                }
            }
        }
    }
}


function BuscarOPImpresion() {

    if (TipoImpresion == "Extrusion") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionExtrusion").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateExtrusion").value = today;

        document.getElementById("PesoBrutoExtrusion").value = pesobrutoimpresion;
        document.getElementById("PesoNetoExtrusion").value = pesonetoimpresion;
        document.getElementById("RolloExtrusion").value = rolloimpresion;
        document.getElementById("MetrosExtrusion").value = metrosimpresion;

        document.getElementById("ItemExtrusion").value = item;
        document.getElementById("OperarioExtrusion").value = operario;
        document.getElementById("MaquinaExtrusion").value = maquina;

    }

    else if (TipoImpresion == "Flexografia") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionFlexografia").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateFlexografia").value = today;
        
        document.getElementById("PesoBrutoFlexografia").value = pesobrutoimpresion;
        document.getElementById("PesoNetoFlexografia").value = pesonetoimpresion;
        document.getElementById("RolloFlexografia").value = rolloimpresion;
        document.getElementById("MetroFlexografia").value = metrosimpresion;

        document.getElementById("ItemFlexografia").value = item;
        document.getElementById("OperarioFlexografia").value = operario;
        document.getElementById("ReferenciaFlexografia").value = referenciaproducto;
        document.getElementById("TurnoFlexografia").value = codigoTurno;
        document.getElementById("MaquinaFlexografia").value = maquina;
        

        BuscarCliente();
    }

    else if (TipoImpresion == "Laminado") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionLaminado").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateLaminado").value = today;

        document.getElementById("PesoBrutoLaminado").value = pesobrutoimpresion;
        document.getElementById("PesoNetoLaminado").value = pesonetoimpresion;
        document.getElementById("RolloLaminado").value = rolloimpresion;
        document.getElementById("MetroLaminado").value = metrosimpresion;

        document.getElementById("ItemLaminado").value = item;
        document.getElementById("OperarioLaminado").value = operario;
        document.getElementById("ReferenciaLaminado").value = referenciaproducto;
        document.getElementById("TurnoLaminado").value = codigoTurno;
        document.getElementById("MaquinaLaminado").value = maquina;

        BuscarCliente();
    }

    else if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionCorte").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateCorte").value = today;
        
        document.getElementById("PesoBrutoCorte").value = parseFloat(pesobrutoimpresion).toFixed(2);
        document.getElementById("PesoNetoCorte").value = parseFloat(pesonetoimpresion).toFixed(2);

        document.getElementById("ItemCorte").value = item;
        document.getElementById("OperarioCorte").value = codigooperario;
        document.getElementById("MaquinaCorte").value = nombremaquina;
        document.getElementById("ReferenciaCorte").value = referenciaproducto;
        document.getElementById("TurnoCorte").value = codigoTurno;
        document.getElementById("ConsecutivoCorte").value = rolloimpresion;

        BuscarCliente();

        var filter = {
            OrdenProduccion: op,
            CodigoProducto: item
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBobinasPorCaja", function (data) {

            if (data != null) {
                document.getElementById("QBobinasCorte").value = data.value;
            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPesoRepeticion", function (data) {

            if (data != null) {

                if (data.value == "")
                {
                    document.getElementById("RepeticionesCorte").value = "";
                }
                else
                {
                    document.getElementById("RepeticionesCorte").value = parseFloat((document.getElementById("PesoNetoCorte").value / parseFloat(data.value)) * 1000).toFixed(0);
                }
                
            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPonum", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("PonumCorte").value = "";
                }
                else {
                    document.getElementById("PonumCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBrand", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("BrandCorte").value = "";
                }
                else {
                    document.getElementById("BrandCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        var filter = {
            Bacth: codigoBatch
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetFechaBatch", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("SlitPackDateCorte").value = "";
                }
                else {
                    document.getElementById("SlitPackDateCorte").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

    }

    else if (TipoImpresion == "SelladoPT") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPt").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSelladoPt").value = today;
        
        document.getElementById("PesoNetoSelladoPt").value = pesonetoimpresion;

        document.getElementById("ItemSelladoPt").value = item;
        document.getElementById("OperarioSelladoPt").value = operario;
        document.getElementById("ReferenciaSelladoPt").value = referenciaproducto;
        document.getElementById("TurnoSelladoPt").value = codigoTurno;

        BuscarCliente();
    }

    else if (TipoImpresion == "SelladoPQ") {
        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPq").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSelladoPq").value = today;
        
        document.getElementById("ItemSelladoPq").value = item;
        document.getElementById("OperarioSelladoPq").value = operario;
        document.getElementById("ReferenciaSelladoPq").value = referenciaproducto;
        document.getElementById("TurnoSelladoPq").value = codigoTurno;

        BuscarCliente();
    }


    else if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSellado").val();
        if (!validaTextoOP(op))
            return;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateSellado").value = today;

        document.getElementById("PesoBrutoSellado").value = parseFloat(pesobrutoimpresion).toFixed(2);
        document.getElementById("PesoNetoSellado").value = parseFloat(pesonetoimpresion).toFixed(2);
        document.getElementById("CantidadSellado").value = parseFloat(metrosimpresion).toFixed(0);

        document.getElementById("ItemSellado").value = item;
        document.getElementById("OperarioSellado").value = codigooperario;
        document.getElementById("MaquinaSellado").value = nombremaquina;
        document.getElementById("ReferenciaSellado").value = referenciaproducto;
        document.getElementById("TurnoSellado").value = codigoTurno;
        document.getElementById("ConsecutivoSellado").value = rolloimpresion;

        BuscarCliente();

        var filter = {
            OrdenProduccion: op,
            CodigoProducto: item
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetPonum", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("PonumSellado").value = "";
                }
                else {
                    document.getElementById("PonumSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetBrand", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("BrandSellado").value = "";
                }
                else {
                    document.getElementById("BrandSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

        var filter = {
            Bacth: codigoBatch
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetFechaBatch", function (data) {

            if (data != null) {

                if (data.value == "") {
                    document.getElementById("SlitPackDateSellado").value = "";
                }
                else {
                    document.getElementById("SlitPackDateSellado").value = data.value;
                }

            }

        }, errorHandler, JSON.stringify(filter))

    }

    
}
function BuscarCliente() {

    if (TipoImpresion == "Flexografia") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionFlexografia").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemFlexografia").value   
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null)
            {
                document.getElementById("ClienteFlexografia").value = data.NombreCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Laminado") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionLaminado").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemLaminado").value   
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteLaminado").value = data.NombreCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Corte" || TipoImpresion == "CorteIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionCorte").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemCorte").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteCorte").value = data.NombreCliente;
                document.getElementById("CodigoClienteCorte").value = data.CodigoCliente;
                document.getElementById("NitClienteCorte").value = data.NitCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    else if (TipoImpresion == "SelladoPT") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPt").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSelladoPt").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSelladoPt").value = data.NombreCliente;
                document.getElementById("CodigoClienteSelladoPt").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))

    }

    else if (TipoImpresion == "SelladoPQ") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSelladoPq").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSelladoPq").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSelladoPq").value = data.NombreCliente;
                document.getElementById("CodigoClienteSelladoPq").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }

    if (TipoImpresion == "Sellado" || TipoImpresion == "SelladoIngles") {

        $("#txtError").html("");
        var op = $("#OrdenProduccionSellado").val();
        if (!validaTextoOP(op))
            return;
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: document.getElementById("ItemSellado").value
        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteSellado").value = data.NombreCliente;
                document.getElementById("CodigoClienteSellado").value = data.CodigoCliente;
                document.getElementById("NitClienteSellado").value = data.NitCliente;
            }

        }, errorHandler, JSON.stringify(filter))
    }
}



function AbrirImpresionGenerico() {
    
    if (document.getElementById("ordenproduccion").value == "" || document.getElementById("ordenproduccion").value == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
    }
    else if (document.getElementById("ordenproduccion").value == "" || document.getElementById("ordenproduccion").value == null) {
        $("#txtError").html("No se ha ingresado ninguna orden de producción");
    }
    else if (document.getElementById("recurso").value == "" || document.getElementById("recurso").value == null) {
        $("#txtError").html("No hay información del recurso");
    }
    else if (document.getElementById("operario").value == "" || document.getElementById("operario").value == null) {
        $("#txtError").html("No hay información del operario");
    }
    else if (document.getElementById("turno").value == "" || document.getElementById("turno").value == null) {
        $("#txtError").html("No hay información del turno");
    }
    else if (document.getElementById("referencia").value == "" || document.getElementById("referencia").value == null) {
        $("#txtError").html("No hay información del producto");
    }
    else
    {
        /*Abrir Modal Genérico*/
        $('#ModalGenerico').modal('show');
        var op = document.getElementById("ordenproduccion").value;

        /*Buscar cliente*/
        var filter = {
            OrdenProduccion: op,
            CodigoProucto: item

        }
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetInformacionCliente", function (data) {

            if (data != null) {
                document.getElementById("ClienteGenerico").value = data.NombreCliente;
                document.getElementById("CodigoClienteGenerico").value = data.CodigoCliente;
            }

        }, errorHandler, JSON.stringify(filter))

        /*Setear Objetos de impresión genérico*/
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        document.getElementById("DateGenerico").value = today;
        document.getElementById("OrdenProduccionGenerico").value = op;
        document.getElementById("ItemGenerico").value = item;
        document.getElementById("ReferenciaGenerico").value = referenciaproducto;
        document.getElementById("MaquinaGenerico").value = maquina;
        document.getElementById("OperarioGenerico").value = operario;
        document.getElementById("TurnoGenerico").value = codigoTurno;
        document.getElementById("CopiasGenerico").value = "1";
    }
}
function Confirmar() {

    $('#ModalConfirmacion').modal('show');
}
function TipoDeProduccion() {

    document.getElementById("contenedorrepeticiones").style.display = "None";
    document.getElementById("contenedorprocesosiguiente").style.display = "None";
    document.getElementById("contenedordevoluciones").style.display = "None";
    document.getElementById("diveventocolor").style.backgroundColor = "";
    document.getElementById("lblevento").innerText = "";

    var opcion = document.getElementById("produccion").value;
    if (opcion == "NO")
    {
        document.getElementById("tara").value = 0;
        document.getElementById("bobina").value = 0;
        document.getElementById("diveventocolor").style.backgroundColor = "red";
        document.getElementById("lblevento").innerText = "DESPERDICIO";
    }
    else if (opcion == "SI") {
       
        document.getElementById("bobina").value = cantidaddebobinaproduccion;
        if (tempTara == undefined)
            tempTara = 0;
        document.getElementById("tara").value = tempTara;
        if (esquemadeimpresion != undefined) {
            if (esquemadeimpresion.TipoImpresion == "CORTE") {
                document.getElementById("contenedorrepeticiones").style.display = "";
                document.getElementById("contenedorprocesosiguiente").style.display = "";
            }
        }
        document.getElementById("diveventocolor").style.backgroundColor = "green";
        document.getElementById("lblevento").innerText = "PRODUCCION";
    }
    
    else if (opcion == "devolucion") {
        document.getElementById("tara").value = 0;
        document.getElementById("bobina").value = 0;
        document.getElementById("contenedordevoluciones").style.display = "";
        document.getElementById("diveventocolor").style.backgroundColor = "blue";
        document.getElementById("lblevento").innerText = "DEVOLUCION";

        //Consultar información de materiales   
        if ($("#ordenproduccion").val() != "" && codigoProceso != null && codigoProceso != "")
            ConsultarInformacionDevoluciones($("#ordenproduccion").val(), codigoProceso);
    }
}
function ActualizarOP()
{
    location.reload();
}



function ModalLogin($function) {

    function_Login = $function;
    $('#ModalLogin').modal('show');
}

function LoginEditBobinas()
{
    var Username = document.getElementById("Username").value;
    var Password = document.getElementById("Password").value;
    
    if (function_Login != null | function_Login != "")
    {
        if (Username != "" & Password != "") {
            var filter = {
                Username: Username,
                Password: Password
            }

            request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetLoginEdicionBobinas", function (data) {

                if (data.value == true) {
                    
                    document.getElementById("Username").value = "";
                    document.getElementById("Password").value = "";

                    $('#ModalLogin').modal('hide');

                    if (function_Login == "Bobinas") 
                        $('#ModalEditBobinas').modal('show');

                    else if (function_Login == "BobinasManual") {

                        if (basculamanual == false) {
                            document.getElementById("pesoneto").value = "";
                            document.getElementById("peso").value = "";
                            document.getElementById("pesoneto").disabled = false;
                            basculamanual = true;
                        }
                        else
                            location.reload();
                    }

                    else if (function_Login == "Configuracion") {
                        $('#ModalConfiguracion').modal('show');
                    }

                    else if (function_Login == "ConfiguracionCarrete") {
                        listadecarretesedicion();
                        $('#ModalConfiguracionCarrete').modal('show');
                    }
                        
                }

                else
                    alert("Username y/o Password incorrecto");

            }, errorHandler, JSON.stringify(filter))
        }

        else
            alert("Debe ingresar Username/Password");
    }
}
function ConsultarOrdenProduccionBobinas() {
    
    var OrdenProduccion = document.getElementById("OrdenProduccionBobina").value;
    if (OrdenProduccion != "") {

        var filter = {
            OrdenProduccion: OrdenProduccion
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EdicionOrdenProduccionBobinas", function (data) {

            if (data.value != null) {
               
                var data = data.value;

                $("#GridBobinas").kendoGrid({
                    dataSource: {
                        data: data,
                        schema: {
                            model: {
                                id: "ID",
                                fields: {
                                    CODIGOORDENPRODUCCION: { editable: false },
                                    NOMBREPUESTO: { editable: false },
                                    REFERENCIA: { editable: false },
                                    PRODUCCION: { editable: false, type: "boolean" },
                                    BOBINA: { editable: false },
                                    TARA: { editable: false },
                                    PESO: { editable: false },
                                    METROS: { editable: false },
                                    ELIMINADO: { editable: true, type: "boolean" },
                                }
                            }
                        }
                    },
                    filterable: true,
                    sortable: true,
                    resizable: true,
                    pageable: false,
                    editable: false,
                    columns: [       
                        { title: "ORDEN PRODUCCION", field: "CODIGOORDENPRODUCCION", filterable: false, editable: false },
                        { title: "MAQUINA", field: "NOMBREPUESTO", filterable: false, editable: false, width: "150px" },
                        { title: "PRODUCTO", field: "REFERENCIA", filterable: false, editable: false, width: "150px" },
                        { title: "PRODUCCION", field: "PRODUCCION", filterable: false, editable: false, type: "boolean" },
                        { title: "BOBINA", field: "BOBINA", filterable: false, editable: false },
                        { title: "TARA", field: "TARA", filterable: false, editable: false },
                        { title: "PESO", field: "PESO", filterable: false, editable: false },
                        { title: "METROS", field: "METROS", filterable: false, editable: false },
                        { title: "ELIMINAR", field: "ELIMINADO", filterable: false, editable: true, type: "boolean" },
                        { command: ["edit" ], title: "Editar", width: "150px" },
                    ],
                    editable: "popup"
                });
            }

            else
                alert("No hay información con el numero Op ingresado");

        }, errorHandler, JSON.stringify(filter))
    }

    else
        alert("Debe ingresar una orden de producción");
}
function GuardarBobinas() {
    
    var _idbasculaeliminar;
    var _idbasculaeliminarestado;
    var grid = $("#GridBobinas").data("kendoGrid");
    if (grid != null)
    {
        var cantidad = grid._data.length;
        if (cantidad > 1)
        {
            for (var i = 1; i <= cantidad; i++) {
                
                var Text = "tr:eq(" + i + ")"
                var data = grid.dataItem(Text);
                
                if (data != null)
                {
                    _idbasculaeliminar = data.ID;
                    _idbasculaeliminarestado = data.ELIMINADO;
                    var filter = {
                        ID: data.ID,
                        OrdenProduccion: data.CODIGOORDENPRODUCCION,
                        Valor: data.ELIMINADO
                    }
                    
                    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarBascula", function (data1) {
                    }, errorHandler, JSON.stringify(filter))

                    //Eliminar registro de EPT si estaiba este asociada
                    if (_idbasculaeliminarestado == true) {

                        var filter2 = {
                            ID: parseInt(_idbasculaeliminar)
                        }

                        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarBobinaEstiba", function (data2) {
                            
                            if (data2.value == true)
                                toastr.info("Se ha eliminado la bobinas de la estiba de EPT", "Error");

                        }, errorHandler, JSON.stringify(filter2))
                    }
                }
            }

            alert("Edición terminada correctamente");
            setTimeout(function () { location.reload(); }, 3000);
        }
    }
    
}
function GuardarConfiguracion() {

    var Impresora = document.getElementById("Impresora").value;
    var CopiasProduccion = document.getElementById("CopiasProduccion").value;
    var CopiasDesperdicio = document.getElementById("CopiasDesperdicio").value;
    var TipoImpresion = document.getElementById("TipoImpresion").value;
    var AnchoImpresion = document.getElementById("AnchoImpresion").value;
    var AltoImpresion = document.getElementById("AltoImpresion").value;
    
    if (Impresora != null & Impresora != "" & CopiasProduccion != null & CopiasProduccion != "" & CopiasDesperdicio != null & CopiasDesperdicio != "" &
        TipoImpresion != null & TipoImpresion != "" & AnchoImpresion != null & AnchoImpresion != "" & AltoImpresion != null & AltoImpresion != "") {

        CopiasProduccion = parseInt(CopiasProduccion);
        CopiasDesperdicio = parseInt(CopiasDesperdicio);

        var filter = {
            Impresora: Impresora,
            CopiasProduccion: CopiasProduccion,
            CopiasDesperdicio: CopiasDesperdicio,
            TipoImpresion: TipoImpresion,
            AnchoImpresion: AnchoImpresion,
            AltoImpresion: AltoImpresion
        }

        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/EditarConfiguracion", function (data) {

            $('#ModalConfiguracion').modal('hide');
            alert("Cambios almacenados correctamente");
        }, errorHandler, JSON.stringify(filter))
    }

    else
        alert("Todos los campos deben contener información");

    
}
function SaveBanderas(Banderas, Proceso){
    
    if (IDBascula != null && IDBascula != "")
    {
        //Bandera Negro
        var BasculaBanderas =
            {
                COLORBANDERA: "Negro",
                CANTIDADBANDERAS: Banderas.Negro,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Rojo
        var BasculaBanderas =
            {
                COLORBANDERA: "Rojo",
                CANTIDADBANDERAS: Banderas.Rojo,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Amariillo
        var BasculaBanderas =
            {
                COLORBANDERA: "Amarillo",
                CANTIDADBANDERAS: Banderas.Amarillo,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Azul
        var BasculaBanderas =
            {
                COLORBANDERA: "Azul",
                CANTIDADBANDERAS: Banderas.Azul,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))

        //Bandera Verde
        var BasculaBanderas =
            {
                COLORBANDERA: "Verde",
                CANTIDADBANDERAS: Banderas.Verde,
                NOBREPROCESO: Proceso,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULABANDERASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaBanderas))
    }   
}
function SaveObservacion(Observacion, Proceso) {

    if (IDBascula != null && IDBascula != "")
    {
        //Bandera Amariillo
        var BasculaExtras =
            {
                NOMBRECAMPO: "Observaciones",
                VALORCAMPO: Observacion,
                BASCULAID: IDBascula
            }
        request("application/json", null, null, "POST", baseUrl + "BASCULAEXTRASs", function (data) {
        }, errorHandler, JSON.stringify(BasculaExtras))
    }
}


function ConsultarTaraPorOrden($OrdenProduccion, $CodigoMaquina, $OpcionProducion) {
    
    var filter = {
        OrdenDeProduccion: $OrdenProduccion,
        CodigoPuesto: $CodigoMaquina
    }
        
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetUltimaTaraPorOrdenProduccion", function (data) {
        
        if (data.value != null) {
            var str = data.value;
            tempTara = str.replace(",", ".");
            document.getElementById("tara").value = "";

            if ($OpcionProducion == "SI")
                document.getElementById("tara").value = tempTara;
            else if ($OpcionProducion == "NO")
                document.getElementById("tara").value = 0;
            else
                document.getElementById("tara").value = "";
        }
    }, errorHandler, JSON.stringify(filter))
}

function ConsultarSiAplicaLimites($CodigoProceso) {
    
    var codigos = ['PSELLA1', 'PSELLA2', 'PSELLA4', 'PWICKET', 'PVALVUL'];//Area de sellado
    aplicalimite = codigos.includes($CodigoProceso)
}

function CerrarModalMensaje() {

    $('#ModalMensajes').modal('hide');
    $("#metros").focus();
}

function ConsultarListaDeCarretes() {

    limpiarcarretes();
    if (maquina != null && maquina != "") {
        
        //var filter = {
        //    CodigoPuesto: maquina
        //}

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/GetListaDeCarretesPorPuestoDeTrabajo",
            //data: JSON.stringify(filter),
            success: function (data) {
                
                var carrete = $("#carrete").data("kendoDropDownList");
                carrete.dataSource.data(data.value);
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
}
function limpiarcarretes() {
    var carrete = $("#carrete").data("kendoDropDownList");
    carrete.dataSource.data([]); 
    carrete.text("");
    carrete.value(""); 
}
function listadecarretesedicion(){
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/GetListaDeCarretes",
        success: function (data) {

            var grid = $("#gridlistacarrete").data("kendoGrid");
            if (grid != undefined) {
                $("#gridlistacarrete").empty();
            }
            
            $("#gridlistacarrete").kendoGrid({
                dataSource: {
                    data: data.value,
                    schema: {
                        model: {
                            fields: {
                                ID: { type: "int" },
                                NOMBRE: { type: "string" },
                                VALOR: { type: "number" },
                                DESCRIPCION: { type: "string" },
                                ESTADO: { type: "bool" },
                                TIMESPAN: { type: "datetime" },
                                CODIGOCELULA: { type: "string" }
                            }
                        }
                    },
                },
                height: 300,
                scrollable: true,
                sortable: true,
                filterable: true,
                selectable: "true",
                change: vieweditarcarrete,
                columns: [
                    { field: "NOMBRE", title: "Código", width: "20%", filterable: { multi: true } },
                    { field: "VALOR", title: "Valor (kg)", width: "20%", filterable: false },
                    { field: "DESCRIPCION", title: "Descripción", width: "30%", filterable: false },
                    { field: "ESTADO", title: "Estado", width: "10%", filterable: false },
                    { field: "CODIGOCELULA", title: "Operación", width: "20%", filterable: { multi: true } }
                ]
            });
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}
function vieweditarcarrete() {

    listarcelulascarrete();
    var grid = $("#gridlistacarrete").data("kendoGrid");
    var itemgrid = grid.dataItem(grid.select());
    $('#ModalEdicionCarrete').modal('show');
    idcarrete = itemgrid.ID;
    document.getElementById("nombrecarrete").value = itemgrid.NOMBRE;
    document.getElementById("valorcarrete").value = itemgrid.VALOR;
    document.getElementById("descripcioncarrte").value = itemgrid.DESCRIPCION;
    $("#estadocarrete").data('kendoDropDownList').value(itemgrid.ESTADO);
    $("#celulacarrete").data('kendoDropDownList').value(itemgrid.CODIGOCELULA);
    document.getElementById("BtnguardarCarrete").textContent = "Editar";
    document.getElementById("Btneliminarcarrete").disabled = false;
}

function listarcelulascarrete() {

    var celulas = $("#celulacarrete").data("kendoDropDownList");
    celulas.dataSource.data([]);
    celulas.text("");
    celulas.value("");
    
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/GetListaCelulas",
        success: function (data) {

                var celulas = $("#celulacarrete").data("kendoDropDownList");
                celulas.dataSource.data(data.value);
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}

function editarcarrete() {

    if (document.getElementById("nombrecarrete").value == null || document.getElementById("nombrecarrete").value == "")
        toastr.warning("Debe ingresar un código para la carreta", "Información");
    else if (document.getElementById("valorcarrete").value == null || document.getElementById("nombrecarrete").value == "")
        toastr.warning("Debe ingresar un valor (kg) para la carreta", "Información");
    else if ($("#celulacarrete").data('kendoDropDownList').value() == 0 || $("#celulacarrete").data('kendoDropDownList').value() == null)
        toastr.warning("Debe seleccionar un código de proceso para la carreta", "Información");

    else if (document.getElementById("BtnguardarCarrete").textContent == "Guardar") {

        var filter = {
            Id: 0,
            Nombre: document.getElementById("nombrecarrete").value,
            Valor: document.getElementById("valorcarrete").value,
            Estado: $("#estadocarrete").data('kendoDropDownList').value() == "true" ? true : false,
            Descripcion: document.getElementById("descripcioncarrte").value,
            CodigoCelula: $("#celulacarrete").data('kendoDropDownList').value()
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EditarCarrata",
            data: JSON.stringify(filter),
            success: function (data) {

                toastr.success("La carreta ha sido creada correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');

                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }

    else { 

        var filter = {
            Id: idcarrete,
            Nombre: document.getElementById("nombrecarrete").value,
            Valor: document.getElementById("valorcarrete").value,
            Estado: $("#estadocarrete").data('kendoDropDownList').value() == "true" ? true : false,
            Descripcion: document.getElementById("descripcioncarrte").value,
            CodigoCelula: $("#celulacarrete").data('kendoDropDownList').value()
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EditarCarrata",
            data: JSON.stringify(filter),
            success: function (data) {

                idcarrete = 0;
                toastr.success("La carreta ha sido editado correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');

                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
}

function crearnuevocarrete() {
    
    $('#ModalEdicionCarrete').modal('show');
    listarcelulascarrete();
    document.getElementById("nombrecarrete").value = "";
    document.getElementById("valorcarrete").value = 0;
    document.getElementById("descripcioncarrte").value = "";
    $("#estadocarrete").data('kendoDropDownList').value(true);
    document.getElementById("BtnguardarCarrete").textContent = "Guardar";
    document.getElementById("Btneliminarcarrete").disabled = true;
}

function eliminarcarrete() {

    if (idcarrete != 0) {

        var filter = {
            Id: idcarrete
        }

        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            url: baseUrl + "TUPLAEJECUCIONs/EliminarCarreta",
            data: JSON.stringify(filter),
            success: function (data) {

                idcarrete = 0;
                toastr.success("La carreta ha sido eliminada correctamente", "Información");
                $('#ModalEdicionCarrete').modal('hide');
                listadecarretesedicion();
            },
            error: function (ajaxContext) {
                toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
            }
        });
    }
    else
        toastr.warning("No se puede eliminar la carreta", "Información");
}

function ConsultarInformacionDevoluciones($codigoorden, $codigoproceso) {
    var filter = {
        CodigoOrdenProduccion: $codigoorden,
        CodigoProceso: $codigoproceso
    }

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        url: baseUrl + "TUPLAEJECUCIONs/ConsultarMaterialesPorJob",
        data: JSON.stringify(filter),
        success: function (data) {
            if (data != null) {
                document.getElementById("assembly").value = data.Assembly;
                document.getElementById("secuenciamaterial").value = data.Secuencia;
                document.getElementById("materialemitido").value = data.Material;
                document.getElementById("materialdescripcion").value = data.DescripcionMaterial;
                document.getElementById("lote").value = data.Lote;
                document.getElementById("notadevolucion").value = "";
            }
            else
                toastr.warning("No existe información de materiales por job el registro, contactar con el administrador");
        },
        error: function (ajaxContext) {
            toastr.error("Error: " + ajaxContext.responseJSON.ExceptionMessage, "Error");
        }
    });
}
