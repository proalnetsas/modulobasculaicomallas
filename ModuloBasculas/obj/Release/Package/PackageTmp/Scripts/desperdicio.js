﻿var LstPuestoPorProducto;
var nombreimpresora;

$(document).ready(function () {
    //Cultura español de kendo
    kendo.culture("es-CO");
    activeClass("CapturaProdDesp", "ParentIngreso", "liIngreso");

    $("#recursodes").kendoDropDownList({
        optionLabel: "Seleccione recurso ...",
        dataTextField: "NombrePuesto",
        dataValueField: "CodigoPuesto",
        change: selecRecurso1
    });

    var data = [
        { text: "1", value: "1" },
        { text: "2", value: "2" },
        { text: "3", value: "3" },
        { text: "4", value: "4" },
        { text: "5", value: "5" },
        { text: "6", value: "6" }
    ];

    $("#numerocopiasdes").kendoDropDownList({
        optionLabel: "Seleccione n° copias ...",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0
    });

    $("#motivodesperdiciodes").kendoDropDownList({
        optionLabel: "Seleccione el motivo ...",
        dataTextField: "NOMBRE",
        dataValueField: "CODIGO",
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: baseUrl + "CAUSALESDESPERDICIOs",
                }
            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
            },
        }
    });

    obtenerpersodes();
});
function obtenerpersodes() {
    
    connection = $.hubConnection("http://localhost:9992");
    proxyBascula = connection.createHubProxy("clientHub");
    connection.start({ logging: true })
    proxyBascula.on('data', function (dato) {
        if (dato != null && dato.Valor != undefined) {
            document.getElementById("pesobrutodes").value = dato.Valor;
            nombreimpresora = dato.NombreImpresora
            //calculoPesodes();
        }
    });
    connection.error(function (error) {

    });
}
//function calculoPesodes() {
//    if (document.getElementById("tarades").value == "")
//        document.getElementById("tarades").value = 0;

//    document.getElementById("pesonetodes").value = document.getElementById("pesobrutodes").value - document.getElementById("tarades").value;
//}
function selecRecurso1(e) {

    if ($("#recursodes").val()) {
        var recurso = $("#recursodes").data("kendoDropDownList");
        for (var i = 0; i < LstPuestoPorProducto.length; i++) {
            if (LstPuestoPorProducto[i].CodigoPuesto == recurso._selectedValue) {
                document.getElementById("codigoproductodes").value = LstPuestoPorProducto[i].CodigoProducto;
                document.getElementById("productodes").value = LstPuestoPorProducto[i].NombreProducto;
                document.getElementById("codigoprocesodes").value = LstPuestoPorProducto[i].CodigoProceso;
                document.getElementById("nombreclientedes").value = LstPuestoPorProducto[i].NombreCliente;
            }
        }
    }
    else
        limpiardes();
}
function consultarOrdenProduccion() {

    limpiardes();
    var filter = {
        OrdenProduccion: document.getElementById("ordenproduccion").value
    }    
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetTuplasPorOPDesperdicio", function (data) {
       
        LstPuestoPorProducto = data.value;
        var recurso = $("#recursodes").data("kendoDropDownList");
        recurso.dataSource.data(LstPuestoPorProducto);
        if (LstPuestoPorProducto.length > 0)
            recurso.text("Seleccione recurso ...");
        else
            recurso.text("");
    }, errorHandler, JSON.stringify(filter))
}
function limpiardes() {

    document.getElementById("codigoproductodes").value = "";
    document.getElementById("productodes").value = "";
    document.getElementById("codigoprocesodes").value = "";
    document.getElementById("nombreclientedes").value = "";
}
function consultarempleadodes() {

    var filter = {
        CodigoOperario: document.getElementById("codigoempleadodes").value
    }
    document.getElementById("nombreempleadodes").value = "";
    request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/GetNameEmpleadoDesperdicio", function (data) {
        if (data.value != undefined)
            document.getElementById("nombreempleadodes").value = data.value;
    }, errorHandler, JSON.stringify(filter))
}
function imprimirdes(){

    if(document.getElementById("ordenproduccion").value == "" || document.getElementById("ordenproduccion").value == null)
        toastr.warning('Debe ingresar un valor para el JOB', 'Información')
    else if (document.getElementById("recursodes").value == "" || document.getElementById("recursodes").value == null)
        toastr.warning('Debe seleccionar un valor para el recurso', 'Información')
    else if (document.getElementById("codigoprocesodes").value == "" || document.getElementById("codigoprocesodes").value == null)
        toastr.warning('Debe ingresar un valor valido de JOB para consultar el ID de Operación', 'Información')
    else if (document.getElementById("codigoproductodes").value == "" || document.getElementById("codigoproductodes").value == null)
        toastr.warning('Debe ingresar un valor valido de JOB para consultar la parte', 'Información')
    else if (document.getElementById("productodes").value == "" || document.getElementById("productodes").value == null)
        toastr.warning('Debe ingresar un valor valido de JOB para consultar la descripción de la parte', 'Información')
    else if (document.getElementById("nombreclientedes").value == "" || document.getElementById("nombreclientedes").value == null)
        toastr.warning('Debe ingresar un valor valido de JOB para consultar la descripción del cliente', 'Información')
    else if (document.getElementById("codigoempleadodes").value == "" || document.getElementById("codigoempleadodes").value == null)
        toastr.warning('Debe ingresar un valor valido para el ID del empleado', 'Información')
    else if (document.getElementById("nombreempleadodes").value == "" || document.getElementById("nombreempleadodes").value == null)
        toastr.warning('Debe ingresar un valor valido de ID del empleado para consultar el nombre del empleado', 'Información')
    else if (document.getElementById("pesobrutodes").value == "" || document.getElementById("pesobrutodes").value == null)
        toastr.warning('Debe ingresar un valor valido para el peso bruto', 'Información')
    //else if (document.getElementById("pesonetodes").value == "" || document.getElementById("pesonetodes").value == null)
    //    toastr.warning('Debe ingresar un valor valido para el peso neto', 'Información')
    else if (document.getElementById("metrosdes").value == "" || document.getElementById("metrosdes").value == null)
        toastr.warning('Debe ingresar un valor valido para los metros/unidades', 'Información')
    else if (document.getElementById("motivodesperdiciodes").value == "" || document.getElementById("motivodesperdiciodes").value == null)
        toastr.warning('Debe seleccionar un valor para el motivo de desperdicio', 'Información')
    else if (document.getElementById("numerocopiasdes").value == "" || document.getElementById("numerocopiasdes").value == null)
        toastr.warning('Debe seleccionar un valor para el numero de copias', 'Información')
    else {

        var filter = {
            OrdenDeProduccion: document.getElementById("ordenproduccion").value,
            Cliente: document.getElementById("nombreclientedes").value,
            ParteyDescripcionProducto: document.getElementById("codigoproductodes").value + " " + document.getElementById("productodes").value,
            ParteyDescripcionDesperdicio: "MP80578 PP_PEBD LAMINA MUCHA TINTA",
            CausalDesperdicio: document.getElementById("motivodesperdiciodes").value,
            CodigoQrTexto: "",
            CodigoQrRuta: "",
            NombreRecurso: document.getElementById("recursodes").text,
            CodigoProceso: document.getElementById("codigoprocesodes").value,
            NombreEmpleado: document.getElementById("nombreempleadodes").value,
            Fecha: "",
            PesoBruto: parseFloat(document.getElementById("pesobrutodes").value).toFixed(2),
            Metros: parseFloat(document.getElementById("metrosdes").value).toFixed(2),
            Unidad: "m",
            CodigoPuesto: document.getElementById("recursodes").value,
            CodigoProducto: document.getElementById("codigoproductodes").value
        }
        
        request("application/json", null, null, "POST", baseUrl + "TUPLAEJECUCIONs/PrintDesperdicio", function (data) {
            
        }, errorHandler, JSON.stringify(filter))
    }
}