﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModuloBasculas.Service;

namespace ModuloBasculas.Controllers
{
    public class BasculaController : Controller
    {
        // GET: Bascula
        public ActionResult Index()
        {
            ViewData["timeCapture"] = System.Configuration.ConfigurationManager.AppSettings["timeCapture"];
            return View();
        }

        public JsonResult ObtenerCapturaBascula()
        {
            try
            {
                CapturaBasculaRead read = new CapturaBasculaRead();
                double valor = read.ObtenerCapturaBascula();
                return Json(valor, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                if (ex.InnerException != null)
                    error = ex.InnerException.Message;
                return Json(new { Errors = error }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Version()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return Content(fvi.FileVersion);
        }
    }
}