﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace ModuloBasculas.Controllers.Reportes
{
    public class ImprimirReportes
    {
        private int m_currentPageIndex;
        private IList<Stream> m_streams;

        public void Export(LocalReport report, string TipoImpresion)
        {
            string PageWidth = "";
            string PageHeight = "";
            if (Convert.ToBoolean(WebConfigurationManager.AppSettings["PreguntartipoDeImpresion"]) == true)
            {
                if (TipoImpresion == "Generico")
                {
                    PageWidth = WebConfigurationManager.AppSettings["PageWidth"].ToString();
                    PageHeight = WebConfigurationManager.AppSettings["PageHeight"].ToString();
                }

                else
                {
                    PageWidth = WebConfigurationManager.AppSettings["PageWidth" + TipoImpresion].ToString();
                    PageHeight = WebConfigurationManager.AppSettings["PageHeight" + TipoImpresion].ToString();
                }         
            }
            else
            {
                PageWidth = WebConfigurationManager.AppSettings["PageWidth"].ToString();
                PageHeight = WebConfigurationManager.AppSettings["PageHeight"].ToString();
            }

            string deviceInfo = "<DeviceInfo>\r\n<OutputFormat>EMF</OutputFormat>\r\n<PageWidth>" + PageWidth + "</PageWidth>\r\n<PageHeight>" + PageHeight + "</PageHeight>\r\n<MarginTop>0in</MarginTop>\r\n<MarginLeft>0in</MarginLeft>\r\n<MarginRight>0in</MarginRight>\r\n<MarginBottom>0in</MarginBottom>\r\n</DeviceInfo>";

            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;

        }

        public void Export(LocalReport report, string tamañoImpresion, bool val )
        {
            string PageWidth = tamañoImpresion.Split('-')[0].ToString();
            string PageHeight = tamañoImpresion.Split('-')[1].ToString();

            string deviceInfo = "<DeviceInfo>\r\n<OutputFormat>EMF</OutputFormat>\r\n<PageWidth>" + PageWidth + "</PageWidth>\r\n<PageHeight>" + PageHeight + "</PageHeight>\r\n<MarginTop>0cm</MarginTop>\r\n<MarginLeft>0cm</MarginLeft>\r\n<MarginRight>0cm</MarginRight>\r\n<MarginBottom>0cm</MarginBottom>\r\n</DeviceInfo>";

            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;

        }

        public void Print(Int16 NumeroCopias)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");

            PrintDocument printDoc = new PrintDocument();
            string NamePrint = WebConfigurationManager.AppSettings["NamePrintDefault"].ToString();
            Int16 Copies = NumeroCopias;
            printDoc.PrinterSettings.PrinterName = NamePrint;
            printDoc.PrinterSettings.Copies = Copies;
            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            m_currentPageIndex = 0;
            printDoc.PrinterSettings.PrintRange = PrintRange.SomePages;

            printDoc.Print();
        }

        public void Print(Int16 NumeroCopias, string impresora)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");

            PrintDocument printDoc = new PrintDocument();
            Int16 Copies = NumeroCopias;
            printDoc.PrinterSettings.PrinterName = impresora;
            //printDoc.PrinterSettings.PrinterName = @"\\Lenovo-pc\EPSON L395 Series";
            printDoc.PrinterSettings.Copies = Copies;

            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            m_currentPageIndex = 0;
            
            printDoc.PrinterSettings.PrintRange = PrintRange.SomePages;
            printDoc.Print();
        }

        public void PrintGenerico(Int16 NumeroCopias)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");

            PrintDocument printDoc = new PrintDocument();

            string NamePrint = WebConfigurationManager.AppSettings["NamePrintDefault"].ToString();
            Int16 Copies = NumeroCopias;
            printDoc.PrinterSettings.PrinterName = NamePrint;
            printDoc.PrinterSettings.Copies = Copies;
            //if (!printDoc.PrinterSettings.IsValid)
            //{
            //    throw new Exception("Error: cannot find the default printer.");
            //}
            //else
            //{
            //    printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            //    m_currentPageIndex = 0;
            //    printDoc.Print();
            //}

            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            m_currentPageIndex = 0;
            printDoc.PrinterSettings.PrintRange = PrintRange.SomePages;
            printDoc.Print();

        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                230,
                0,
                320,
                185);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
    }
}