﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ModuloBasculas.Models;

namespace ModuloBasculas.Controllers
{
    public class BASCULAsController : ODataController
    {
        private Entities db = new Entities();

        // GET: odata/BASCULAs
        [EnableQuery]
        public IQueryable<BASCULA> GetBASCULAs()
        {
            return db.BASCULA;
        }

        // GET: odata/BASCULAs(5)
        [EnableQuery]
        public SingleResult<BASCULA> GetBASCULA([FromODataUri] int key)
        {
            return SingleResult.Create(db.BASCULA.Where(bASCULA => bASCULA.ID == key));
        }

        // PUT: odata/BASCULAs(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<BASCULA> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BASCULA bASCULA = db.BASCULA.Find(key);
            if (bASCULA == null)
            {
                return NotFound();
            }

            patch.Put(bASCULA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BASCULAExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(bASCULA);
        }

        // POST: odata/BASCULAs
        public IHttpActionResult Post(BASCULA bASCULA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            /*Setear Hora de bascula*/
            //bASCULA.FECHA = new DateTime(bASCULA.FECHA.Value.Year, bASCULA.FECHA.Value.Month, bASCULA.FECHA.Value.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            bASCULA.TIMESPAN = DateTime.Now;
            db.BASCULA.Add(bASCULA);
            db.SaveChanges();
            return Created(bASCULA);
        }

        // PATCH: odata/BASCULAs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<BASCULA> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BASCULA bASCULA = db.BASCULA.Find(key);
            if (bASCULA == null)
            {
                return NotFound();
            }

            patch.Patch(bASCULA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BASCULAExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(bASCULA);
        }

        // DELETE: odata/BASCULAs(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            BASCULA bASCULA = db.BASCULA.Find(key);
            if (bASCULA == null)
            {
                return NotFound();
            }

            db.BASCULA.Remove(bASCULA);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BASCULAExists(int key)
        {
            return db.BASCULA.Count(e => e.ID == key) > 0;
        }
    }
}
