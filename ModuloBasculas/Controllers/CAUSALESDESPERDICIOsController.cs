﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ModuloBasculas.Models;

namespace ModuloBasculas.Controllers
{
    public class CAUSALESDESPERDICIOsController : ODataController
    {
        private Entities db = new Entities();

        // GET: odata/CAUSALESDESPERDICIOs
        [EnableQuery]
        public IQueryable<CAUSALESDESPERDICIO> GetCAUSALESDESPERDICIOs()
        {
            return db.CAUSALESDESPERDICIO;
        }

        // GET: odata/CAUSALESDESPERDICIOs(5)
        [EnableQuery]
        public SingleResult<CAUSALESDESPERDICIO> GetCAUSALESDESPERDICIO([FromODataUri] string key)
        {
            return SingleResult.Create(db.CAUSALESDESPERDICIO.Where(cAUSALESDESPERDICIO => cAUSALESDESPERDICIO.CODIGO == key));
        }

        // PUT: odata/CAUSALESDESPERDICIOs(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<CAUSALESDESPERDICIO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CAUSALESDESPERDICIO cAUSALESDESPERDICIO = db.CAUSALESDESPERDICIO.Find(key);
            if (cAUSALESDESPERDICIO == null)
            {
                return NotFound();
            }

            patch.Put(cAUSALESDESPERDICIO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAUSALESDESPERDICIOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(cAUSALESDESPERDICIO);
        }

        // POST: odata/CAUSALESDESPERDICIOs
        public IHttpActionResult Post(CAUSALESDESPERDICIO cAUSALESDESPERDICIO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAUSALESDESPERDICIO.Add(cAUSALESDESPERDICIO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CAUSALESDESPERDICIOExists(cAUSALESDESPERDICIO.CODIGO))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(cAUSALESDESPERDICIO);
        }

        // PATCH: odata/CAUSALESDESPERDICIOs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<CAUSALESDESPERDICIO> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CAUSALESDESPERDICIO cAUSALESDESPERDICIO = db.CAUSALESDESPERDICIO.Find(key);
            if (cAUSALESDESPERDICIO == null)
            {
                return NotFound();
            }

            patch.Patch(cAUSALESDESPERDICIO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAUSALESDESPERDICIOExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(cAUSALESDESPERDICIO);
        }

        // DELETE: odata/CAUSALESDESPERDICIOs(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            CAUSALESDESPERDICIO cAUSALESDESPERDICIO = db.CAUSALESDESPERDICIO.Find(key);
            if (cAUSALESDESPERDICIO == null)
            {
                return NotFound();
            }

            db.CAUSALESDESPERDICIO.Remove(cAUSALESDESPERDICIO);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAUSALESDESPERDICIOExists(string key)
        {
            return db.CAUSALESDESPERDICIO.Count(e => e.CODIGO == key) > 0;
        }
    }
}
