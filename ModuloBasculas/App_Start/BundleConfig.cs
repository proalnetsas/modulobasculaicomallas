﻿using System.Web;
using System.Web.Optimization;

namespace ModuloBasculas
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/bootstrap.css",
                //"~/Content/site.css"));
                       "~/Content/AdminLTE.css",
                       "~/Content/font-awesome-4.1.0/css/font-awesome.min.css",
                       "~/Content/ionicons.css"));

            bundles.Add(new ScriptBundle("~/bundles/adminlte").Include(
                "~/Scripts/AdminLTE/app.js",
                "~/Scripts/common.js"));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
               "~/Content/kendo/2014.1.415/kendo.common.min.css",
               "~/Content/kendo/2014.1.415/kendo.blueopal.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
               "~/Scripts/kendo/2014.1.415/kendo.all.min.js",
               "~/Scripts/kendo/2014.1.415/kendo.aspnetmvc.min.js",
               "~/Scripts/kendo/cultures/kendo.culture.es-CO.js"));

            bundles.Add(new ScriptBundle("~/bundles/ingresoproddesp").Include(
                "~/Scripts/ingresoproddesp.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/desperdicio").Include(
                "~/Scripts/desperdicio.js"
                ));

            bundles.Add(new StyleBundle("~/Content/ingresoproddesp").Include(
                 "~/Content/ingresoproddesp.css"
                ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
