﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using ModuloBasculas.Models;

namespace ModuloBasculas
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<TUPLAEJECUCION>("TUPLAEJECUCIONs");
            GetMaquinasOP(builder);
            GetProductosOP(builder);
            GetAnchoLargo(builder);
            GetCantidadAPesarOrdenProduccion(builder);
            GetCantidadPesadaOrdenProduccion(builder);
            GetDetalleOP(builder);
            GetTipoImpresion(builder);
            GetDatosExtrusion(builder);
            GetPrintExtrusion(builder);
            GetPrintFlexografia(builder);
            GetPrintLaminado(builder);
            GetPrintCorte(builder);
            GetPrintSelladoPt(builder);
            GetPrintSelladoPq(builder);
            GetInformacionCliente(builder);
            GetPrintGenerico(builder);
            GetLoginEdicionBobinas(builder);
            EdicionOrdenProduccionBobinas(builder);
            EditarBascula(builder);
            ConsultarConfiguracion(builder);
            EditarConfiguracion(builder);
            GetConsecutivoBobina(builder);
            GetBobinasPorCaja(builder);
            GetPesoRepeticion(builder);
            GetPrintCorteIngles(builder);
            GetPonum(builder);
            GetFechaBatch(builder);
            GetPrintSellado(builder);
            GetPrintSelladoIngles(builder);
            GetUltimaTaraPorOrdenProduccion(builder);
            GetBrand(builder);
            GetConsultarLimitesParaSellado(builder);

            GetTuplasPorOPDesperdicio(builder);
            GetNameEmpleadoDesperdicio(builder);
            PrintDesperdicio(builder);
            PrintReproceso(builder);
            PrintCoExtrusoraGeneral(builder);
            PrintFlexoGeneral(builder);
            PrintCorteGeneral(builder);
            PrintCorteQuala(builder);
            PrintCorteNutresa(builder);
            PrintCorteIngles(builder);
            PrintSelladoGeneral(builder);
            PrintSelladoQuala(builder);
            PrintSelladoNutresa(builder);
            PrintSelladoIngles(builder);
            PrintProduccionGenerico(builder);
            GetEsquemaDeImpresion(builder);
            GetInformacionExtraImpresion(builder);
            GetEsquemaDeImpresionPorProceso(builder);
            GetEditarEstandares(builder);
            GetListaCelulas(builder);
            EditarCarrata(builder);
            EliminarCarreta(builder);
            ConsultarEstibaAbierta(builder);
            InsertarDetalleEstiba(builder);
            ConsultarEstibaAtope(builder);
            EditarBobinaEstiba(builder);
            ConsultarMaterialesPorJob(builder);
            AlmacenarRegistroDevolucion(builder);

            builder.EntitySet<ENTRADAEJECUCION>("ENTRADAEJECUCION");
            builder.EntitySet<ORDENPRODUCCIONXPRODUCTO>("ORDENPRODUCCIONXPRODUCTO");
            builder.EntitySet<PERSONALQUEEJECUTO>("PERSONALQUEEJECUTO");
            builder.EntitySet<ORDENES_DE_PRODUCCION>("ORDENES_DE_PRODUCCION");
            builder.EntitySet<PUESTOTRABAJOSEGUNPRODUCTO>("PUESTOTRABAJOSEGUNPRODUCTO");
            builder.EntitySet<PERSONAL>("PERSONAL");
            builder.EntitySet<TIPOSDETURNOS>("TIPOSDETURNOS");
            builder.EntitySet<PRODUCTOS>("PRODUCTOS");
            builder.EntitySet<CAUSALESDESPERDICIO>("CAUSALESDESPERDICIOs");
            builder.EntitySet<BASCULA>("BASCULAs");
            
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
        static void GetMaquinasOP(ODataConventionModelBuilder builder)
        {
            var getmaquinasop = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetMaquinasOP");
            getmaquinasop.Parameter<string>("OrdenProduccion");
            getmaquinasop.Parameter<DateTime>("Fecha");
            getmaquinasop.ReturnsCollectionFromEntitySet<PUESTOSDETRABAJO>("GetMaquinasOP");
        }

        static void GetProductosOP(ODataConventionModelBuilder builder)
        {
            var getproductosop = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetProductosOP");
            getproductosop.Parameter<string>("OrdenProduccion");
            getproductosop.ReturnsCollectionFromEntitySet<ProductosDTO>("GetProductosOP");
        }

        static void GetAnchoLargo(ODataConventionModelBuilder builder)
        {
            var getancholargo = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetAnchoLargo");
            getancholargo.Parameter<string>("PRODUCTO");
            getancholargo.ReturnsCollectionFromEntitySet<ProductosDTO>("GetAnchoLargo");
        }

        static void GetCantidadAPesarOrdenProduccion(ODataConventionModelBuilder builder)
        {
            var getCantidadAPesarOrdenProduccion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetCantidadAPesarOrdenProduccion");
            getCantidadAPesarOrdenProduccion.Parameter<string>("CODIGOORDENPRODUCCION");

        }

        static void GetCantidadPesadaOrdenProduccion(ODataConventionModelBuilder builder)
        {
            var etCantidadPesadaOrdenProduccion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetCantidadPesadaOrdenProduccion");
            etCantidadPesadaOrdenProduccion.Parameter<string>("CODIGOORDENPRODUCCION");

        }


        

        static void GetDetalleOP(ODataConventionModelBuilder builder)
        {
            var getdetalleop = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetDetalleOP");
            getdetalleop.Parameter<string>("OrdenProduccion");
            getdetalleop.Parameter<DateTime>("Fecha");
            getdetalleop.Parameter<string>("CodigoPuesto");
            getdetalleop.ReturnsFromEntitySet<Detalle>("GetDetalleOP");
        }


        //Consultar Tipo de impresion
        static void GetTipoImpresion(ODataConventionModelBuilder builder)
        {
            var gettipoimpresion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetTipoImpresion");
            //gettipoimpresion.ReturnsFromEntitySet<string>("GetTipoImpresion");
        }

        //Consultar Información de llenado para la impresión
        static void GetDatosExtrusion(ODataConventionModelBuilder builder)
        {
            var getdatosextrusion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetDatosExtrusion");
            getdatosextrusion.Parameter<string>("OrdenProduccion");
            getdatosextrusion.ReturnsFromEntitySet<Detalle>("GetDatosExtrusion");
        }

        //Consultar Información de llenado para la impresión
        static void GetPrintExtrusion(ODataConventionModelBuilder builder)
        {
            var getprintextrusion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintExtrusion");
            getprintextrusion.Parameter<string>("OrdenProduccion");
            getprintextrusion.Parameter<string>("Fecha");
            getprintextrusion.Parameter<string>("Item");
            getprintextrusion.Parameter<string>("Formula");
            getprintextrusion.Parameter<string>("Operario");
            getprintextrusion.Parameter<string>("PesoBruto");
            getprintextrusion.Parameter<string>("PesoNeto");
            getprintextrusion.Parameter<string>("Rollo");
            getprintextrusion.Parameter<string>("Maquina");
            getprintextrusion.Parameter<string>("Metro");

            getprintextrusion.Parameter<string>("CodigoBarras");
            getprintextrusion.Parameter<string>("CodigoBarrasTexto");
            getprintextrusion.Parameter<string>("Ancho");
            getprintextrusion.Parameter<string>("Calibre");
            getprintextrusion.Parameter<string>("Material");
            getprintextrusion.Parameter<string>("ProcesoSiguiente");
            getprintextrusion.Parameter<string>("TipoProduccion");

            getprintextrusion.Parameter<Int32>("Negro");
            getprintextrusion.Parameter<Int32>("Rojo");
            getprintextrusion.Parameter<Int32>("Amarillo");
            getprintextrusion.Parameter<Int32>("Azul");
            getprintextrusion.Parameter<Int32>("Verde");
            getprintextrusion.Parameter<string>("Observaciones");
        }

        static void GetPrintFlexografia(ODataConventionModelBuilder builder)
        {
            var getprintflexografia = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintFlexografia");
            getprintflexografia.Parameter<string>("OrdenProduccion");
            getprintflexografia.Parameter<string>("Fecha");
            getprintflexografia.Parameter<string>("Turno");          
            getprintflexografia.Parameter<string>("Operario");
            getprintflexografia.Parameter<string>("Item");
            getprintflexografia.Parameter<string>("Referencia");
            getprintflexografia.Parameter<string>("Metros");
            getprintflexografia.Parameter<string>("PesoBruto");
            getprintflexografia.Parameter<string>("PesoNeto");
            getprintflexografia.Parameter<string>("Rollo");
            getprintflexografia.Parameter<string>("Observaciones");


            getprintflexografia.Parameter<string>("CodigoBarras");
            getprintflexografia.Parameter<string>("CodigoBarrasTexto");
            getprintflexografia.Parameter<string>("Material");
            getprintflexografia.Parameter<string>("ProcesoSiguiente");
            getprintflexografia.Parameter<string>("Cliente");
            getprintflexografia.Parameter<string>("TipoProduccion");

            getprintflexografia.Parameter<Int32>("Negro");
            getprintflexografia.Parameter<Int32>("Rojo");
            getprintflexografia.Parameter<Int32>("Amarillo");
            getprintflexografia.Parameter<Int32>("Azul");
            getprintflexografia.Parameter<Int32>("Verde");
            getprintflexografia.Parameter<string>("Maquina");

        }

        static void GetPrintLaminado(ODataConventionModelBuilder builder)
        {
            var getprintlaminado = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintLaminado");
            getprintlaminado.Parameter<string>("OrdenProduccion");
            getprintlaminado.Parameter<string>("Fecha");
            getprintlaminado.Parameter<string>("Turno");
            getprintlaminado.Parameter<string>("Operario");
            getprintlaminado.Parameter<string>("Item");
            getprintlaminado.Parameter<string>("Referencia");
            getprintlaminado.Parameter<string>("Metros");
            getprintlaminado.Parameter<string>("PesoBruto");
            getprintlaminado.Parameter<string>("PesoNeto");
            getprintlaminado.Parameter<string>("Rollo");
            getprintlaminado.Parameter<string>("Observaciones");

            getprintlaminado.Parameter<string>("CodigoBarras");
            getprintlaminado.Parameter<string>("CodigoBarrasTexto");
            getprintlaminado.Parameter<string>("Material");
            getprintlaminado.Parameter<string>("ProcesoSiguiente");
            getprintlaminado.Parameter<string>("Cliente");
            getprintlaminado.Parameter<string>("TipoProduccion");

            getprintlaminado.Parameter<Int32>("Negro");
            getprintlaminado.Parameter<Int32>("Rojo");
            getprintlaminado.Parameter<Int32>("Amarillo");
            getprintlaminado.Parameter<Int32>("Azul");
            getprintlaminado.Parameter<Int32>("Verde");
            getprintlaminado.Parameter<string>("Maquina");
        }

        static void GetPrintCorte(ODataConventionModelBuilder builder)
        {
            var getprintcorte = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintCorte");
            getprintcorte.Parameter<string>("Turno");
            getprintcorte.Parameter<string>("Maquina");
            getprintcorte.Parameter<string>("CodigoCliente");
            getprintcorte.Parameter<string>("Repeticiones");
            getprintcorte.Parameter<string>("Bobinas");
            getprintcorte.Parameter<string>("Consecutivo");
            getprintcorte.Parameter<string>("CodigoOperario");
            getprintcorte.Parameter<string>("Cliente");
            getprintcorte.Parameter<string>("CodigoBarrasFijo");
            getprintcorte.Parameter<string>("CodigoBarrasEan14");
            getprintcorte.Parameter<string>("Producto");
            getprintcorte.Parameter<string>("CodigoBarrasEan132");
            getprintcorte.Parameter<string>("CodigoBarrasEan132Texto");
            getprintcorte.Parameter<double>("PesoNeto");
            getprintcorte.Parameter<string>("OrdenProduccion");
            getprintcorte.Parameter<double>("PesoBruto");
            getprintcorte.Parameter<string>("Fecha");
            getprintcorte.Parameter<string>("CodigoPuesto");
            getprintcorte.Parameter<string>("CodigoQrRuta");
            getprintcorte.Parameter<string>("TipoProduccion");
            getprintcorte.Parameter<string>("CodigoQr");
            getprintcorte.Parameter<string>("CodigoProducto");
            getprintcorte.Parameter<string>("CodigoBarrasFijoTexto");
            getprintcorte.Parameter<string>("FechaVence");
            getprintcorte.Parameter<string>("NitCliente");
        }

        static void GetPrintSelladoPt(ODataConventionModelBuilder builder)
        {
            var getprintselladopt = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintSelladoPt");
            getprintselladopt.Parameter<string>("OrdenProduccion");
            getprintselladopt.Parameter<string>("Turno");
            getprintselladopt.Parameter<string>("Material");
            getprintselladopt.Parameter<string>("Operario");
            getprintselladopt.Parameter<string>("Cliente");
            getprintselladopt.Parameter<string>("Item");
            getprintselladopt.Parameter<string>("Referencia");
            getprintselladopt.Parameter<string>("PesoNeto");
            getprintselladopt.Parameter<string>("Cantidad");
            getprintselladopt.Parameter<string>("Fecha");
            getprintselladopt.Parameter<string>("OC");
            getprintselladopt.Parameter<string>("CodigoCliente");
            getprintselladopt.Parameter<string>("CodigoQr");
            getprintselladopt.Parameter<string>("CodigoQrRuta");
            getprintselladopt.Parameter<string>("CodigoDeBarras");
            getprintselladopt.Parameter<string>("CodigoDeBarrasTexto");
            getprintselladopt.Parameter<string>("PreCodigoDeBarras");
            getprintselladopt.Parameter<string>("TipoProduccion");
        }

        static void GetPrintSelladoPq(ODataConventionModelBuilder builder)
        {
            var getprintselladopq = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintSelladoPq");
            getprintselladopq.Parameter<string>("OrdenProduccion");
            getprintselladopq.Parameter<string>("Turno");
            getprintselladopq.Parameter<string>("Material");
            getprintselladopq.Parameter<string>("Operario");
            getprintselladopq.Parameter<string>("Cliente");
            getprintselladopq.Parameter<string>("Item");
            getprintselladopq.Parameter<string>("Referencia");
            getprintselladopq.Parameter<string>("Paquete");
            getprintselladopq.Parameter<string>("Unidad");
            getprintselladopq.Parameter<string>("Fecha");
            getprintselladopq.Parameter<string>("OC");
            getprintselladopq.Parameter<string>("CodigoCliente");
            getprintselladopq.Parameter<string>("CodigoQr");
            getprintselladopq.Parameter<string>("CodigoQrRuta");
            getprintselladopq.Parameter<string>("CodigoDeBarras");
            getprintselladopq.Parameter<string>("CodigoDeBarrasTexto");
            getprintselladopq.Parameter<string>("PreCodigoDeBarras");
            getprintselladopq.Parameter<string>("TipoProduccion");
        }

        static void GetInformacionCliente(ODataConventionModelBuilder builder)
        {
            var getinformacioncliente = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetInformacionCliente");
            getinformacioncliente.Parameter<string>("OrdenProduccion");
            getinformacioncliente.Parameter<string>("CodigoProucto");
            getinformacioncliente.ReturnsFromEntitySet<Detalle>("GetInformacionCliente");
        }


        static void GetPrintGenerico(ODataConventionModelBuilder builder)
        {
            var getprintgenerico = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintGenerico");
            getprintgenerico.Parameter<string>("OrdenProduccion");
            getprintgenerico.Parameter<string>("Fecha");
            getprintgenerico.Parameter<string>("Turno");
            getprintgenerico.Parameter<string>("Operario");
            getprintgenerico.Parameter<string>("Cliente");
            getprintgenerico.Parameter<string>("CodigoCliente");
            getprintgenerico.Parameter<string>("Item");
            getprintgenerico.Parameter<string>("Referencia");
            getprintgenerico.Parameter<string>("Maquina");
            getprintgenerico.Parameter<Int32>("NumeroCopias");
        }

        static void GetLoginEdicionBobinas(ODataConventionModelBuilder builder)
        {
            var getloginedicionbobinas = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetLoginEdicionBobinas");
            getloginedicionbobinas.Parameter<string>("Username");
            getloginedicionbobinas.Parameter<string>("Password");
            getloginedicionbobinas.ReturnsFromEntitySet<Detalle>("GetLoginEdicionBobinas");
        }

        static void EdicionOrdenProduccionBobinas(ODataConventionModelBuilder builder)
        {
            var edicionordenproduccionbobinas = builder.Entity<TUPLAEJECUCION>().Collection.Action("EdicionOrdenProduccionBobinas");
            edicionordenproduccionbobinas.Parameter<string>("OrdenProduccion");
            edicionordenproduccionbobinas.ReturnsFromEntitySet<DetalleBascula>("EdicionOrdenProduccionBobinas");

            var resumenOP = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetResumenOp");
            resumenOP.Parameter<string>("OrdenProduccion");
            resumenOP.ReturnsFromEntitySet<ResumenOPDTO>("GetResumenOp");

        }

        static void EditarBascula(ODataConventionModelBuilder builder)
        {
            var editarbascula = builder.Entity<TUPLAEJECUCION>().Collection.Action("EditarBascula");
            editarbascula.Parameter<string>("OrdenProduccion");
            editarbascula.Parameter<Int32>("ID");
            editarbascula.Parameter<bool>("Valor");
            editarbascula.ReturnsFromEntitySet<DetalleBascula>("EditarBascula");
        }

        //Consultar datos de configuracion
        static void ConsultarConfiguracion(ODataConventionModelBuilder builder)
        {
            var consultarconfiguracion = builder.Entity<TUPLAEJECUCION>().Collection.Action("ConsultarConfiguracion");
        }

        //Editar configuracion de bascula
        static void EditarConfiguracion(ODataConventionModelBuilder builder)
        {
            var editarconfiguracion = builder.Entity<TUPLAEJECUCION>().Collection.Action("EditarConfiguracion");
            editarconfiguracion.Parameter<string>("Impresora");
            editarconfiguracion.Parameter<Int32>("CopiasProduccion");
            editarconfiguracion.Parameter<Int32>("CopiasDesperdicio");
            editarconfiguracion.Parameter<string>("TipoImpresion");
            editarconfiguracion.Parameter<string>("AnchoImpresion");
            editarconfiguracion.Parameter<string>("AltoImpresion");
            editarconfiguracion.ReturnsFromEntitySet<DetalleBascula>("EditarConfiguracion");
        }

        static void GetConsecutivoBobina(ODataConventionModelBuilder builder)
        {
            var getconsecutivobobina = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetConsecutivoBobina");
            getconsecutivobobina.Parameter<string>("OrdenProduccion");
            getconsecutivobobina.Parameter<string>("CodigoPuesto");
            getconsecutivobobina.Parameter<string>("TipoProduccion");
            //getconsecutivobobina.ReturnsFromEntitySet<string>("GetConsecutivoBobina");
        }

        static void GetBobinasPorCaja(ODataConventionModelBuilder builder)
        {
            var getbobinasporcaja = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetBobinasPorCaja");
            getbobinasporcaja.Parameter<string>("OrdenProduccion");
            getbobinasporcaja.Parameter<string>("CodigoProducto");
            //getbobinasporcaja.ReturnsFromEntitySet<string>("GetBobinasPorCaja");
        }

        static void GetPesoRepeticion(ODataConventionModelBuilder builder)
        {
            var getpesorepeticion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPesoRepeticion");
            getpesorepeticion.Parameter<string>("OrdenProduccion");
            getpesorepeticion.Parameter<string>("CodigoProducto");
            //getpesorepeticion.ReturnsFromEntitySet<string>("GetPesoRepeticion");
        }

        static void GetPrintCorteIngles(ODataConventionModelBuilder builder)
        {
            var getprintcorteingles = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintCorteIngles");
            getprintcorteingles.Parameter<string>("Turno");
            getprintcorteingles.Parameter<string>("Maquina");
            getprintcorteingles.Parameter<string>("CodigoCliente");
            getprintcorteingles.Parameter<string>("Repeticiones");
            getprintcorteingles.Parameter<string>("Bobinas");
            getprintcorteingles.Parameter<string>("Consecutivo");
            getprintcorteingles.Parameter<string>("CodigoOperario");
            getprintcorteingles.Parameter<string>("Cliente");
            getprintcorteingles.Parameter<string>("Producto");
            getprintcorteingles.Parameter<string>("CodigoBarrasEan132");
            getprintcorteingles.Parameter<string>("CodigoBarrasEan132Texto");
            getprintcorteingles.Parameter<double>("PesoNeto");
            getprintcorteingles.Parameter<string>("OrdenProduccion");
            getprintcorteingles.Parameter<double>("PesoBruto");
            getprintcorteingles.Parameter<string>("Fecha");
            getprintcorteingles.Parameter<string>("CodigoPuesto");
            getprintcorteingles.Parameter<string>("CodigoQrRuta");
            getprintcorteingles.Parameter<string>("TipoProduccion");
            getprintcorteingles.Parameter<string>("CodigoQr");
            getprintcorteingles.Parameter<string>("CodigoProducto");
            getprintcorteingles.Parameter<string>("FechaVence");
            getprintcorteingles.Parameter<string>("NitCliente");
            getprintcorteingles.Parameter<string>("Brand");
            getprintcorteingles.Parameter<string>("Ponum");
            getprintcorteingles.Parameter<string>("PrintDate");
            getprintcorteingles.Parameter<string>("SlitPackDate");
        }

        static void GetPonum(ODataConventionModelBuilder builder)
        {
            var getponum = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPonum");
            getponum.Parameter<string>("OrdenProduccion");
            getponum.Parameter<string>("CodigoProducto");
        }

        static void GetFechaBatch(ODataConventionModelBuilder builder)
        {
            var getfechabatch = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetFechaBatch");
            getfechabatch.Parameter<Int32>("Bacth");
        }

        static void GetPrintSellado(ODataConventionModelBuilder builder)
        {
            var getprintsellado = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintSellado");
            getprintsellado.Parameter<string>("Turno");
            getprintsellado.Parameter<string>("Maquina");
            getprintsellado.Parameter<string>("CodigoCliente");
            getprintsellado.Parameter<string>("Consecutivo");
            getprintsellado.Parameter<string>("CodigoOperario");
            getprintsellado.Parameter<string>("Cliente");
            getprintsellado.Parameter<string>("CodigoBarrasFijo");
            getprintsellado.Parameter<string>("CodigoBarrasEan14");
            getprintsellado.Parameter<string>("Producto");
            getprintsellado.Parameter<string>("CodigoBarrasEan132");
            getprintsellado.Parameter<string>("CodigoBarrasEan132Texto");
            getprintsellado.Parameter<double>("PesoNeto");
            getprintsellado.Parameter<string>("OrdenProduccion");
            getprintsellado.Parameter<double>("PesoBruto");
            getprintsellado.Parameter<string>("Fecha");
            getprintsellado.Parameter<string>("CodigoPuesto");
            getprintsellado.Parameter<string>("CodigoQrRuta");
            getprintsellado.Parameter<string>("TipoProduccion");
            getprintsellado.Parameter<string>("CodigoQr");
            getprintsellado.Parameter<string>("CodigoProducto");
            getprintsellado.Parameter<string>("CodigoBarrasFijoTexto");
            getprintsellado.Parameter<string>("FechaVence");
            getprintsellado.Parameter<string>("NitCliente");
            getprintsellado.Parameter<string>("Cantidad");
        }

        static void GetPrintSelladoIngles(ODataConventionModelBuilder builder)
        {
            var getprintselladoingles = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetPrintSelladoIngles");
            getprintselladoingles.Parameter<string>("Turno");
            getprintselladoingles.Parameter<string>("Maquina");
            getprintselladoingles.Parameter<string>("CodigoCliente");
            getprintselladoingles.Parameter<string>("Consecutivo");
            getprintselladoingles.Parameter<string>("CodigoOperario");
            getprintselladoingles.Parameter<string>("Cliente");
            getprintselladoingles.Parameter<string>("Producto");
            getprintselladoingles.Parameter<string>("CodigoBarrasEan132");
            getprintselladoingles.Parameter<string>("CodigoBarrasEan132Texto");
            getprintselladoingles.Parameter<double>("PesoNeto");
            getprintselladoingles.Parameter<string>("OrdenProduccion");
            getprintselladoingles.Parameter<double>("PesoBruto");
            getprintselladoingles.Parameter<string>("Fecha");
            getprintselladoingles.Parameter<string>("CodigoPuesto");
            getprintselladoingles.Parameter<string>("CodigoQrRuta");
            getprintselladoingles.Parameter<string>("TipoProduccion");
            getprintselladoingles.Parameter<string>("CodigoQr");
            getprintselladoingles.Parameter<string>("CodigoProducto");
            getprintselladoingles.Parameter<string>("FechaVence");
            getprintselladoingles.Parameter<string>("NitCliente");
            getprintselladoingles.Parameter<string>("Brand");
            getprintselladoingles.Parameter<string>("Ponum");
            getprintselladoingles.Parameter<string>("PrintDate");
            getprintselladoingles.Parameter<string>("SlitPackDate");
            getprintselladoingles.Parameter<string>("Cantidad");
        }

        static void GetUltimaTaraPorOrdenProduccion(ODataConventionModelBuilder builder)
        {
            var getultimataraporprdenproduccion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetUltimaTaraPorOrdenProduccion");
            getultimataraporprdenproduccion.Parameter<string>("OrdenDeProduccion");
            getultimataraporprdenproduccion.Parameter<string>("CodigoPuesto");
        }

        static void GetBrand(ODataConventionModelBuilder builder)
        {
            var getbrand = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetBrand");
            getbrand.Parameter<string>("OrdenProduccion");
            getbrand.Parameter<string>("CodigoProducto");
        }

        static void GetConsultarLimitesParaSellado(ODataConventionModelBuilder builder)
        {
            var getconsultarlimitesparasellado = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetConsultarLimitesParaSellado");
            getconsultarlimitesparasellado.Parameter<string>("CodigoPuesto");
            getconsultarlimitesparasellado.Parameter<string>("CodigoProceso");
            getconsultarlimitesparasellado.Parameter<string>("CodigoProducto");
            getconsultarlimitesparasellado.Parameter<double>("PesoNeto");
        }


        static void GetTuplasPorOPDesperdicio(ODataConventionModelBuilder builder)
        {
            var gettuplasporopdesperdicio = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetTuplasPorOPDesperdicio");
            gettuplasporopdesperdicio.Parameter<string>("OrdenProduccion");
            gettuplasporopdesperdicio.ReturnsCollectionFromEntitySet<DetalleDesperdicio>("GetTuplasPorOPDesperdicio");
        }

        static void GetNameEmpleadoDesperdicio(ODataConventionModelBuilder builder)
        {
            var getnameempleadodesperdicio = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetNameEmpleadoDesperdicio");
            getnameempleadodesperdicio.Parameter<string>("CodigoOperario");
        }

        static void PrintDesperdicio(ODataConventionModelBuilder builder)
        {
            var printDesperdicio = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintDesperdicio");

            printDesperdicio.Parameter<string>("OrdenDeProduccion");
            printDesperdicio.Parameter<string>("CodigoProducto");
            printDesperdicio.Parameter<string>("Producto");
            printDesperdicio.Parameter<string>("CodigoMaquina");
            printDesperdicio.Parameter<string>("Consecutivo");
            printDesperdicio.Parameter<string>("Ancho");
            printDesperdicio.Parameter<string>("Largo");
            printDesperdicio.Parameter<string>("Tramos");
            printDesperdicio.Parameter<string>("Metros");
            printDesperdicio.Parameter<string>("PesoNeto");
            printDesperdicio.Parameter<string>("PesoTotal");
            printDesperdicio.Parameter<string>("CodigoQrRuta");
            printDesperdicio.Parameter<string>("Impresora");
            printDesperdicio.Parameter<string>("NumeroCopias");
            printDesperdicio.Parameter<string>("Observaciones");
            printDesperdicio.Parameter<string>("MotivoDesperdicio");
 
        }

        static void PrintReproceso(ODataConventionModelBuilder builder)
        {
            var printReproceso = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintReproceso");

            printReproceso.Parameter<string>("OrdenDeProduccion");
            printReproceso.Parameter<string>("CodigoProducto");
            printReproceso.Parameter<string>("Producto");
            printReproceso.Parameter<string>("CodigoMaquina");
            printReproceso.Parameter<string>("Consecutivo");
            printReproceso.Parameter<string>("Ancho");
            printReproceso.Parameter<string>("Largo");
            printReproceso.Parameter<string>("Tramos");
            printReproceso.Parameter<string>("Metros");
            printReproceso.Parameter<string>("PesoNeto");
            printReproceso.Parameter<string>("PesoTotal");
            printReproceso.Parameter<string>("CodigoQrRuta");
            printReproceso.Parameter<string>("Impresora");
            printReproceso.Parameter<string>("NumeroCopias");
            printReproceso.Parameter<string>("Observaciones");
            printReproceso.Parameter<string>("MotivoDesperdicio");

        }

        static void PrintCoExtrusoraGeneral(ODataConventionModelBuilder builder) 
        {
            var printcoextrusorageneral = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintCoExtrusoraGeneral");
            printcoextrusorageneral.Parameter<string>("OrdenDeProduccion");
            printcoextrusorageneral.Parameter<string>("Cliente");
            printcoextrusorageneral.Parameter<string>("CodigoProducto");
            printcoextrusorageneral.Parameter<string>("CodigoQrTexto");
            printcoextrusorageneral.Parameter<string>("Producto");
            printcoextrusorageneral.Parameter<string>("CodigoMaquina");
            printcoextrusorageneral.Parameter<string>("CodigoProceso");
            printcoextrusorageneral.Parameter<string>("Consecutivo");
            printcoextrusorageneral.Parameter<string>("Banderas");
            printcoextrusorageneral.Parameter<string>("TipoMaterial");
            printcoextrusorageneral.Parameter<string>("TratamientoCorona");
            printcoextrusorageneral.Parameter<string>("Fecha");
            printcoextrusorageneral.Parameter<string>("Formula");
            printcoextrusorageneral.Parameter<string>("Calibre");
            printcoextrusorageneral.Parameter<string>("AnchoExtrusion");
            printcoextrusorageneral.Parameter<string>("Metros");
            printcoextrusorageneral.Parameter<string>("PesoNeto");
            printcoextrusorageneral.Parameter<string>("PesoBruto");
            printcoextrusorageneral.Parameter<string>("CodigoQrRuta");
            printcoextrusorageneral.Parameter<string>("Impresora");
            printcoextrusorageneral.Parameter<string>("NumeroCopias");
            printcoextrusorageneral.Parameter<string>("WarehouseCode");
            printcoextrusorageneral.Parameter<string>("Nota");
            printcoextrusorageneral.Parameter<string>("FechaVencimiento");
            printcoextrusorageneral.Parameter<string>("MesesDespues");
        }

        static void PrintFlexoGeneral(ODataConventionModelBuilder builder)
        {
            var printflexogeneral = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintFlexoGeneral");
            printflexogeneral.Parameter<string>("OrdenDeProduccion");
            printflexogeneral.Parameter<string>("Cliente");
            printflexogeneral.Parameter<string>("CodigoProducto");
            printflexogeneral.Parameter<string>("CodigoQrTexto");
            printflexogeneral.Parameter<string>("Producto");
            printflexogeneral.Parameter<string>("CodigoMaquina");
            printflexogeneral.Parameter<string>("CodigoProceso");
            printflexogeneral.Parameter<string>("Consecutivo");
            printflexogeneral.Parameter<string>("Banderas");
            printflexogeneral.Parameter<string>("TipoMaterial");
            printflexogeneral.Parameter<string>("Curado");
            printflexogeneral.Parameter<string>("OpSiguiente");
            printflexogeneral.Parameter<string>("Fecha");
            printflexogeneral.Parameter<string>("Metros");
            printflexogeneral.Parameter<string>("PesoNeto");
            printflexogeneral.Parameter<string>("PesoBruto");
            printflexogeneral.Parameter<string>("CodigoQrRuta");
            printflexogeneral.Parameter<string>("Impresora");
            printflexogeneral.Parameter<string>("NumeroCopias");
            printflexogeneral.Parameter<string>("WarehouseCode");
            printflexogeneral.Parameter<string>("Nota");
            printflexogeneral.Parameter<string>("FechaVencimiento");
            printflexogeneral.Parameter<string>("MesesDespues");
        }

        static void PrintProduccionGenerico(ODataConventionModelBuilder builder)
        {
            var printProduccionGenerico = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintProduccionGenerico");
       
            printProduccionGenerico.Parameter<string>("OrdenDeProduccion");
            printProduccionGenerico.Parameter<string>("CodigoProducto");
            printProduccionGenerico.Parameter<string>("Producto");
            printProduccionGenerico.Parameter<string>("CodigoMaquina");
            printProduccionGenerico.Parameter<string>("Consecutivo");
            printProduccionGenerico.Parameter<string>("Ancho");
            printProduccionGenerico.Parameter<string>("Largo");
            printProduccionGenerico.Parameter<string>("Tramos");
            printProduccionGenerico.Parameter<string>("Metros");
            printProduccionGenerico.Parameter<string>("PesoNeto");
            printProduccionGenerico.Parameter<string>("PesoTotal");
            printProduccionGenerico.Parameter<string>("CodigoQrRuta");
            printProduccionGenerico.Parameter<string>("Impresora");
            printProduccionGenerico.Parameter<string>("NumeroCopias");
            printProduccionGenerico.Parameter<string>("Observaciones");
        }


        static void PrintCorteGeneral(ODataConventionModelBuilder builder)
        {
            var printcortegeneral = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintCorteGeneral");
            printcortegeneral.Parameter<string>("OrdenDeProduccion");
            printcortegeneral.Parameter<string>("Cliente");
            printcortegeneral.Parameter<string>("CodigoProducto");
            printcortegeneral.Parameter<string>("CodigoQrTexto");
            printcortegeneral.Parameter<string>("IdClienteYCliente");
            printcortegeneral.Parameter<string>("CodigoMaquina");
            printcortegeneral.Parameter<string>("CodigoProceso");
            printcortegeneral.Parameter<string>("Consecutivo");
            printcortegeneral.Parameter<string>("Bobinas");
            printcortegeneral.Parameter<string>("Repeticiones");
            printcortegeneral.Parameter<string>("NombreOperario");    
            printcortegeneral.Parameter<string>("Fecha");
            printcortegeneral.Parameter<string>("PesoNeto");
            printcortegeneral.Parameter<string>("PesoBruto");
            printcortegeneral.Parameter<string>("CodigoQrRuta");
            printcortegeneral.Parameter<string>("Impresora");
            printcortegeneral.Parameter<string>("NumeroCopias");
            printcortegeneral.Parameter<string>("WarehouseCode");
            printcortegeneral.Parameter<string>("FechaVencimiento");
            printcortegeneral.Parameter<string>("MesesDespues");
            printcortegeneral.Parameter<string>("CodigoOperario");
            printcortegeneral.Parameter<string>("Metros");
        }

        static void PrintCorteQuala(ODataConventionModelBuilder builder)
        {
            var printcortequala = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintCorteQuala");
            printcortequala.Parameter<string>("OrdenDeProduccion");
            printcortequala.Parameter<string>("Cliente");
            printcortequala.Parameter<string>("CodigoProducto");
            printcortequala.Parameter<string>("CodigoQrTexto");
            printcortequala.Parameter<string>("IdClienteYCliente");
            printcortequala.Parameter<string>("CodigoMaquina");
            printcortequala.Parameter<string>("CodigoProceso");
            printcortequala.Parameter<string>("Consecutivo");
            printcortequala.Parameter<string>("Bobinas");
            printcortequala.Parameter<string>("Repeticiones");
            printcortequala.Parameter<string>("NombreOperario");
            printcortequala.Parameter<string>("Fecha");
            printcortequala.Parameter<string>("PesoNeto");
            printcortequala.Parameter<string>("PesoBruto");
            printcortequala.Parameter<string>("CodigoQrRuta");
            printcortequala.Parameter<string>("Impresora");
            printcortequala.Parameter<string>("NumeroCopias");
            printcortequala.Parameter<string>("WarehouseCode");
            printcortequala.Parameter<string>("FechaVencimiento") ;
            printcortequala.Parameter<string>("MesesDespues");
            printcortequala.Parameter<string>("CodigoOperario");
            printcortequala.Parameter<string>("CodigoQrRutaCliente");
            printcortequala.Parameter<string>("Codigo128");
            printcortequala.Parameter<string>("Codigo128Texto");
            printcortequala.Parameter<string>("Metros");
        }

        static void PrintCorteNutresa(ODataConventionModelBuilder builder)
        {
            var printcortenutresa = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintCorteNutresa");
            printcortenutresa.Parameter<string>("OrdenDeProduccion");
            printcortenutresa.Parameter<string>("Cliente");
            printcortenutresa.Parameter<string>("CodigoProducto");
            printcortenutresa.Parameter<string>("CodigoMaquina");
            printcortenutresa.Parameter<string>("CodigoProceso");
            printcortenutresa.Parameter<string>("Consecutivo");
            printcortenutresa.Parameter<string>("Bobinas");
            printcortenutresa.Parameter<string>("Repeticiones");
            printcortenutresa.Parameter<string>("NombreOperario");
            printcortenutresa.Parameter<string>("Fecha");
            printcortenutresa.Parameter<string>("PesoNeto");
            printcortenutresa.Parameter<string>("PesoBruto");
            printcortenutresa.Parameter<string>("CodigoQrRuta");
            printcortenutresa.Parameter<string>("Impresora");
            printcortenutresa.Parameter<string>("NumeroCopias");
            printcortenutresa.Parameter<string>("WarehouseCode");
            printcortenutresa.Parameter<string>("FechaVencimiento");
            printcortenutresa.Parameter<string>("CodigoEAN14");
            printcortenutresa.Parameter<string>("Producto");
            printcortenutresa.Parameter<string>("Codigo128");
            printcortenutresa.Parameter<string>("Codigo128Texto");
            printcortenutresa.Parameter<string>("MesesDespues");
            printcortenutresa.Parameter<string>("CodigoOperario");
            printcortenutresa.Parameter<string>("Metros");
            printcortenutresa.Parameter<string>("SKU");
        }

        static void PrintCorteIngles(ODataModelBuilder builder)
        {
            var printcorteingles = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintCorteIngles");
            printcorteingles.Parameter<string>("OrdenDeProduccion");
            printcorteingles.Parameter<string>("Cliente");
            printcorteingles.Parameter<string>("CodigoProducto");
            printcorteingles.Parameter<string>("CodigoQrTexto");
            printcorteingles.Parameter<string>("IdClienteYCliente");
            printcorteingles.Parameter<string>("CodigoMaquina");
            printcorteingles.Parameter<string>("CodigoProceso");
            printcorteingles.Parameter<string>("Consecutivo");
            printcorteingles.Parameter<string>("Bobinas");
            printcorteingles.Parameter<string>("Repeticiones");
            printcorteingles.Parameter<string>("NombreOperario");
            printcorteingles.Parameter<string>("Fecha");
            printcorteingles.Parameter<string>("PesoNeto");
            printcorteingles.Parameter<string>("PesoBruto");
            printcorteingles.Parameter<string>("CodigoQrRuta");
            printcorteingles.Parameter<string>("Impresora");
            printcorteingles.Parameter<string>("NumeroCopias");
            printcorteingles.Parameter<string>("WarehouseCode");
            printcorteingles.Parameter<string>("FechaVencimiento");
            printcorteingles.Parameter<string>("MesesDespues");
            printcorteingles.Parameter<string>("CodigoOperario");
            printcorteingles.Parameter<string>("Metros");
            printcorteingles.Parameter<string>("Brand");
            printcorteingles.Parameter<string>("Ponum");
            printcorteingles.Parameter<string>("FechaBatch");
            printcorteingles.Parameter<string>("CodigoEntrada");
            printcorteingles.Parameter<string>("CamposOcultos");
        }

        static void PrintSelladoGeneral(ODataModelBuilder builder)
        {
            var printselladogeneral = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintSelladoGeneral");
            printselladogeneral.Parameter<string>("OrdenDeProduccion");
            printselladogeneral.Parameter<string>("Cliente");
            printselladogeneral.Parameter<string>("CodigoProducto");
            printselladogeneral.Parameter<string>("CodigoQrTexto");
            printselladogeneral.Parameter<string>("IdClienteYCliente");
            printselladogeneral.Parameter<string>("CodigoMaquina");
            printselladogeneral.Parameter<string>("CodigoProceso");
            printselladogeneral.Parameter<string>("Consecutivo");
            printselladogeneral.Parameter<string>("NombreOperario");
            printselladogeneral.Parameter<string>("Fecha");
            printselladogeneral.Parameter<string>("PesoNeto");
            printselladogeneral.Parameter<string>("PesoBruto");
            printselladogeneral.Parameter<string>("CodigoQrRuta");
            printselladogeneral.Parameter<string>("Impresora");
            printselladogeneral.Parameter<string>("NumeroCopias");
            printselladogeneral.Parameter<string>("WarehouseCode");
            printselladogeneral.Parameter<string>("FechaVencimiento");
            printselladogeneral.Parameter<string>("MesesDespues");
            printselladogeneral.Parameter<string>("CodigoOperario");
            printselladogeneral.Parameter<string>("Metros");

        }

        static void PrintSelladoQuala(ODataModelBuilder builder)
        {
            var printselladoquala= builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintSelladoQuala");
            printselladoquala.Parameter<string>("OrdenDeProduccion");
            printselladoquala.Parameter<string>("Cliente");
            printselladoquala.Parameter<string>("CodigoProducto");
            printselladoquala.Parameter<string>("CodigoQrTexto");
            printselladoquala.Parameter<string>("IdClienteYCliente");
            printselladoquala.Parameter<string>("CodigoMaquina");
            printselladoquala.Parameter<string>("CodigoProceso");
            printselladoquala.Parameter<string>("Consecutivo");
            printselladoquala.Parameter<string>("NombreOperario");
            printselladoquala.Parameter<string>("Fecha");
            printselladoquala.Parameter<string>("PesoNeto");
            printselladoquala.Parameter<string>("PesoBruto");
            printselladoquala.Parameter<string>("CodigoQrRuta");
            printselladoquala.Parameter<string>("Impresora");
            printselladoquala.Parameter<string>("NumeroCopias");
            printselladoquala.Parameter<string>("WarehouseCode");
            printselladoquala.Parameter<string>("FechaVencimiento");
            printselladoquala.Parameter<string>("MesesDespues");
            printselladoquala.Parameter<string>("CodigoOperario");
            printselladoquala.Parameter<string>("Metros");
            printselladoquala.Parameter<string>("CodigoQrRutaCliente");
            printselladoquala.Parameter<string>("Codigo128");
            printselladoquala.Parameter<string>("Codigo128Texto");
        }

        static void PrintSelladoNutresa(ODataConventionModelBuilder builder)
        {
            var printselladonutresa = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintSelladoNutresa");
            printselladonutresa.Parameter<string>("OrdenDeProduccion");
            printselladonutresa.Parameter<string>("Cliente");
            printselladonutresa.Parameter<string>("CodigoProducto");
            printselladonutresa.Parameter<string>("CodigoMaquina");
            printselladonutresa.Parameter<string>("CodigoProceso");
            printselladonutresa.Parameter<string>("Consecutivo");
            printselladonutresa.Parameter<string>("NombreOperario");
            printselladonutresa.Parameter<string>("Fecha");
            printselladonutresa.Parameter<string>("PesoNeto");
            printselladonutresa.Parameter<string>("PesoBruto");
            printselladonutresa.Parameter<string>("CodigoQrRuta");
            printselladonutresa.Parameter<string>("Impresora");
            printselladonutresa.Parameter<string>("NumeroCopias");
            printselladonutresa.Parameter<string>("WarehouseCode");
            printselladonutresa.Parameter<string>("FechaVencimiento");
            printselladonutresa.Parameter<string>("CodigoEAN14");
            printselladonutresa.Parameter<string>("Producto");
            printselladonutresa.Parameter<string>("Codigo128");
            printselladonutresa.Parameter<string>("Codigo128Texto");
            printselladonutresa.Parameter<string>("MesesDespues");
            printselladonutresa.Parameter<string>("CodigoOperario");
            printselladonutresa.Parameter<string>("Metros");
            printselladonutresa.Parameter<string>("SKU");
        }

        static void PrintSelladoIngles(ODataModelBuilder builder)
        {
            var printselladoinlges = builder.Entity<TUPLAEJECUCION>().Collection.Action("PrintSelladoIngles");
            printselladoinlges.Parameter<string>("OrdenDeProduccion");
            printselladoinlges.Parameter<string>("Cliente");
            printselladoinlges.Parameter<string>("CodigoProducto");
            printselladoinlges.Parameter<string>("CodigoQrTexto");
            printselladoinlges.Parameter<string>("IdClienteYCliente");
            printselladoinlges.Parameter<string>("CodigoMaquina");
            printselladoinlges.Parameter<string>("CodigoProceso");
            printselladoinlges.Parameter<string>("Consecutivo");
            printselladoinlges.Parameter<string>("NombreOperario");
            printselladoinlges.Parameter<string>("Fecha");
            printselladoinlges.Parameter<string>("PesoNeto");
            printselladoinlges.Parameter<string>("PesoBruto");
            printselladoinlges.Parameter<string>("CodigoQrRuta");
            printselladoinlges.Parameter<string>("Impresora");
            printselladoinlges.Parameter<string>("NumeroCopias");
            printselladoinlges.Parameter<string>("WarehouseCode");
            printselladoinlges.Parameter<string>("FechaVencimiento");
            printselladoinlges.Parameter<string>("MesesDespues");
            printselladoinlges.Parameter<string>("CodigoOperario");
            printselladoinlges.Parameter<string>("Metros");
            printselladoinlges.Parameter<string>("Brand");
            printselladoinlges.Parameter<string>("Ponum");
            printselladoinlges.Parameter<string>("FechaBatch");
            printselladoinlges.Parameter<string>("CodigoEntrada");
            printselladoinlges.Parameter<string>("CamposOcultos");
        }

        static void GetEsquemaDeImpresion(ODataConventionModelBuilder builder)
        {
            var getesquemadeimpresion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetEsquemaDeImpresion");
            getesquemadeimpresion.Parameter<string>("CodigoPuesto");
        }

        static void GetEsquemaDeImpresionPorProceso(ODataConventionModelBuilder builder)
        {
            var getesquemadeimpresionporproceso = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetEsquemaDeImpresionPorProceso");
            getesquemadeimpresionporproceso.Parameter<string>("Proceso");
        }

        static void GetInformacionExtraImpresion(ODataModelBuilder builder)
        {
            var getinformacionextraimpresion = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetInformacionExtraImpresion");
            getinformacionextraimpresion.Parameter<string>("TipoImpresion");
            getinformacionextraimpresion.Parameter<string>("OrdenDeProduccion");
            getinformacionextraimpresion.Parameter<string>("CodigoProducto");
            getinformacionextraimpresion.Parameter<string>("CodigoPuesto");
            getinformacionextraimpresion.Parameter<string>("CodigoProceso");
        }

        static void GetEditarEstandares(ODataConventionModelBuilder builder)
        {
            var geteditarestandares = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetEditarEstandares");
            geteditarestandares.Parameter<string>("Proceso");
            geteditarestandares.Parameter<string>("ValueRDLC");
        }

        static void GetListaCelulas(ODataConventionModelBuilder builder)
        {
            var getlistacelulas = builder.Entity<TUPLAEJECUCION>().Collection.Action("GetListaCelulas");
            getlistacelulas.ReturnsFromEntitySet<CELULAS>("GetListaCelulas");
        }

        static void EditarCarrata(ODataConventionModelBuilder builder)
        {
            var editarcarreta = builder.Entity<TUPLAEJECUCION>().Collection.Action("EditarCarrata");
            editarcarreta.Parameter<int>("Id");
            editarcarreta.Parameter<string>("Nombre");
            editarcarreta.Parameter<double>("Valor");
            editarcarreta.Parameter<bool>("Estado");
            editarcarreta.Parameter<string>("Descripcion");
            editarcarreta.Parameter<string>("CodigoCelula");
        }

        static void EliminarCarreta(ODataConventionModelBuilder builder)
        {
            var eliminarcarreta = builder.Entity<TUPLAEJECUCION>().Collection.Action("EliminarCarreta");
            eliminarcarreta.Parameter<int>("Id");
        }

        static void ConsultarEstibaAbierta(ODataConventionModelBuilder builder)
        {
            var consultarestibaabierta = builder.Entity<TUPLAEJECUCION>().Collection.Action("ConsultarEstibaAbierta");
            consultarestibaabierta.Parameter<string>("ordendeproduccion");
            consultarestibaabierta.Parameter<string>("codigoproducto");
            consultarestibaabierta.Parameter<string>("codigoproceso");
            consultarestibaabierta.Parameter<string>("codigomaquina");
        }

        static void InsertarDetalleEstiba(ODataConventionModelBuilder builder)
        {
            var insertardetalleestiba = builder.Entity<TUPLAEJECUCION>().Collection.Action("InsertarDetalleEstiba");
            insertardetalleestiba.Parameter<int>("consecutivo");
            insertardetalleestiba.Parameter<int>("idbascula");
            insertardetalleestiba.Parameter<int>("idestibaencabezado");
            insertardetalleestiba.Parameter<double>("pesoneto");
            insertardetalleestiba.Parameter<double>("tara");
            insertardetalleestiba.Parameter<double>("cantidad");
        }

        static void ConsultarEstibaAtope(ODataConventionModelBuilder builder)
        {
            var consultarestibaatope = builder.Entity<TUPLAEJECUCION>().Collection.Action("ConsultarEstibaAtope");
            consultarestibaatope.Parameter<int>("idestibaencabezado");
            consultarestibaatope.Parameter<string>("codigoproducto");
        }

        static void EditarBobinaEstiba(ODataConventionModelBuilder builder)
        {
            var editarbobinaestiba = builder.Entity<TUPLAEJECUCION>().Collection.Action("EditarBobinaEstiba");
            editarbobinaestiba.Parameter<int>("ID");
        }

        static void ConsultarMaterialesPorJob(ODataConventionModelBuilder builder)
        {
            var obj = builder.Entity<TUPLAEJECUCION>().Collection.Action("ConsultarMaterialesPorJob");
            obj.Parameter<string>("CodigoOrdenProduccion");
            obj.Parameter<string>("CodigoProceso");
            obj.ReturnsFromEntitySet<Detalle>("ConsultarMaterialesPorJob");
        }

        static void AlmacenarRegistroDevolucion(ODataConventionModelBuilder builder)
        {
            var obj = builder.Entity<TUPLAEJECUCION>().Collection.Action("AlmacenarRegistroDevolucion");
            obj.Parameter<string>("OrdenDeProduccion");
            obj.Parameter<string>("CodigoProducto");
            obj.Parameter<string>("CodigoQrTexto");
            obj.Parameter<string>("CodigoMaquina");
            obj.Parameter<string>("Fecha");
            obj.Parameter<string>("NombreOperario");
            obj.Parameter<string>("CodigoQrRuta");
            obj.Parameter<string>("Assembly");
            obj.Parameter<string>("Producto");
            obj.Parameter<string>("Lote");
            obj.Parameter<string>("Cliente");
            obj.Parameter<string>("Secuencia");
            obj.Parameter<string>("CodigoProceso");
            obj.Parameter<string>("Nota");
            obj.Parameter<string>("BodegaTo");
            obj.Parameter<string>("BinTo");
            obj.Parameter<string>("PesoBruto");
            obj.Parameter<string>("PesoNeto");
            obj.Parameter<string>("Impresora");
            obj.Parameter<string>("Tara");
            obj.Parameter<string>("Cantidad");
        }
    }
}
