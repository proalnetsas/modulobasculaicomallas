﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ModuloBasculas.Models;
using Microsoft.Reporting.WebForms;
using System.Web;
using ModuloBasculas.Controllers.Reportes;
using System.Web.Configuration;
using System.Drawing;
//using Telerik.Reporting.Processing;
using System.Drawing.Printing;
using Gma.QrCodeNet.Encoding.Windows.Forms;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.IO;
using System.Drawing.Imaging;
using System.Globalization;
using Net.ConnectCode.Barcode;
using System.Data.Linq.SqlClient;

namespace ModuloBasculas.Controllers
{
    public class TUPLAEJECUCIONsController : ODataController
    {
        private Entities db = new Entities();
        

        // GET: odata/TUPLAEJECUCIONs
        [EnableQuery]
        public IQueryable<TUPLAEJECUCION> GetTUPLAEJECUCIONs()
        {
            return db.TUPLAEJECUCION;
        }

        [HttpPost]
        public List<ProductosDTO> GetProductosOP(ODataActionParameters parameters)
        {
            string OrdenProduccion = (string)parameters["OrdenProduccion"];
           
            List<ProductosDTO> productos = (from opp in db.ORDENPRODUCCIONXPRODUCTO
                           join p in db.PRODUCTOS on opp.CODIGOPRODUCTOS equals p.CODIGOPRODUCTOS
                           where opp.CODIGOORDENPRODUCCION == OrdenProduccion
                           select new ProductosDTO
                           {
                               CodigoProducto = opp.CODIGOPRODUCTOS,
                               NombreProducto = p.NOMBREPRODUCTO
                           }).ToList();

            return productos;
        }

        [HttpPost]
        public List<ProductosDTO> GetAnchoLargo(ODataActionParameters parameters)
        {
            string CodigoProducto = (string)parameters["PRODUCTO"];

            List<ProductosDTO> productos = new List<ProductosDTO>();

            try
            {
                productos = (from p in db.PRODUCTOS
                             where p.CODIGOPRODUCTOS == CodigoProducto
                             select new ProductosDTO
                             {
                                 AnchoProducto = p.ANCHO,
                                 LargoProducto = p.LARGO,
                                 PesoProducto = p.PESOPRODUCTO

                             }).ToList();
            }
            catch (Exception ex)
            {

            }
            

            return productos;


        }

        [HttpPost]
        public double GetCantidadAPesarOrdenProduccion(ODataActionParameters parameters)
        {
            string CodigoOrdenProduccion = parameters["CODIGOORDENPRODUCCION"].ToString();
            try
            {
                var a = Convert.ToDouble((from opp in db.ORDENPRODUCCIONXPRODUCTO
                                         join p in db.PRODUCTOS on opp.CODIGOPRODUCTOS equals p.CODIGOPRODUCTOS
                                        where opp.CODIGOORDENPRODUCCION == CodigoOrdenProduccion
                                        select (opp.CANTIDAD * p.PESOPRODUCTO)).FirstOrDefault());

                return a;

            }
            catch (Exception ex)
            {
                return 0;
            }
            
        }

        [HttpPost]
        public double GetCantidadPesadaOrdenProduccion(ODataActionParameters parameters)
        {
            string CodigoOrdenProduccion = parameters["CODIGOORDENPRODUCCION"].ToString();
            try
            {
                return Convert.ToDouble((from b in db.BASCULA
                        where b.CODIGOORDENPRODUCCION == CodigoOrdenProduccion && b.PRODUCCION == "PRODUCCION"
                        select b.PESO).Sum());

            }
            catch
            {
                return 0;
            }

        }

        [HttpPost]
        public List<PUESTOSDETRABAJO> GetMaquinasOP(ODataActionParameters parameters)
        {
            string OrdenProduccion = (string)parameters["OrdenProduccion"];
            DateTime Fecha = (DateTime)parameters["Fecha"];
            //Fecha = Fecha.Add(DateTime.Now.TimeOfDay);
            //DateTime FechaFinal = Fecha.AddDays(1);
            DateTime FechaInicial = Fecha.AddHours(-12);
            var query = (from te in db.TUPLAEJECUCION
                         join eej in db.ENTRADAEJECUCION on te.CODIGOTUPLA equals eej.CODIGOTUPLA
                         //where eej.FECHAENTRADA >= Fecha && eej.FECHAENTRADA < FechaFinal 
                         where eej.FECHAENTRADA >= FechaInicial && eej.FECHAENTRADA < Fecha
                         && te.CODIGOORDENPRODUCCION == OrdenProduccion
                         orderby eej.FECHAENTRADA descending
                         select new
                         {
                             CODIGOPUESTO = te.PUESTOTRABAJOSEGUNPRODUCTO.PUESTOSDETRABAJO.CODIGOPUESTO,
                             NOMBREPUESTO = te.PUESTOTRABAJOSEGUNPRODUCTO.PUESTOSDETRABAJO.NOMBREPUESTO
                         }).ToList().Distinct().ToList();

            List<PUESTOSDETRABAJO> Lstpuestos = query.Select(np => new PUESTOSDETRABAJO
            {
                CODIGOPUESTO = np.CODIGOPUESTO,
                NOMBREPUESTO = np.NOMBREPUESTO
            }).ToList();
            return Lstpuestos;
        }

        [HttpPost]
        public Detalle GetDetalleOP(ODataActionParameters parameters)
        {
            //obtener la hora actual del batch y la hora actual del server
            TimeSpan horaactual = DateTime.Now.TimeOfDay;
            TimeSpan horaturno1 = new TimeSpan(6, 0, 0);
            TimeSpan horaturno2 = new TimeSpan(14, 0, 0);
            TimeSpan horaturno3 = new TimeSpan(22, 0, 0);

            int tiempodiferenciabatch = Convert.ToInt32(WebConfigurationManager.AppSettings["TiempoDiferenciaEntreBatch"].ToString());
            string listaperonal = WebConfigurationManager.AppSettings["CodigoOperarioInhabilitadas"].ToString();

            string OrdenProduccion = (string)parameters["OrdenProduccion"];
            string CodigoPuesto = (string)parameters["CodigoPuesto"];
            DateTime Fecha = (DateTime)parameters["Fecha"];
            DateTime FechaInicial = Fecha.AddHours(-12);

            Detalle det = (from te in db.TUPLAEJECUCION
                           join eej in db.ENTRADAEJECUCION on te.CODIGOTUPLA equals eej.CODIGOTUPLA
                           where eej.FECHAENTRADA >= FechaInicial && eej.FECHAENTRADA < Fecha 
                           && te.CODIGOORDENPRODUCCION == OrdenProduccion && te.CODIGOPUESTO == CodigoPuesto
                           orderby eej.FECHAENTRADA descending
                           select new Detalle
                           {
                               Operario = eej.PERSONALQUEEJECUTO.FirstOrDefault() != null ? eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.APELLIDOS + " " + eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.NOMBRES : "",
                               CodigoTurno = eej.CODIGOTURNO,
                               Turno = eej.TIPOSDETURNOS.TURNO,
                               Referencia = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.NOMBREPRODUCTO,
                               CodigoEntradaEjecucion = eej.CODIGOENTRADAEJECUCION,
                               CodigoProducto = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.CODIGOPRODUCTOS,
                               CodigoPuesto = te.CODIGOPUESTO,
                               CodigoOperario = eej.PERSONALQUEEJECUTO.FirstOrDefault() != null ? eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.CODIGOPERSONAL : "",
                               NombreMaquina = (from p in db.PUESTOSDETRABAJO where p.CODIGOPUESTO == te.CODIGOPUESTO select p).FirstOrDefault().NOMBREPUESTO.ToString(),
                               CodigoProceso = te.CODIGOPROCESO,
                               FechaEntradaEjecucion = eej.FECHAENTRADA,
                               BobinaEnturno = true,
                               RequiereCambioOperario = false,
                               OrdenDeProduccion = te.CODIGOORDENPRODUCCION
                           }).FirstOrDefault();

            //if (det != null)
            //{

            //    Validación para identificar si el BATCH anterior es igual en OP y cumple con el tiempo actual.
            //    DateTime fechadebatch = Convert.ToDateTime(det.FechaEntradaEjecucion);
            //    if ((DateTime.Now - fechadebatch).TotalMinutes <= tiempodiferenciabatch)
            //    {
            //        Detalle det1 = (from te in db.TUPLAEJECUCION
            //                        join eej in db.ENTRADAEJECUCION on te.CODIGOTUPLA equals eej.CODIGOTUPLA
            //                        where eej.FECHAENTRADA < det.FechaEntradaEjecucion && te.CODIGOPUESTO == CodigoPuesto
            //                        orderby eej.FECHAENTRADA descending
            //                        select new Detalle
            //                        {
            //                            Operario = eej.PERSONALQUEEJECUTO.FirstOrDefault() != null ? eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.APELLIDOS + " " + eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.NOMBRES : "",
            //                            CodigoTurno = eej.CODIGOTURNO,
            //                            Turno = eej.TIPOSDETURNOS.TURNO,
            //                            Referencia = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.NOMBREPRODUCTO,
            //                            CodigoEntradaEjecucion = eej.CODIGOENTRADAEJECUCION,
            //                            CodigoProducto = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.CODIGOPRODUCTOS,
            //                            CodigoPuesto = te.CODIGOPUESTO,
            //                            CodigoOperario = eej.PERSONALQUEEJECUTO.FirstOrDefault() != null ? eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.CODIGOPERSONAL : "",
            //                            NombreMaquina = (from p in db.PUESTOSDETRABAJO where p.CODIGOPUESTO == te.CODIGOPUESTO select p).FirstOrDefault().NOMBREPUESTO.ToString(),
            //                            CodigoProceso = te.CODIGOPROCESO,
            //                            FechaEntradaEjecucion = eej.FECHAENTRADA,
            //                            BobinaEnturno = true,
            //                            RequiereCambioOperario = false,
            //                            OrdenDeProduccion = te.CODIGOORDENPRODUCCION
            //                        }).FirstOrDefault();

            //        Se remplaza para el BARCT ANTERIOR POR ARRASTRE DE JOB Y CUMPLEEL TIEMPO 
            //        if (det.OrdenDeProduccion == det1.OrdenDeProduccion)
            //            det = det1;
            //    }

                //Validar si la bobina a registrar se encuentra en el turno
                DateTime fecha = Convert.ToDateTime(det.FechaEntradaEjecucion);
                if (DateTime.Now.Subtract(fecha).Hours <= 8)
                {
                    if (horaactual >= horaturno1 && horaactual <= horaturno2 && det.CodigoTurno == "1")
                        det.BobinaEnturno = true;
                    else if (horaactual >= horaturno2 && horaactual <= horaturno3 && det.CodigoTurno == "2")
                        det.BobinaEnturno = true;
                    else if (((horaactual >= horaturno3 && horaactual <= new TimeSpan(24, 00, 00)) || (horaactual >= new TimeSpan(00, 00, 00) && horaactual <= horaturno1)) && det.CodigoTurno == "3")
                        det.BobinaEnturno = true;
                    else
                        det.BobinaEnturno = false;
                }
                else
                    det.BobinaEnturno = false;

                //Validar si el operario es permitido si está en el turno actual
                if (det.BobinaEnturno == true)
                {
                    string personal = (from pe in db.PERSONALQUEEJECUTO
                                       where pe.CODIGOENTRADAEJECUCION == det.CodigoEntradaEjecucion
                                       select pe.CODIGOPERSONAL).FirstOrDefault().ToString();

                    List<string> lst = listaperonal.Split(',').ToList();

                    if (lst.Contains(personal))
                        det.RequiereCambioOperario = true;
                }
            
            return det;
        }

        [HttpPost]
        public Int32 GetConsecutivoBobina(ODataActionParameters parameters)
        {
            string OrdenProduccion = (string)parameters["OrdenProduccion"];
            string CodigoPuesto = (string)parameters["CodigoPuesto"];
            string TipoProduccion = (string)parameters["TipoProduccion"];
            return (from b in db.BASCULA where b.CODIGOPUESTO == CodigoPuesto && b.CODIGOORDENPRODUCCION == OrdenProduccion && b.PRODUCCION == TipoProduccion select b).Count();            
        }

        // GET: odata/TUPLAEJECUCIONs(5)
        [EnableQuery]
        public SingleResult<TUPLAEJECUCION> GetTUPLAEJECUCION([FromODataUri] int key)
        {
            return SingleResult.Create(db.TUPLAEJECUCION.Where(tUPLAEJECUCION => tUPLAEJECUCION.CODIGOTUPLA == key));
        }

        // PUT: odata/TUPLAEJECUCIONs(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<TUPLAEJECUCION> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TUPLAEJECUCION tUPLAEJECUCION = db.TUPLAEJECUCION.Find(key);
            if (tUPLAEJECUCION == null)
            {
                return NotFound();
            }

            patch.Put(tUPLAEJECUCION);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TUPLAEJECUCIONExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(tUPLAEJECUCION);
        }

        // POST: odata/TUPLAEJECUCIONs
        public IHttpActionResult Post(TUPLAEJECUCION tUPLAEJECUCION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TUPLAEJECUCION.Add(tUPLAEJECUCION);
            db.SaveChanges();

            return Created(tUPLAEJECUCION);
        }

        // PATCH: odata/TUPLAEJECUCIONs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<TUPLAEJECUCION> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TUPLAEJECUCION tUPLAEJECUCION = db.TUPLAEJECUCION.Find(key);
            if (tUPLAEJECUCION == null)
            {
                return NotFound();
            }

            patch.Patch(tUPLAEJECUCION);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TUPLAEJECUCIONExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(tUPLAEJECUCION);
        }

        // DELETE: odata/TUPLAEJECUCIONs(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            TUPLAEJECUCION tUPLAEJECUCION = db.TUPLAEJECUCION.Find(key);
            if (tUPLAEJECUCION == null)
            {
                return NotFound();
            }

            db.TUPLAEJECUCION.Remove(tUPLAEJECUCION);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/TUPLAEJECUCIONs(5)/ENTRADAEJECUCION
        [EnableQuery]
        public IQueryable<ENTRADAEJECUCION> GetENTRADAEJECUCION([FromODataUri] int key)
        {
            return db.TUPLAEJECUCION.Where(m => m.CODIGOTUPLA == key).SelectMany(m => m.ENTRADAEJECUCION);
        }

        // GET: odata/TUPLAEJECUCIONs(5)/ORDENPRODUCCIONXPRODUCTO
        [EnableQuery]
        public SingleResult<ORDENPRODUCCIONXPRODUCTO> GetORDENPRODUCCIONXPRODUCTO([FromODataUri] int key)
        {
            return SingleResult.Create(db.TUPLAEJECUCION.Where(m => m.CODIGOTUPLA == key).Select(m => m.ORDENPRODUCCIONXPRODUCTO));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TUPLAEJECUCIONExists(int key)
        {
            return db.TUPLAEJECUCION.Count(e => e.CODIGOTUPLA == key) > 0;
        }

        //Consultar OP para identificación de material extrusión
        [HttpPost]
        public Detalle GetDatosExtrusion(ODataActionParameters parameters)
        {
            string OrdenProduccion = (string)parameters["OrdenProduccion"];
            Detalle det = (from te in db.TUPLAEJECUCION
                           join eej in db.ENTRADAEJECUCION on te.CODIGOTUPLA equals eej.CODIGOTUPLA
                           where te.CODIGOORDENPRODUCCION == OrdenProduccion
                           orderby eej.FECHAENTRADA descending
                           select new Detalle
                           {
                               Operario = eej.PERSONALQUEEJECUTO.FirstOrDefault() != null ? eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.NOMBRES + " " + eej.PERSONALQUEEJECUTO.FirstOrDefault().PERSONAL.APELLIDOS : "",
                               CodigoProducto = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.CODIGOPRODUCTOS,
                               NombreProducto = te.ORDENPRODUCCIONXPRODUCTO.PRODUCTOS.NOMBREPRODUCTO,
                               CodigoPuesto = te.CODIGOPUESTO,
                               CodigoTurno = eej.CODIGOTURNO
                           }).FirstOrDefault();
            return det;
        }


        //Consultar el tipo de impresión
        [HttpPost]
        public string GetTipoImpresion()
        {
            try
            {
                string TipoDeImpresion = Convert.ToString(WebConfigurationManager.AppSettings["TipoImpresion"]);
                if (TipoDeImpresion == "")
                {
                    return "Extrusion";
                }
                else
                {
                    return TipoDeImpresion;
                }             
            }
            catch (Exception e)
            {
                return "Extrusion";
            }

        }

        [HttpPost]
        public string GetFechaProcesoImpresionCorte(string OrdenProduccion, string _CodigoProducto)
        {
            try
            {
                string CodigoProducto = _CodigoProducto;

                if (CodigoProducto.Contains("-1"))
                    CodigoProducto = CodigoProducto.Split('-')[0];

                DateTime? date = (from ej in db.ENTRADAEJECUCION
                                join te in db.TUPLAEJECUCION on ej.CODIGOTUPLA equals te.CODIGOTUPLA
                                where te.CODIGOORDENPRODUCCION == OrdenProduccion &&
                                te.CODIGOPRODUCTOS == CodigoProducto && te.CODIGOPROCESO == "PIMPRE"
                                select ej.FECHAENTRADA).FirstOrDefault();

                if (date != null)
                    return date.Value.ToString("yy/MM/dd", CultureInfo.InvariantCulture);
                else
                {
                    CodigoProducto = CodigoProducto + "-1";
                    DateTime? date2 = (from ej in db.ENTRADAEJECUCION
                                      join te in db.TUPLAEJECUCION on ej.CODIGOTUPLA equals te.CODIGOTUPLA
                                      where te.CODIGOORDENPRODUCCION == OrdenProduccion &&
                                      te.CODIGOPRODUCTOS == CodigoProducto && te.CODIGOPROCESO == "PIMPRE"
                                      select ej.FECHAENTRADA).FirstOrDefault();

                    if (date2 != null)
                        return date2.Value.ToString("yy/MM/dd", CultureInfo.InvariantCulture);

                    else
                        return "";
                }
            }

            catch
            {
                return "";
            }
        }

        [HttpPost]
        public string GetFechaBatch(ODataActionParameters parameters)
        {
            try
            {
                Int32 Bacth = (Int32)parameters["Bacth"];
                string query = "";

                DateTime? date = (from ej in db.ENTRADAEJECUCION
                                  where ej.CODIGOENTRADAEJECUCION == Bacth
                                  select ej.FECHAENTRADA).FirstOrDefault();
                if (date != null)
                    return date.Value.ToString("yy/MM/dd", CultureInfo.InvariantCulture);

                else
                    return "";
            }
            catch
            {
                return "";
            }
        }

        #region EDICION DE BOBINAS
        [HttpPost]
        public bool GetLoginEdicionBobinas(ODataActionParameters parameters)
        {
            try
            {
                string Username = WebConfigurationManager.AppSettings["Username"].ToString();
                string Password = WebConfigurationManager.AppSettings["Password"].ToString();
                string UsernameLogin = parameters["Username"].ToString();
                string PasswordLogin = parameters["Password"].ToString();

                if (UsernameLogin == Username && Password == PasswordLogin)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch(Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public List<DetalleBascula> EdicionOrdenProduccionBobinas(ODataActionParameters parameters)
        {
            try
            {
                string OrdenProduccion = parameters["OrdenProduccion"].ToString();

                List<DetalleBascula> Lst = (from ba in db.BASCULA
                                            where ba.CODIGOORDENPRODUCCION == OrdenProduccion
                                            select new DetalleBascula
                                            {
                                                 ID = ba.ID,
                                                 NROETIQUETA = ba.NROETIQUETA,
                                                 CODIGOORDENPRODUCCION = ba.CODIGOORDENPRODUCCION,
                                                 NOMBREPUESTO = ba.NOMBREPUESTO,
                                                 REFERENCIA = ba.REFERENCIA,
                                                 PRODUCCION = ba.PRODUCCION,
                                                 TRAMOS = ba.TRAMOS,
                                                 ANCHO = ba.ANCHO,
                                                 LARGO = ba.LARGO,
                                                 PESO = ba.PESO,
                                                 METROS = ba.METROS,
                                                 CODIGOPUESTO = ba.CODIGOPUESTO,
                                                 CODIGOTURNO = ba.CODIGOTURNO,
                                                 ELIMINADO = ba.ELIMINADO
                                            }).ToList();

                return Lst;

            }

            catch (Exception)
            {
                return null;
            }
        }


        [HttpPost]
        public bool EditarBascula(ODataActionParameters parameters)
        {
            try
            {
                string OrdenProduccion = parameters["OrdenProduccion"].ToString();
                Int32 ID = Convert.ToInt32(parameters["ID"]);
                bool Valor = Convert.ToBoolean(parameters["Valor"]);

                bool? EstadoDeLaBobina = (from ba in db.BASCULA
                                         where ba.ID == ID
                                         select ba.ELIMINADO).FirstOrDefault();

                if (EstadoDeLaBobina != null)
                {
                    /*Se compara estado de la bobina antes de hacer el Update, si el estado actual de la bobina es NO ELIMINADO (false),
                    y su nuevo estado es ELIMANDO (true), se procede hacer el update e indicar generar XML para la interfaz, de lo contrario solo se realiza
                    /update*/

                    if (EstadoDeLaBobina == false && Valor == true)
                    {
                        var query = (from ba in db.BASCULA
                                     where ba.CODIGOORDENPRODUCCION == OrdenProduccion && ba.ID == ID
                                     select ba).FirstOrDefault();
                        query.ELIMINADO = Valor;   
                        db.SaveChanges();
                    }

                    else
                    {
                        var query = (from ba in db.BASCULA
                                     where ba.CODIGOORDENPRODUCCION == OrdenProduccion && ba.ID == ID
                                     select ba).FirstOrDefault();
                        query.ELIMINADO = Valor;
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error EditarBascula: " + ex.Message);
            }
        }
        #endregion

        #region CONFIGURACION BASCULA
        public List<string> ConsultarConfiguracion()
        {
            try
            {
                List<string> Lst = new List<string>();

                string Impresora = WebConfigurationManager.AppSettings["NamePrintDefault"].ToString();
                Lst.Add(Impresora);
                string CopiasProduccion = WebConfigurationManager.AppSettings["NumeroDeCopiasProduccion"].ToString();
                Lst.Add(CopiasProduccion);
                string CopiasDesperdicio = WebConfigurationManager.AppSettings["NumeroDeCopiasDesperdicio"].ToString();
                Lst.Add(CopiasDesperdicio);
                string TipoImpresion = WebConfigurationManager.AppSettings["TipoImpresion"].ToString();
                Lst.Add(TipoImpresion);
                string AnchoImpresion = WebConfigurationManager.AppSettings["PageWidth"].ToString();
                Lst.Add(AnchoImpresion);
                string LargoImpresion = WebConfigurationManager.AppSettings["PageHeight"].ToString();
                Lst.Add(LargoImpresion);

                return Lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void EditarConfiguracion(ODataActionParameters parameters)
        {
            try
            {
                var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                config.AppSettings.Settings["NamePrintDefault"].Value = parameters["Impresora"].ToString();
                config.AppSettings.Settings["NumeroDeCopiasProduccion"].Value = parameters["CopiasProduccion"].ToString();
                config.AppSettings.Settings["NumeroDeCopiasDesperdicio"].Value = parameters["CopiasDesperdicio"].ToString();
                config.AppSettings.Settings["TipoImpresion"].Value = parameters["TipoImpresion"].ToString();
                config.AppSettings.Settings["PageWidth"].Value = parameters["AnchoImpresion"].ToString();
                config.AppSettings.Settings["PageHeight"].Value = parameters["AltoImpresion"].ToString();
                config.Save();
            }
            catch (Exception)
            {
 
            }
        }

        #endregion

        #region ALMACENAR BANDERAS
        /// <summary>
        /// Almacena la información de banderas
        /// </summary>
        /// <param name="parameters"></param>
        //[HttpPost]
        //public void AlmacenarBanderas(ODataActionParameters parameters)
        //{
        //    try
        //    {
                
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error AlmacenarBanderas" + ex.Message);
        //    }
        //}
        #endregion

        [HttpPost]
        public List<ResumenOPDTO> GetResumenOp(ODataActionParameters parameters)
        {
            string CodigoOrden = parameters["OrdenProduccion"].ToString();
            List<ResumenOPDTO> resumenOrden = new List<ResumenOPDTO>();

            var detBascula = db.SP_RESUMEN_ORDENPRODUCCION(CodigoOrden).ToList();

            foreach(var item in detBascula)
            {
                ResumenOPDTO resumen = new ResumenOPDTO();

                resumen.CODIGOORDNEPRODUCCION = item.CODIGOORDENPRODUCCION;
                resumen.TIPO = item.TIPO;
                resumen.TRAMOSPRODUCCION = item.TRAMOSPRODUCCION.ToString();
                resumen.M2PRODUCCION = item.M2PRODUCCION.ToString();
                resumen.PESOPRODUCCION = item.PESOPRODUCCION.ToString();
                resumen.TRAMOSRETAL = item.TRAMOSRETAL.ToString();
                resumen.M2RETAL = item.M2RETAL.ToString();
                resumen.PESORETAL = item.PESORETAL.ToString();
                resumen.TRAMOSIMPERFECTO = item.TRAMOSIMPERFECTO.ToString();
                resumen.M2IMPERFECTO = item.M2IMPERFECTO.ToString();
                resumen.PESOIMPERFECTO = item.PESOIMPERFECTO.ToString();
                resumen.TRAMOSSALDO = item.TRAMOSSALDO.ToString();
                resumen.M2SALDO = item.M2SALDO.ToString();
                resumen.PESOSALDO = item.PESOSALDO.ToString();
                resumen.TRAMOSREPROCESO = item.TRAMOSREPROCESO.ToString();
                resumen.M2REPROCESO = item.M2REPROCESO.ToString();
                resumen.PESOREPROCESO = item.PESOREPROCESO.ToString();

                resumenOrden.Add(resumen);
            }


            return resumenOrden;
        }


        [HttpPost]
        public List<double> GetConsultarLimitesParaSellado(ODataActionParameters parameters)
        {
            try
            {
                List<double> limites = new List<double>();
                string CodigoPuesto = (string)parameters["CodigoPuesto"];
                string CodigoProceso = (string)parameters["CodigoProceso"];
                string CodigoProducto = (string)parameters["CodigoProducto"];
                double PesoNeto = (double)parameters["PesoNeto"];

                double? factorvariable = (from p in db.PUESTOTRABAJOSEGUNPRODUCTO
                                          where p.CODIGOPRODUCTOS == CodigoProducto &&
                                          p.CODIGOPUESTO == CodigoPuesto &&
                                          p.CODIGOPROCESO == CodigoProceso
                                      select p.FACTORVARIABLE1).FirstOrDefault();

                bool FactorOriginal = true;
                if (factorvariable == null || factorvariable == 0)
                {
                    factorvariable = 1;
                    FactorOriginal = false;
                }
                    
                limites.Add(Convert.ToDouble(PesoNeto / Convert.ToDouble(factorvariable) * (1 - 0.8)));//Inferior
                limites.Add(Convert.ToDouble(PesoNeto / Convert.ToDouble(factorvariable) * (1 + 0.1)));//Superior
                limites.Add(Convert.ToDouble(factorvariable));//FactorVariable1
                if (FactorOriginal == true)
                    limites.Add(1);//FactorVariableOriginal
                else
                    limites.Add(0);//FactorVariableOriginal

                return limites;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al obtener ConsultarLimitesParaSellado(): " + ex.Message);
            }
        }

        [HttpPost]
        public string GetNameEmpleadoDesperdicio(ODataActionParameters parameters)
        {
            string codigoperario = (string)parameters["CodigoOperario"];
            var v = (from p in db.PERSONAL
                     where p.CODIGOPERSONAL == codigoperario
                     select new
                     {
                         NOMBRES = p.APELLIDOS + " " + p.NOMBRES
                     }).FirstOrDefault();

            if (v == null)
                return null;
            else
                return v.NOMBRES;
        }


        [HttpPost]
        public void PrintProduccionGenerico(ODataActionParameters parameters)
        {
            try
            {
                DateTime Fecha = DateTime.Now;
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string NombreProducto = parameters["Producto"].ToString();
                string CodigoMaquina = parameters["CodigoMaquina"].ToString();
                string Consecutivo = parameters["Consecutivo"].ToString();
                string Ancho = parameters["Ancho"].ToString();
                string Largo = parameters["Largo"].ToString();
                string Metros = parameters["Metros"].ToString();
                string PesoNeto = parameters["PesoNeto"].ToString();
                string PesoTotal = parameters["PesoTotal"].ToString();
                string Tramos = parameters["Tramos"].ToString();
                string Impresora;
                try
                {
                    Impresora = parameters["Impresora"].ToString();
                }
                catch
                {
                    Impresora = "Microsoft Print to PDF";
                    parameters["Impresora"] = Impresora;
                }

                parameters["CodigoPedido"] = ConsultarPedidoOrden(CodigoOrdenProduccion);

                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string Observaciones = parameters["Observaciones"].ToString();

                parameters.Remove("CodigoPuesto");
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("CodigoMaquina");

                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);

                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteProduccion.rdlc";

                string CodigoQr = Fecha.Year.ToString() + Fecha.Month.ToString() + Fecha.Day.ToString() +";" + CodigoOrdenProduccion.ToString() +
                    ";" + CodigoProducto + ";" + PesoTotal + ";" + Consecutivo;

                
                string nombreimageqr = "QRDESPERDICIO" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                Imprimir.Export(report, "Generico");
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintProduccionGenerico: " + ex.Message);
            }

        }


        /// <summary>
        /// Metodo para realizar la impresión de etiquetas de desperdicio
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintDesperdicio(ODataActionParameters parameters)
        {
            try
            {
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                DateTime Fecha = DateTime.Now;
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string NombreProducto = parameters["Producto"].ToString();
                string CodigoMaquina = parameters["CodigoMaquina"].ToString();
                string Consecutivo = parameters["Consecutivo"].ToString();
                string Ancho = parameters["Ancho"].ToString();
                string Largo = parameters["Largo"].ToString();
                string Metros = parameters["Metros"].ToString();
                string PesoNeto = parameters["PesoNeto"].ToString();
                string PesoTotal = parameters["PesoTotal"].ToString();
                string Tramos = parameters["Tramos"].ToString();
                string Impresora;
                try
                {
                    Impresora = parameters["Impresora"].ToString();
                }
                catch
                {
                    Impresora = "Microsoft Print to PDF";
                    parameters["Impresora"] = Impresora;
                }

                parameters["CodigoPedido"] = ConsultarPedidoOrden(CodigoOrdenProduccion);

                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string Observaciones = parameters["Observaciones"].ToString();

                try
                {
                    string MotivoDesperdicio = parameters["MotivoDesperdicio"].ToString();

                    parameters["CodigoProducto"] = CodigoProducto + MotivoDesperdicio[0];

                }
                catch
                {

                }
                parameters.Remove("CodigoPuesto");
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("CodigoMaquina");

                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);

                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteDesperdicio.rdlc";

                string CodigoQr = Fecha.Year.ToString() + Fecha.Month.ToString() + Fecha.Day.ToString() + ";" + CodigoOrdenProduccion.ToString() +
                   ";" + CodigoProducto + ";" + PesoTotal + ";" + Consecutivo;

                //CreateQrImage(ruta + "\\" + Impresora + "\\QRDESPERDICIO.png", CodigoQr);
                //parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + Impresora + "\\QRDESPERDICIO.png";
                string nombreimageqr = "QRDESPERDICIO" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                Imprimir.Export(report, "Generico");
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintDesperdicio: " + ex.Message); 
            }
        }


        /// <summary>
        /// Metodo para realizar la impresión de etiquetas de reproceso
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintReproceso(ODataActionParameters parameters)
        {
            try
            {
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                DateTime Fecha = DateTime.Now;
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string NombreProducto = parameters["Producto"].ToString();
                string CodigoMaquina = parameters["CodigoMaquina"].ToString();
                string Consecutivo = parameters["Consecutivo"].ToString();
                string Ancho = parameters["Ancho"].ToString();
                string Largo = parameters["Largo"].ToString();
                string Metros = parameters["Metros"].ToString();
                string PesoNeto = parameters["PesoNeto"].ToString();
                string PesoTotal = parameters["PesoTotal"].ToString();
                string Tramos = parameters["Tramos"].ToString();
                string Impresora;
                try
                {
                    Impresora = parameters["Impresora"].ToString();
                }
                catch
                {
                    Impresora = "Microsoft Print to PDF";
                    parameters["Impresora"] = Impresora;
                }

                parameters["CodigoPedido"] = ConsultarPedidoOrden(CodigoOrdenProduccion);

                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string Observaciones = parameters["Observaciones"].ToString();

                parameters.Remove("CodigoPuesto");
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("CodigoMaquina");

                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);

                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteDesperdicio.rdlc";

                string CodigoQr = Fecha.Year.ToString() + Fecha.Month.ToString() + Fecha.Day.ToString() + ";" + CodigoOrdenProduccion.ToString() +
                   ";" + CodigoProducto + ";" + PesoTotal + ";" + Consecutivo;

                //CreateQrImage(ruta + "\\" + Impresora + "\\QRDESPERDICIO.png", CodigoQr);
                //parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + Impresora + "\\QRDESPERDICIO.png";
                string nombreimageqr = "QRDESPERDICIO" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                Imprimir.Export(report, "Generico");
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintDesperdicio: " + ex.Message);
            }
        }



        /// <summary>
        /// Permite realizar la impresión de las etiquetas de Co extrusión
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintCoExtrusoraGeneral(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");
                
                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else 
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteCoExtrusora.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    parameters["Metros"].ToString().Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    parameters["Metros"].ToString().Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRCOEXTRUSIONGENERAL" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }

            catch (Exception ex)
            {
                throw new Exception("Error en PrintCoExtrusoraGeneral: " + ex.Message); 
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de flexografia y laminado
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintFlexoGeneral(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                DateTime FechaCurado = Fecha;
                if (parameters["Curado"].ToString() != "N/A")
                {
                    FechaCurado = FechaCurado.AddHours(Convert.ToInt32(parameters["Curado"]));
                    parameters["Curado"] = FechaCurado.ToString("yy/MM/dd HH:mm");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteFlexo.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    parameters["Metros"].ToString().Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    parameters["Metros"].ToString().Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRFLEXOGENERAL" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintflexoGeneral: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de corte general
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintCorteGeneral(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");
                parameters.Remove("Metros");

                //Consultar parte en el cliente y descipcion del cliente
                //parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar consecutivo
                parameters["Consecutivo"] = GetConsecutivoEtiquetas(CodigoPuesto) + Convert.ToInt32(parameters["Consecutivo"]);

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                ////ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteCorte.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRCORTEGENERAL" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintCorteGeneral: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de corte cliente quala
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintCorteQuala(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                DateTime FechaVencimiento = Fecha;
                FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");
                parameters.Remove("Metros");

                //Consultar parte en el cliente y descipcion del cliente
                //parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar consecutivo
                parameters["Consecutivo"] = GetConsecutivoEtiquetas(CodigoPuesto) + Convert.ToInt32(parameters["Consecutivo"]);

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteCorteQuala.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRCORTEQUALA" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                //Creación de Qr del cliente
                string CodigoQrCliente = "01" + parameters["IdClienteYCliente"].ToString().Split(' ')[0].PadLeft(8,'0').ToString() + "02" + "90307885" + "03" + "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() +
                    parameters["Consecutivo"].ToString().PadLeft(4, '0') + "10" + CodigoOrdenProduccion.PadLeft(10, '0') + "PE" + "11" +
                    Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "17" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() +
                    "3101" + "311" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(7, '0').ToString();

                string nombreimageqrcliente = "QRCORTEQUALACLIENTE" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqrcliente, CodigoQrCliente);
                parameters["CodigoQrRutaCliente"] = @"file:" + "///" + ruta + "\\" + nombreimageqrcliente;

                //Creación de código de barras cliente
                BarcodeFonts codigobarras = new BarcodeFonts();
                codigobarras.BarcodeType = BarcodeFonts.BarcodeEnum.Code128Auto;
                codigobarras.Data = "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + parameters["Consecutivo"].ToString().PadLeft(4, '0');
                codigobarras.encode();
                parameters["Codigo128"] = codigobarras.EncodedData;
                parameters["Codigo128Texto"] = "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + parameters["Consecutivo"].ToString().PadLeft(4, '0');

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
                EliminarImage(ruta + "\\" + nombreimageqrcliente);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrntCorteQuala: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de corte cliente nutresa
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintCorteNutresa(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                DateTime FechaVencimiento = Fecha;
                FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");
                parameters.Remove("Metros");

                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar información del código EAN14
                //parameters["CodigoEAN14"] = ConsultarEAN14(CodigoProducto);
                //Consultar consecutivo
                parameters["Consecutivo"] = GetConsecutivoEtiquetas(CodigoPuesto) + Convert.ToInt32(parameters["Consecutivo"]);
                //Consultar SKU
                //parameters["SKU"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto).Replace(" ", "").ToString();

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteCorteNutresa.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                string nombreimageqr = "QRCORTENUTRESA" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                //Creación de códigos de barra cliente
                parameters["Codigo128"] = "01" + parameters["CodigoEAN14"].ToString() + "3102" + parameters["PesoNeto"].ToString().Replace(".", "").ToString().PadLeft(6, '0') + "17" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" + CodigoOrdenProduccion;
                parameters["Codigo128Texto"] = "(01)" + parameters["CodigoEAN14"].ToString() + "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").ToString().PadLeft(6, '0') + "(17)" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" + CodigoOrdenProduccion;

                //Creación de código de barras cliente EAN14
                BarcodeFonts codigobarrasean14 = new BarcodeFonts();
                codigobarrasean14.BarcodeType = BarcodeFonts.BarcodeEnum.GS1Databar14;
                codigobarrasean14.Data = parameters["CodigoEAN14"].ToString();
                codigobarrasean14.encode();
                parameters["CodigoEAN14"] = codigobarrasean14.EncodedData; 

                //Creación de código de barras cliente EAN32
                BarcodeFonts codigobarras132 = new BarcodeFonts();
                codigobarras132.BarcodeType = BarcodeFonts.BarcodeEnum.UCCEAN;
                codigobarras132.Data = parameters["Codigo128"].ToString();
                codigobarras132.encode();
                parameters["Codigo128"] = codigobarras132.EncodedData; 
                
                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrntCorteQuala: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de corte clientes en ingles
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintCorteIngles(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                int Batch = Convert.ToInt32(parameters["CodigoEntrada"].ToString());
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");
                parameters.Remove("Metros");
                parameters.Remove("CodigoEntrada");

                //Consultar parte en el cliente y descipcion del cliente
               //parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar consecutivo
                parameters["Consecutivo"] = GetConsecutivoEtiquetas(CodigoPuesto) + Convert.ToInt32(parameters["Consecutivo"]);
                //Consultar brand
                //parameters["Brand"] = ConsultarBrand(CodigoOrdenProduccion, CodigoProducto);
                //Consultar ponum
                //parameters["Ponum"] = ConsultarPonum(CodigoOrdenProduccion, CodigoProducto);
                //Consultar fecha del batch
                parameters["FechaBatch"] = ConsultarFechaDeBatch(Batch);

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteCorteIngles.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRCORTEINGLES" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());
                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintCorteIngles: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de sellado general
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintSelladoGeneral(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");

                //Consultar parte en el cliente y descipcion del cliente
               //parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);




                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteSellado.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRSELLADOGENERAL" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch(Exception ex)
            {
                throw new Exception("Error en PrintSelladoGeneral: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de sellado cliente quala
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintSelladoQuala(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                DateTime FechaVencimiento = Fecha;
                FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");

                //Consultar parte en el cliente y descipcion del cliente
               // parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteSelladoQuala.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRSELLADOQUALA" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                //Creación de Qr del cliente
                string CodigoQrCliente = "01" + parameters["IdClienteYCliente"].ToString().Split(' ')[0].PadLeft(8,'0').ToString() + "02" + "90307885" + "03" + "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() +
                    parameters["Consecutivo"].ToString().PadLeft(4, '0') + "10" + CodigoOrdenProduccion.PadLeft(10, '0') + "PE" + "11" +
                    Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "17" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() +
                    "3101" + "311" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(7, '0').ToString();

                string nombreimageqrcliente = "QRSELLADOQUALACLIENTE" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqrcliente, CodigoQrCliente);
                parameters["CodigoQrRutaCliente"] = @"file:" + "///" + ruta + "\\" + nombreimageqrcliente;

                //Creación de código de barras cliente
                BarcodeFonts codigobarras = new BarcodeFonts();
                codigobarras.BarcodeType = BarcodeFonts.BarcodeEnum.Code128Auto;
                codigobarras.Data = "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + parameters["Consecutivo"].ToString().PadLeft(4, '0');
                codigobarras.encode();
                parameters["Codigo128"] = codigobarras.EncodedData;
                parameters["Codigo128Texto"] = "PE" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + parameters["Consecutivo"].ToString().PadLeft(4, '0');

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
                EliminarImage(ruta + "\\" + nombreimageqrcliente);
            }
            catch(Exception ex)
            {
                throw new Exception("Error en PrintSelladoQuala: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de sellado cliente nutresa
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintSelladoNutresa(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                DateTime FechaVencimiento = Fecha;
                FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");

                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar información del código EAN14
                //parameters["CodigoEAN14"] = ConsultarEAN14(CodigoProducto);
                //Consultar SKU
               // parameters["SKU"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto).Replace(" ", "").ToString();

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteSelladoNutresa.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                string nombreimageqr = "QRSELLADONUTRESA" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                //Creación de códigos de barra cliente
                parameters["Codigo128"] = "01" + parameters["CodigoEAN14"].ToString() + "3102" + parameters["PesoNeto"].ToString().Replace(".", "").ToString().PadLeft(6, '0') + "17" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" + CodigoOrdenProduccion;
                parameters["Codigo128Texto"] = "(01)" + parameters["CodigoEAN14"].ToString() + "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").ToString().PadLeft(6, '0') + "(17)" + FechaVencimiento.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" + CodigoOrdenProduccion;

                //Creación de código de barras cliente EAN14
                BarcodeFonts codigobarrasean14 = new BarcodeFonts();
                codigobarrasean14.BarcodeType = BarcodeFonts.BarcodeEnum.GS1Databar14;
                codigobarrasean14.Data = parameters["CodigoEAN14"].ToString();
                codigobarrasean14.encode();
                parameters["CodigoEAN14"] = codigobarrasean14.EncodedData; 

                //Creación de código de barras cliente EAN32
                BarcodeFonts codigobarras132 = new BarcodeFonts();
                codigobarras132.BarcodeType = BarcodeFonts.BarcodeEnum.UCCEAN;
                codigobarras132.Data = parameters["Codigo128"].ToString();
                codigobarras132.encode();
                parameters["Codigo128"] = codigobarras132.EncodedData; 
                
                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintSelladoNutresa: " + ex.Message);
            }
        }

        /// <summary>
        /// Permite realizar la impresión de las etiquetas de sellado ingles
        /// </summary>
        /// <param name="parameters"></param>
        [HttpPost]
        public void PrintSelladoIngles(ODataActionParameters parameters)
        {
            try
            {
                //Obtener variables de impresión
                string CodigoOrdenProduccion = parameters["OrdenDeProduccion"].ToString();
                string CodigoPuesto = parameters["CodigoMaquina"].ToString();
                string CodigoProducto = parameters["CodigoProducto"].ToString();
                string CodigoProceso = parameters["CodigoProceso"].ToString();
                string Impresora = parameters["Impresora"].ToString();
                string NumeroCopias = parameters["NumeroCopias"].ToString();
                string WarehouseCode = parameters["WarehouseCode"].ToString();
                string CodigoOperario = parameters["CodigoOperario"].ToString();
                string Metros = parameters["Metros"].ToString();
                int Batch = Convert.ToInt32(parameters["CodigoEntrada"].ToString());
                DateTime Fecha = DateTime.Now;
                parameters["Fecha"] = Fecha.ToString("yy/MM/dd HH:mm");

                if (parameters["MesesDespues"].ToString() == "N/A")
                    parameters["FechaVencimiento"] = "";
                else
                {
                    DateTime FechaVencimiento = Fecha;
                    FechaVencimiento = FechaVencimiento.AddMonths(Convert.ToInt32(parameters["MesesDespues"]));
                    parameters["FechaVencimiento"] = FechaVencimiento.ToString("yy/MM/dd");
                }

                //Eliminar parametros excluidos de reporte
                parameters.Remove("Impresora");
                parameters.Remove("NumeroCopias");
                parameters.Remove("WarehouseCode");
                parameters.Remove("MesesDespues");
                parameters.Remove("CodigoProceso");
                parameters.Remove("CodigoOperario");
                parameters.Remove("CodigoEntrada");

                //Consultar parte en el cliente y descipcion del cliente
               //parameters["IdClienteYCliente"] = ConsultarCodigoParteYDescripcionCliente(CodigoOrdenProduccion, CodigoProducto) + parameters["IdClienteYCliente"].ToString();
                //Construir nombre del empleado
                parameters["NombreOperario"] = ConsultarArmarNombreOperario(CodigoOperario);
                //Consultar brand
                //parameters["Brand"] = ConsultarBrand(CodigoOrdenProduccion, CodigoProducto);
                //Consultar ponum
                //parameters["Ponum"] = ConsultarPonum(CodigoOrdenProduccion, CodigoProducto);
                //Consultar fecha del batch
                parameters["FechaBatch"] = ConsultarFechaDeBatch(Batch);

                //Obtener rutas y seleccion de reporte
                string ruta = HttpContext.Current.Server.MapPath("~/Reportes");
                //ValidarDirectorio(ruta + "\\" + Impresora);
                LocalReport report = new LocalReport();
                report.ReportPath = ruta + "\\ReporteSelladoIngles.rdlc";

                //Creación de código QR
                string CodigoQr = "01" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "21" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "3102" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "37" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "11" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "10" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "99" + WarehouseCode;

                parameters["CodigoQrTexto"] = "(01)" + CodigoProducto.Split('-')[0].PadLeft(7, '0').ToString() + "(21)" + CodigoOrdenProduccion.PadLeft(6, '0') +
                    "(3102)" + parameters["PesoNeto"].ToString().Replace(".", "").PadLeft(6, '0').ToString() + "(37)" +
                    Metros.Replace(".", "").PadLeft(9, '0').ToString() + "(11)" + Fecha.ToString("yy/MM/dd").Replace("/", "").ToString() + "(10)" +
                    CodigoOrdenProduccion.PadLeft(6, '0') + parameters["Consecutivo"].ToString().PadLeft(4, '0') + "(99)" + WarehouseCode;

                string nombreimageqr = "QRSELLADOINGLES" + DateTime.Now.ToString("yy/MM/dd").Replace("/", "").ToString() + new Random().Next(1000, 9999).ToString() + ".png";
                CreateQrImage(ruta + "\\" + nombreimageqr, CodigoQr);
                parameters["CodigoQrRuta"] = @"file:" + "///" + ruta + "\\" + nombreimageqr;

                ReportParameter[] RepParameter = ObternerPamrametros(parameters.Count, parameters.Values.ToArray(), parameters.Keys.ToArray());

                report.EnableExternalImages = true;
                report.SetParameters(RepParameter);

                ImprimirReportes Imprimir = new ImprimirReportes();
                //Imprimir.Export(report, ConsultarTamañoDeImpresion(CodigoPuesto), true);
                Imprimir.Print(Convert.ToInt16(NumeroCopias), Impresora);

                EliminarImage(ruta + "\\" + nombreimageqr);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en PrintSelladoIngles: " + ex.Message);
            }
        }

        //[HttpPost]
        //public string GetEsquemaDeImpresion(ODataActionParameters parameters)
        //{
        //    string codigoPueto = parameters["CodigoPuesto"].ToString();
        //    var a = (from tw in db.TRANSMICIONWEBORDEN
        //            where tw.CODIGOPUESTO == codigoPueto
        //            select tw.RDLCBASCULA).FirstOrDefault();
        //    return a;
        //}

        //[HttpPost]
        //public string GetEsquemaDeImpresionPorProceso(ODataActionParameters parameters)
        //{
        //    string proceso = parameters["Proceso"].ToString();

        //    if (proceso == "COEXTRUSION")
        //        proceso = "MCOEXPP7";
        //    else if (proceso == "SELLADO")
        //        proceso = "MPOUCH JUD";
        //    else if (proceso == "CORTE")
        //        proceso = "MCOT5";
        //    else if (proceso == "FLEXO")
        //        proceso = "MFLEX6";

        //    return (from tw in db.TRANSMICIONWEBORDEN
        //            where tw.CODIGOPUESTO == proceso
        //            select tw.RDLCBASCULA).FirstOrDefault();
        //}

        ///// <summary>
        ///// Permite editar configuración de impresión de etiquetas
        ///// </summary>
        ///// <param name="parameters"></param>
        //[HttpPost]
        //public void GetEditarEstandares(ODataActionParameters parameters)
        //{
        //    try
        //    {
        //        string proceso = parameters["Proceso"].ToString();
        //        string value = parameters["ValueRDLC"].ToString();


        //        var lista = db.TRANSMICIONWEBORDEN.Where(c => c.RDLCBASCULA.Contains(proceso)).ToList();


        //        //var lista = (from tw in db.TRANSMICIONWEBORDEN
        //        //             where SqlMethods.Like(tw.RDLCBASCULA, "%COEXTRUSION%")
        //        //                select tw).ToList();

        //        foreach(var l in lista)
        //        {
        //            l.RDLCBASCULA = value;
        //        }
        //        db.SaveChanges();


        //        //if (proceso == "COEXTRUSION")
        //        //    proceso = "MCOEXPP7";
        //        //else if (proceso == "SELLADO")
        //        //    proceso = "MPOUCH JUD";
        //        //else if (proceso == "CORTE")
        //        //    proceso = "MCOT5";
        //        //else if (proceso == "FLEXO")
        //        //    proceso = "MFLEX6";

        //        //return (from tw in db.TRANSMICIONWEBORDEN
        //        //        where tw.CODIGOPUESTO == proceso
        //        //        select tw.RDLCBASCULA).FirstOrDefault();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error en GetEditarEstandares: " + ex.Message);
        //    }
        //}

        private void ValidarDirectorio(string ruta)
        {
            if (!Directory.Exists(ruta))
                Directory.CreateDirectory(ruta);
        }

        private void CreateQrImage(string nombre, string texto)
        {
            QrCodeGraphicControl Qr = new QrCodeGraphicControl();
            Qr.Text = texto;
            BitMatrix matrix = Qr.GetQrMatrix();
            GraphicsRenderer gRender = new GraphicsRenderer(new FixedModuleSize(30, QuietZoneModules.Four));
            using (FileStream stream = new FileStream(nombre, FileMode.Create))
            {
                gRender.WriteToStream(matrix, ImageFormat.Png, stream);

            }
        }

        private ReportParameter[] ObternerPamrametros(int count, Array ParameterValues, Array ParameterKeys)
        {
            ReportParameter[] RepParameter = new ReportParameter[count];
            for (int i = 0; i < count; i++)
            {
                string ValueParameter = ParameterValues.GetValue(i).ToString();
                if (ValueParameter == "" || ValueParameter == null)
                {
                    ValueParameter = " ";
                }

                string KeyParameter = ParameterKeys.GetValue(i).ToString();
                RepParameter[i] = new ReportParameter(KeyParameter, ValueParameter);
            }

            return RepParameter;
        }

        //private string ConsultarTamañoDeImpresion(string codigoPuesto)
        //{
        //    return (from tw in db.TRANSMICIONWEBORDEN
        //            where tw.CODIGOPUESTO == codigoPuesto
        //            select tw.TAMAÑOIMPRESION).FirstOrDefault();
        //}

        private string ConsultarUnidadDeMedida(string codigopuesto)
        {
            //string value = (from pa in db.PUESTOSPORASEMBLY
            //                where pa.CODIGOPUESTO == codigopuesto && pa.ASEMBLY == 0
            //                select pa.UNIDAD).FirstOrDefault();

            //if (value == null || value == "" || value == "M" || value == "KG")
            //    return "m";
            //else
            //    return "";

            string[] value = {"MBETOL1","MBETOL2","MBETOL3","MBETOL4"
                                ,"MPOUCH ZEN"
                                ,"MWICKETE"
                                ,"MLATEAL"
                                ,"MLATEAL1"
                                ,"MLATEAL2"
                                ,"MLATEAL3"
                                ,"MLATEAL4"
                                ,"MLATEAL5"
                                ,"MRENOVA"
                                ,"MVAL MAN1","MVAL MAN2"};

            if(value.Contains("codigopuesto"))
                return "u";
            else
                return "m";
        }
   
        private string ConsultarArmarNombreOperario(string codigoOperario)
        {
            var data = (from p in db.PERSONAL
                        where p.CODIGOPERSONAL == codigoOperario
                        select new
                        {
                            Nombre = p.NOMBRES,
                            Apellido = p.APELLIDOS
                        }).FirstOrDefault();
            if (data == null)
                return "";
            else
                return data.Apellido.Substring(0, 1).ToString() + "." + data.Nombre.Split(' ')[0].ToString();
        }

        private void EliminarImage(string ruta)
        {
            if (File.Exists(ruta))
                File.Delete(ruta);
        }

        
        private int? GetConsecutivoEtiquetas(string CodigoPuesto)
        {
            return db.SP_CONSECUTOSBASCULA(CodigoPuesto).FirstOrDefault();
        }

        //private string ConsultarBodega(string ordenDeProduccion, string CodigoProceso)
        //{
        //    string bodega = (from e in db.EPICOR_MATERIALES_PORJOB
        //                     where e.CODIGOORDENPRODUCCION == ordenDeProduccion && e.CODIGOPROCESO == CodigoProceso
        //                     select e.BODEGA).FirstOrDefault();
        //    if(bodega == null)
        //    {
        //        bodega = "";
        //    }

        //    return bodega;
        //}

        //private string ConsultarBin(string ordenDeProduccion, string CodigoProceso)
        //{
        //    string bin = (from e in db.EPICOR_MATERIALES_PORJOB
        //                  where e.CODIGOORDENPRODUCCION == ordenDeProduccion && e.CODIGOPROCESO == CodigoProceso
        //                  select e.BIN).FirstOrDefault();
        //    if (bin == null)
        //    {
        //        bin = "";
        //    }

        //    return bin;
        //}

        private string ConsultarFechaDeBatch(int codigoEntrada)
        {
            DateTime? date = (from ej in db.ENTRADAEJECUCION
                              where ej.CODIGOENTRADAEJECUCION == codigoEntrada
                              select ej.FECHAENTRADA).FirstOrDefault();
            if (date != null)
                return date.Value.ToString("yy/MM/dd HH:mm", CultureInfo.InvariantCulture);
            else
                return "";
        }


        [HttpPost]
        public List<CELULAS> GetListaCelulas()
        {
            try
            {
                List<CELULAS> lst = new List<CELULAS>();
                lst = (from c in db.CELULAS
                       orderby c.CODIGOCELULA ascending
                       select c).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener GetListaCelulas(): " + ex.Message);
            }
        }

        private string ConsultarPedidoOrden(string CodigoOrdenProduccion)
        {
            string pedido = (from pop in db.ORDENES_DE_PRODUCCION
                            where pop.CODIGOORDENPRODUCCION == CodigoOrdenProduccion
                            select pop.CODIGOPEDIDO.ToString()).FirstOrDefault().ToString();
            if(pedido == "")
            {
                pedido = "9999";
            }

            return pedido;
        }
    }
}
