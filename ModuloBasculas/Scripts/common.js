﻿function activeClass(id, content, parent) {
    var ul = document.getElementById("side-menu");
    var items = ul.getElementsByTagName("li");
    for (var i = 0; i < items.length; ++i) {
        //Debemos remover la class active de todos los elementos li
        $(items[i].getElementsByTagName("a")).removeClass("active");
    }
    //Después de haber quitado la class active de todos los objetos de la lista se debe activar el id mandado por parametro
    //$("#" + parent).addClass("in");
    $("#" + parent).addClass("active");
    document.getElementById(content).style.display = "block";
    $("#" + id).addClass("active");
}