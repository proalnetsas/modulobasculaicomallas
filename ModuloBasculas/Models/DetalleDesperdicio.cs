﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModuloBasculas.Models
{
    public class DetalleDesperdicio
    {
        public string CodigoPuesto { get; set; }
        public string NombrePuesto { get; set; }
        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public string CodigoProceso { get; set; }
        public string NombreCliente { get; set; }
    }
}