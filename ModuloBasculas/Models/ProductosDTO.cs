﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModuloBasculas.Models
{
    public class ProductosDTO
    {
        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public double? AnchoProducto { get; set; }
        public double? LargoProducto { get; set; }
        public double? PesoProducto { get; set; }
    
    }
}