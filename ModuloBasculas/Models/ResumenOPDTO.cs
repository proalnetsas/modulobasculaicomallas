﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModuloBasculas.Models
{
    public class ResumenOPDTO
    {

        public string CODIGOORDNEPRODUCCION { get; set; }
        public string TRAMOSPRODUCCION { get; set; }
        public string M2PRODUCCION { get; set; }
        public string PESOPRODUCCION { get; set; }
        public string TRAMOSRETAL { get; set; }
        public string M2RETAL { get; set; }
        public string PESORETAL { get; set; }
        public string TRAMOSIMPERFECTO { get; set; }
        public string M2IMPERFECTO { get; set; }
        public string PESOIMPERFECTO { get; set; }
        public string TRAMOSSALDO { get; set; }
        public string M2SALDO { get; set; }
        public string PESOSALDO { get; set; }
        public string TRAMOSREPROCESO { get; set; }
        public string M2REPROCESO { get; set; }
        public string PESOREPROCESO { get; set; }
        public string TIPO { get; set; }
    }
}