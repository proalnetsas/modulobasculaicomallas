//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModuloBasculas.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ORDENES_DE_PRODUCCION
    {
        public ORDENES_DE_PRODUCCION()
        {
            this.ORDENPRODUCCIONXPRODUCTO = new HashSet<ORDENPRODUCCIONXPRODUCTO>();
        }
    
        public string CODIGOORDENPRODUCCION { get; set; }
        public string CODIGO_BODEGA { get; set; }
        public string NOMBRE { get; set; }
        public string CODIGOPEDIDO { get; set; }
        public string CODIGOBARRAS { get; set; }
        public Nullable<System.DateTime> FECHA_INICIO { get; set; }
        public Nullable<System.DateTime> FECHA_ENTREGA { get; set; }
        public Nullable<int> ESTADO { get; set; }
        public string DOCUMENTO_REMICION { get; set; }
        public Nullable<System.DateTime> FECHA_REGISTRO { get; set; }
        public string USUARIO { get; set; }
        public Nullable<System.DateTime> TIMESPAN { get; set; }
    
        public virtual ICollection<ORDENPRODUCCIONXPRODUCTO> ORDENPRODUCCIONXPRODUCTO { get; set; }
    }
}
