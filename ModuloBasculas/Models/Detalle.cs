﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModuloBasculas.Models
{
    public class Detalle
    {
        public string Operario { get; set; }
        public string CodigoTurno { get; set; }
        public string Turno { get; set; }
        public string Referencia { get; set; }
        public Int64 CodigoEntradaEjecucion { get; set; }

        public string CodigoProducto { get; set; }
        public string CodigoPuesto { get; set; }

        public string NombreProducto { get; set; }

        public string NombreCliente { get; set; }
        public string CodigoCliente { get; set; }
        public string NitCliente { get; set; }

        public string CodigoOperario { get; set; }
        public string NombreMaquina { get; set; }

        public string CodigoProceso { get; set; }

        public DateTime? FechaEntradaEjecucion { get; set; }
        public bool BobinaEnturno { get; set; }
        public bool RequiereCambioOperario { get; set; }
        public string OrdenDeProduccion { get; set; }

        public string Secuencia { get; set; }
        public string Material { get; set; }
        public string DescripcionMaterial { get; set; }
        public string Assembly { get; set; }
        public string Lote { get; set; }
    }
}