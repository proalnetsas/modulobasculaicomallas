//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModuloBasculas.Models
{
    using System;
    
    public partial class SP_RESUMEN_ORDENPRODUCCION_Result
    {
        public string CODIGOORDENPRODUCCION { get; set; }
        public Nullable<int> TRAMOSPRODUCCION { get; set; }
        public Nullable<double> M2PRODUCCION { get; set; }
        public Nullable<double> PESOPRODUCCION { get; set; }
        public Nullable<int> TRAMOSRETAL { get; set; }
        public Nullable<double> M2RETAL { get; set; }
        public Nullable<double> PESORETAL { get; set; }
        public Nullable<int> TRAMOSIMPERFECTO { get; set; }
        public Nullable<double> M2IMPERFECTO { get; set; }
        public Nullable<double> PESOIMPERFECTO { get; set; }
        public Nullable<int> TRAMOSSALDO { get; set; }
        public Nullable<double> M2SALDO { get; set; }
        public Nullable<double> SALDO { get; set; }
        public Nullable<int> TRAMOSREPROCESO { get; set; }
        public Nullable<double> M2REPROCESO { get; set; }
        public Nullable<double> PESOREPROCESO { get; set; }
    }
}
