﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModuloBasculas.Models
{
    public class DetalleBascula
    {    
        public int ID { get; set; }
        public string NROETIQUETA { get; set; }
        public string CODIGOORDENPRODUCCION { get; set; }
        public string NOMBREPUESTO { get; set; }
        public string REFERENCIA { get; set; }
        public string PRODUCCION { get; set; }
        public int? TRAMOS { get; set; }
        public double? ANCHO { get; set; }
        public double? LARGO { get; set; }
        public double? PESO { get; set; }
        public double? METROS { get; set; }
        public string CODIGOPUESTO { get; set; }
        public string CODIGOTURNO { get; set; }
        public Boolean? ELIMINADO { get; set; }
    }
}